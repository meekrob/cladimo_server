"""
Convert an open text file into an XML representation for CladiMo output or a MAF file.

    Two XML representations of cladimo are used here, a sparse one and a rich one. 
    The sparse format is intented to provide the mafbrowser.swf flash application the text coordinates to highlight sequence
    matches from CladiMo, mapped onto the alignment text.

    The rich format provides a full representation of cladistic motifs. XSD specification of the data structures is not yet
    available.
"""
from xml.dom.minidom import Document

from gffMarshall import *

import assembly_translation

def createChildNode(doc, nodeName, value):
    """
    Helper function to create <nodeName>value</nodeName> underneath doc. 
    Value is added to <nodeName> as a text node.
    The new node <nodeName> is returned.
    """
    node = doc.createElement(nodeName)
    text = doc.createTextNode( str(value).strip() )
    node.appendChild( text )
    return node

def gff_to_styled_xml( infile, tag, accession):
    """
    Produce the rich XML format from GFF-parseable input.
    The user files are of the format accession_tag.gff, where tag is intended to be the user-defined part
    of the filename (or else "pastedtext"). The accession is assigned within accession.py using mkstemp.

    XML follows this structure:

    <cladimo_output>
        <datasource> i.e. "gff" </datasource>
        <dataname> tag </dataname>
        <accession> accession </accession>
        
        <scanned_block>
            <maf_block> ... </maf_block>
            <!-- Data for a set of matches (forming a cladistic motif) goes here -->
            <cladistic_motif> 
                <matches>
                    <!-- Data for an individual sequence match goes here -->
                    <match>
                        <query> ... </query>
                        <reference_interval> ... </reference_interval> <!-- The coordinates on the reference genome -->
                        <matched_position>   ... </matched_position>   <!-- The coordinates on the other species' genome -->
                    </match>
                        ...
                </matches>
                <species_matched_list>
                    <species_matched>""</species_matched>
                                    ...
                </species_matched_list>
            </cladistic_motif>
                ...
        </scanned_block>
            ...
    </cladimo_output>
    """
    doc = Document()
    styleinfo = doc.createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="/css/cladimo.xslt"')
    doc.appendChild(styleinfo)

    # create a new <bed> base element
    newbed = doc.createElement('cladimo_output')
    doc.appendChild(newbed)
    newbed.appendChild( createChildNode(doc, "datasource", "gff") )
    newbed.appendChild( createChildNode(doc, "dataname", tag) )
    newbed.appendChild( createChildNode(doc, "accession", accession) )

    output_block = None
    pwm_motif = None
    cladistic_motif = None
    pwm_query = None

    for linenum,line in enumerate(infile):
        newbed.appendChild(doc.createComment("encountered line #" + str(linenum)))
        if line.startswith('#'): continue
        if not line.isspace():
            try: 
                gff = GFF(line, commasplit="True")
                # all output lines contain at least this information
                interval = doc.createElement("reference_interval")
                interval.appendChild(createChildNode(doc,"chrom", gff.seqname))
                interval.appendChild(createChildNode(doc,"start", gff.start))
                interval.appendChild(createChildNode(doc,"end", gff.end))
                interval.appendChild(createChildNode(doc, "score", gff.score))
                interval.appendChild(createChildNode(doc, "strand", gff.strand))
                #interval.appendChild(createChildNode(doc, "frame", gff.frame))

                if gff.feature == "maf_block":
                    output_block = doc.createElement("scanned_block")
                    newbed.appendChild(output_block)
                    maf_block = doc.createElement("alignment")
                    maf_block.setAttribute("type","maf")
                    output_block.appendChild(maf_block)
                    maf_block.appendChild(interval)
                elif gff.feature == "cladistic_motif":
                    cladistic_motif = doc.createElement("cladistic_motif")
                    output_block.appendChild(cladistic_motif)
                    cladistic_motif.appendChild(interval)
                    matches = doc.createElement("matches")
                    cladistic_motif.appendChild(matches)
                
                elif gff.feature == "pwm_match":
                    pwm_match = doc.createElement("match")
                    pwm_match.appendChild(interval)
                    matches.appendChild(pwm_match)
                    pwm_query = doc.createElement("query")
                    pwm_match.appendChild(pwm_query)

                # now unstack the data in the final, ;-delimited fields
                for k,v in gff.attributes.items():
                    if k == 'aligned_sources':
                        aligned_sources = doc.createElement('aligned_sources')
                        maf_block.appendChild(aligned_sources)
                        for src in v:
                            aligned_source = doc.createElement('aligned_source')
                            aligned_sources.appendChild(aligned_source)
                            try:
                                aligned_sp, aligned_seq = src.split('.')
                                aligned_source.appendChild(createChildNode(doc, 'species', aligned_sp))
                                aligned_source.appendChild(createChildNode(doc, 'sequence', aligned_seq))
                            except:
                                aligned_source.appendChild(doc.createTextNode(src))
                    elif k == 'species_matched':
                        species_matched_list = doc.createElement('species_matched_list')
                        cladistic_motif.appendChild(species_matched_list)
                        species_matched = v
                        if type(species_matched) == type(""):
                            species_matched = [v]
                        for sp in species_matched: 
                            species_matched_list.appendChild(createChildNode(doc, "species_matched", sp))
                    elif k == 'position':
                        (spchrom, seqstart, seqend) = v
                        position = doc.createElement('matched_position')
                        pwm_match.appendChild(position)
                        match_src = doc.createElement("source")
                        position.appendChild(match_src)
                        try:
                            match_sp,match_seq = spchrom.split('.')
                            # as above with aligned_sources
                            match_src.appendChild(createChildNode(doc, "species", match_sp))
                            match_src.appendChild(createChildNode(doc, "sequence", match_seq))
                        except:
                            match_src.appendChild(doc.createTextNode(spchrom))

                        position.appendChild(createChildNode(doc, "start", seqstart))
                        position.appendChild(createChildNode(doc, "end", seqend))
                        if (gff.end == seqend) and (gff.start == seqstart) and (spchrom.endswith(gff.seqname)):
                            cladistic_motif.setAttribute("reference_matched", "true")
                    elif k == "column":
                        pwm_match.appendChild(createChildNode(doc, "column", v))
                    elif k == "width":
                        pwm_match.appendChild(createChildNode(doc, "width", v))
                    elif k == "sequence":
                        pwm_match.appendChild(createChildNode(doc, "matched_sequence", v))
                    elif k == 'pwm_name':
                        if gff.feature == "cladistic_motif":
                            cladistic_motif.setAttribute("pwm_name", v)
                        elif gff.feature == 'pwm_match':
                            pwm_query.appendChild(createChildNode(doc, "pwm_name", v))
                    elif k == 'pwm_motif':
                        if gff.feature == "cladistic_motif":
                            cladistic_motif.setAttribute("pwm_motif", v)
                        elif gff.feature == "pwm_match":
                            pwm_query.appendChild(createChildNode(doc, "pwm_motif", v))
                    elif k == 'pwm_accession':
                        if gff.feature == "cladistic_motif":
                            cladistic_motif.setAttribute("pwm_accession", v)
                        elif gff.feature == "pwm_match":
                            #pwm_match.setAttribute("accession", v)
                            pwm_query.appendChild(createChildNode(doc, "pwm_accession", v))
                    elif k == 'number_of_matches':
                        cladistic_motif.setAttribute("number_of_matches", str(v))
                    elif k == 'ncolumns':
                        if gff.feature == "maf_block":
                            maf_block.setAttribute("columns", str(v))
                    else:  # un-anticipated?
                        output_block.appendChild(createChildNode(doc, k, v))
        

            except:
                unparseable = doc.createElement("unparsed")
                msg = doc.createElement("exception")
                err = doc.createCDATASection(str( sys.exc_info() ))
                msg.appendChild(err)
                unparseable.appendChild(msg)
                txt = doc.createCDATASection(line)
                unparseable.appendChild(txt)
                newbed.appendChild(unparseable)
                continue
    return doc

def gff_to_xml( infile, name=None):
    """
    Create the sparse format for the MAFbrowser.

    Root element is <bed>, followed by
    <interval species="" chrom="" start="" end="" name="">
        

    Last line of output deletes the header... why?
    """
    # create a mini document
    doc = Document()

    # create a new <gff> base element
    newbed = doc.createElement('bed')

    if name:
        newbed.setAttribute("name", name)

    doc.appendChild(newbed)
    for line in infile:
        if line.startswith('#'):
            continue
        if line.isspace():
            continue
        try: 
            gff = GFF(line, commasplit="True")
        except:
            # this outcome could be a CDATA element
            continue
        
        if 'position' in gff.attributes:
            newinterval = doc.createElement( "interval" )

            chrom, start, end = gff.attributes['position']
            newinterval.setAttribute( "start", str( start ) )
            newinterval.setAttribute( "end", str( end ) )

            if chrom.find('.') >= 0:
                species,src = chrom.split('.',1)
                srcsp = assembly_translation.translate( species )

                newinterval.setAttribute( "species", srcsp )
                newinterval.setAttribute( "chrom", src )
            else:
                newinterval.setAttribute( "chrom", chrom )

            # fish for a PWM or motif name for the interval
            if 'pwm_motif' in gff.attributes:
                newinterval.setAttribute( "name", gff.attributes['pwm_motif'] )

            newbed.appendChild( newinterval )

    xmlhead = '<?xml version="1.0" ?>'
    #return doc.toprettyxml(indent="  ").replace(xmlhead, '')
    return doc

def bed_to_xml( infile ):

    # create a mini document
    doc = Document()

    # create a new <bed> base element
    newbed = doc.createElement('bed')
    doc.appendChild(newbed)

    # create elements
    for line in infile:
        if line.startswith("#") or line.isspace():
            continue

        fields = line.split()
        chrom = fields[0]
        start = fields[1]
        end = fields[2]
        name = None
        if len(fields) > 3:
            name = fields[3]

        newinterval = doc.createElement( "interval" )
        newinterval.setAttribute( "chrom", chrom )
        newinterval.setAttribute( "start", start )
        newinterval.setAttribute( "end", end )
        if name: newinterval.setAttribute( "name", name )

        newbed.appendChild( newinterval )

    xmlhead = '<?xml version="1.0" ?>'
    #return doc.toprettyxml(indent="  ").replace(xmlhead, '')
    return doc

def data_to_xml( infile ):
    """
    Reads the old cladimo output format and produces an XML format similar to the other sparse varieties.
    """

    dataset = None

    # create a mini document
    doc = Document()

    # create a new <bed> base element
    newbed = doc.createElement('bed')
    doc.appendChild(newbed)
    element_types = doc.createElement('types')
    types = {}
    newbed.appendChild(element_types)
    

    # create elements
    for line in infile:
        if line.startswith("#") or line.isspace():
            continue

        if line.startswith(">"): 
            if dataset:
                newbed.appendChild(dataset)
            dataset = doc.createElement('interval_set')
            line = line.lstrip(">")
            try:
                fields = line.split()
                dataset.setAttribute("chrom", fields[0])
                dataset.setAttribute("start", fields[1])
                dataset.setAttribute("end", fields[2])
                dataset.setAttribute("name", fields[3])
                #dataset.setAttribute("species", fields[4])
                splist = doc.createElement('species_list')
                for sp in fields[4].split(','):
                    spitem = doc.createElement('species_match')
                    spitem.setAttribute('species', sp)
                    splist.appendChild(spitem)
                dataset.appendChild(splist)

            except:
                pass

            continue

        try:
            fields = line.split()
            chrom = fields[0]
            start = fields[1]
            end = fields[2]
            name = None
            if len(fields) > 3:
                name = fields[3]
        except:
            continue

        newinterval = doc.createElement( "interval" )
        if chrom.find(".") > -1:
            fields = chrom.split(".")
            species, src = fields[0], fields[1]
            newinterval.setAttribute("species", species)
            newinterval.setAttribute("chrom", src)
        else:
            newinterval.setAttribute( "chrom", chrom )
        newinterval.setAttribute( "start", start )
        newinterval.setAttribute( "end", end )
        if name: 
            newinterval.setAttribute( "name", name )
            if name not in types:
                types[name] = 1
                newtype = doc.createElement("element_type")
                newtype.appendChild(doc.createTextNode(name))
                element_types.appendChild(newtype)
                #element_types.appendChild(doc.createTextNode(name))

        if dataset:
            dataset.appendChild( newinterval )
        else:
            newbed.appendChild( newinterval )

    return doc

def maf_to_xml( reader, name=None ):
    """
    Creates an XML representation of a MAF. Requires bx-python.
    <maf name="">
        <species_list>
            <species id="" />
                ...
        </species_list>
        <block score="" text_size="" ... {implicitly-defined attributes}>
            <component src="" start="" size="" strand="" text="" src_size="" />
            ...
        </block>
           ...
    </maf>

    This routine also deletes the xml header from doc.toprettyxml(). It must trip up the MAFbrowser somehow.
    """

    # create a mini document
    doc = Document()

    # create a new <maf> base element
    newmaf = doc.createElement('maf')
    doc.appendChild(newmaf)
    if name:
        newmaf.setAttribute("name", name)

    # create species elements
    newsplist = doc.createElement('species_list')
    newmaf.appendChild(newsplist)
    splist = {}

    # create block elements
    for block in reader:
        newblock = doc.createElement("block")
        newblock.setAttribute("score", str(block.score))
        newblock.setAttribute("text_size", str(block.text_size))
        # implicitly defined attributes
        for attr, setting in block.attributes.items():
            newblock.setAttribute( attr, str(setting) )

        newmaf.appendChild(newblock)

        for i,comp in enumerate( block.components ):
            newcomp = doc.createElement( "component" )
            # attempt to translate assembly, if needed
            #if comp.src.find('.') > 0:
                #sp,chrom = comp.src.split('.')
                #common_name = assembly_translation.translate(sp)
                #src = '.'.join(common_name, chrom)
                #srcsp = common_name
            #else:
                #src = assembly_translation.translate(comp.src)
                #srcsp = src
            this_src_sp = comp.src.split('.')[0]
            alt_name = assembly_translation.translate( this_src_sp )
            if alt_name != this_src_sp: 
                comp.src = comp.src.replace( this_src_sp, alt_name )

            newcomp.setAttribute( "src", comp.src )

            newcomp.setAttribute( "start", str( comp.start) )
            newcomp.setAttribute( "size", str( comp.size) )
            newcomp.setAttribute( "strand", str( comp.strand) )
            newcomp.setAttribute( "text", str( comp.text) )
            newcomp.setAttribute( "src_size", str( comp.src_size) )

            srcsp = assembly_translation.translate( comp.src.split('.')[0] )
            
            newcomp.setAttribute( "species", srcsp )

            newblock.appendChild(newcomp)

            if srcsp not in splist:
                newsp = doc.createElement( "species" )
                newsp.setAttribute( "id", srcsp )
                newsp.setAttribute( "order", str(i) )
                newsplist.appendChild( newsp )
                splist[srcsp] = newsp 
            else:
                prevorder = int(splist[srcsp].getAttribute( "order" ))
                if i > prevorder:
                    splist[srcsp].setAttribute( "order", str(i) )
    return doc

def pseudomaf_to_xml( fd, filename ):

    header = fd.next().lstrip()
    seq = ''.join(list(fd)).replace('\n','')
    
    doc = Document()

    newmaf = doc.createElement("maf")
    doc.appendChild(newmaf)

    # create single species
    newsplist = doc.createElement('species_list')
    newsp = doc.createElement( "species" )
    newsp.setAttribute( "id", filename )
    newsplist.appendChild( newsp )
    newmaf.appendChild(newsplist)
    # create single block
    newblock = doc.createElement("block")
    newblock.setAttribute("score", "0")
    newblock.setAttribute("text_size", str( len(seq) ) )
    # create single component
    newcomp = doc.createElement( "component" )
    newcomp.setAttribute( "src", filename )
    newcomp.setAttribute( "start", "0" )
    newcomp.setAttribute( "size", str( len( seq ) ) )
    newcomp.setAttribute( "strand", "+" )
    newcomp.setAttribute( "text", seq )
    newcomp.setAttribute( "src_size", str( len( seq ) ) )
    newcomp.setAttribute( "species", filename )
    newblock.appendChild(newcomp)
    newmaf.appendChild(newblock)

    return doc


