#!/usr/bin/env python2.6
"""
Convert assembly name abbreviations (rampant in MAF files) to common names for vertebrates, or a more readable abbreviation for
invertebrates (i.e. dm2 -> d_melanogaster}
"""

import re

arthropoda = {
"dm": "d_melanogaster",
"drosim": "d_simulans",
"drosec": "d_sechellia",
"droyak": "d_yakuba",
"droere": "d_erecta",
"droana": "d_ananassae",
"dp": "d_pseudoobscura",
"droper": "d_persimilis",
"drowil": "d_willistoni",
"drovir": "d_virilis",
"dromoj": "d_mojavensis",
"drogri": "d_grimshawi",
"anogam": "a_gambiae",
"apimel": "a_mellifera",
"tricas": "t_castaneum"
}

vertebrata = {
"hg": "human",
"pantro": "chimp",
"gorgor": "gorilla",
"ponabe": "orangutan",
"nomleu": "gibbon",
"papanu": "baboon",
"papham": "baboon",
"rhemac": "rhesus",
"macaque": "rhesus",
"caljac": "marmoset",
"tarsyr": "tarsier",
"micmur": "mouse_lemur",
"otogar": "bushbaby",
"tupbel": "treeshrew",
"saibol": "squirrel_monkey",
"mm": "mouse",
"rn": "rat",
"dipord": "kangaroo_rat",
"hetgla": "naked_mole_rat",
"cavpor": "guineapig",
"spetri": "squirrel",
"orycun": "rabbit",
"ochpri": "pika",
"vicpac": "alpaca",
"turtru": "dolphin",
"triman": "manatee",
"bostau": "cow",
"equcab": "horse",
"ailmel": "panda",
"felcat": "cat",
"canfam": "dog",
"susscr": "pig",
"cf": "dog",
"oviari": "sheep",
"myoluc": "microbat",
"ptevam": "megabat",
"erieur": "hedgehog",
"sorara": "shrew",
"loxafr": "elephant",
"procap": "rock_hyrax",
"echtel": "tenrec",
"dasnov": "armadillo",
"chohof": "sloth",
"maceug": "wallaby",
"mondom": "opossum",
"ornana": "platypus",
"galgal": "chicken",
"taegut": "zebra_finch",
"anocar": "lizard",
"xentro": "xenopus",
"tetnig": "tetraodon",
"fr": "fugu",
"gasacu": "stickleback",
"orylat": "medaka",
"danrer": "zebrafish",
"petmar": "lamprey" 
}

numeric_pattern = re.compile('\d*')

#class AssemblyTranslator:
translations = {}
translations.update(arthropoda)
translations.update(vertebrata)

#    @staticmethod
def translate( name ):
    non_numeric = re.sub(numeric_pattern, '', name.lower())
    if non_numeric in translations:
        return translations[non_numeric]

    return name

if __name__ == "__main__":
    import sys
    print translate(sys.argv[1])

