import cgi, urllib2
import cgitb; cgitb.enable()
from htmilf import *

def printfieldstuff( field ):
    print "<a name='%s'>" % field.name
    print 'name:'; printwww(field.name)
    print 'filename:'; printwww(field.filename)
    print 'value:'; printwww(field.value[:40])
    print 'file:'; printwww(field.file)
    if isinstance(field.file, file):
        print 'file has %d lines<br/>' % len(list(field.file))
    print 'isinstance(file)', isinstance(field.file, file), '<br/>'
    print 'type:'; printwww(field.type)
    print 'type_options:'; printwww(field.type_options)
    print 'disposition:'; printwww(field.disposition)
    print 'disposition_options:'; printwww(field.disposition_options)
    print '</a><br/>'


def print_all_fields( contents ):

    #### verbose info

    print "--------------------<br/>"
    print span("list:", style='font-style:italic; height: auto', _class='dumpty', width='80%', background_color='LightCyan', position='absolute', font_weight='bold'), '<br/>'
    for item in contents:
        print "<a href='#%s'>%s</a><br/>" % (item, item)
    for item in contents:
        field = contents[ item ]
        if isinstance(field, cgi.FieldStorage):
            printfieldstuff( field )
        else:
            for f in field: 
                print "<div style='padding-left: 50px;'>"
                printfieldstuff(f)
                print "</div>"
        print '<br/>'

    print "--------------------<br/>"
    print span("headers:", width='80%', background_color='LightCyan', position='absolute', font_weight='bold'), '<br/>'
    if contents.headers:
        for k,v in contents.headers.items():
            print span(cgi.escape(k),  font_weight='bold'), cgi.escape(v), '<br/>'


