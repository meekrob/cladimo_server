#!/usr/bin/env python2.6
"""
Maintains record of the pertinent directories used by the cladimo server designed by David King.  The
directories are returned to the calling script, almost always in cgi-bin, as relative paths so as to avoid
revealing system hierarchy in stack traces reported through the cgitb module.

Usage:
import cladimo_locale
locale = cladimo_locale.cladimoLocale()
locale.jobdir
"""

import subprocess,os
import site

#hostname = subprocess.check_output( ['hostname'] ).strip() # only in python2.7+
hostname = subprocess.Popen(['hostname'], stdout=subprocess.PIPE).communicate()[0].strip() # for python2.6

#: Qualified pathname of the cladimo server base directory (above htroot, cgi-bin, python, etc.)
if hostname == 'madman.local':
    basedir = os.path.realpath( os.path.expanduser('~david/bxcladimo') ) 
elif hostname == 'badger.bx.psu.edu':
    basedir = os.path.realpath( os.path.expanduser('~dcking/bxcladimo') ) 
else:
    raise "hostname not recognized"

def relpath(path):
    """
    Formulate the relative path of a directory (it will be relative to the calling script)
    """
    return os.path.relpath( os.path.join( basedir, path ) )

class cladimoLocale:
    """
    Provides the locale-related variables as class attributes.
    """
    values = {
        'badger.bx.psu.edu': {
            'bxpython': relpath('lib64/python2.6/site-packages'),
        },    
        'madman.local': {
            'bxpython': relpath('lib/python2.6/site-packages'),
        }
    }
    defaults = {
        #paths relative to the calling script
        'jobdir': relpath('jobdir'),
        'bindir':  relpath('bin'),
        'cladimopython': relpath('python')
    }

    def __init__(self):
        site.addsitedir( self.bxpython ) # # 'bx_python-0.7.1-py2.6-arch.egg'

    def __getattribute__(self, name):
        if name == 'hostname': return hostname
        if name in cladimoLocale.values[hostname]:
            return cladimoLocale.values[hostname][name]
        elif name in cladimoLocale.defaults:
            return cladimoLocale.defaults[name]
        else:
            print str(cladimoLocale.values[hostname])
            raise AttributeError

    def __getattr__(self, name): return self.__getattribute__(name)

