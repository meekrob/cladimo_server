import cgi, urllib2, sys
# beautification of HTML
def styleargs(args): 
    # format key value into key : value ;
    # convert to xml syntax (replace python-friendly substitutes with xml chars)
    return '; '.join(['%s: %s' % (k.replace('_','-'),v) for k,v in args.items()])

# python-agreeable versions of standard attributes for many tags
tagattr = ['id', '_class', 'dir', 'lang', 'style', 'title', 'xml_lang']

def tag( tagname, s, raw=True, **kwargs ):

    if raw:
        html = printable( s )
    else:
        html = s

    attributes = {}
    # extract the kwargs that are standard tag attributes
    for attr in tagattr:
        if attr in kwargs:
            attributes[attr] = kwargs[attr]
            del kwargs[attr]
            
    # append the remaining kwargs to the style attribute
    style = styleargs( kwargs )
    if 'style' in attributes:
        style = ';'.join( [attributes['style'], style] )
    attributes['style'] = style
    if len(attributes['style']) == 0: del( attributes['style'] )

    # make a tag='value' string
    tagattrstr = ' '.join( ["%s='%s'" % (tag,val) for tag,val in attributes.items()] )

    # change to xml syntax
    tagattrstr = tagattrstr.replace('_class', 'class')
    tagattrstr = tagattrstr.replace('xml_lang', 'xml:lang')

    return "<%s %s>%s</%s>" % ( tagname, tagattrstr, html, tagname )

def span( s, raw=True, **kwargs ):
    return tag('span', s, raw, **kwargs)

def div( s, raw=True, **kwargs ):
    return tag('div', s, raw, **kwargs)

def br():
    return '<br/>'

def printable(*args):
    s = [ cgi.escape( str(a) ) for a in args ]
    return '&nbsp;'.join( s )

def printwww(*args):
    s = [ cgi.escape( str(a) ) for a in args ]
    print '&nbsp;'.join( s ), '<br/>'

def bail(*html):
    for clause in html:
        print clause
    sys.exit(1)


htmlhead = """Content-Type: text/html\n
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />
%(title)s
%(style)s
</head>
<body>"""

htmlfoot = """</body>
</html>"""
