#!/usr/bin/env python
"""
Parse output from cladimo's gff output.

As default output, cladimo returns standard GFF lines pertaining to the reference sequence in an alignment,
but also provides additional information in the semicolon-delimited field.

"""
import sys
import shlex

def next_group( gffs ):
    last = None
    group = []
    gffs.sort()
    for gff in gffs:
            current = (gff.seqname, gff.start, gff.end)
            if current == last:
                group.append( gff )
            else:
                if group: yield group
                group = [gff]
            last = current
    if group: yield group


def next_mafblock( lines ):
    block = []
    for line in lines:
        if line.startswith('#'):
            continue
        if not line.isspace():
            try: gff = GFF(line, commasplit="True")
            except:
                print >>sys.stderr, "failed to gff %s" % line
                raise
            if gff.feature == 'maf_block':
                if block: yield block
                block = [gff]
            else:
                block.append( gff )
    if block: 
        yield block
        

def next_clm( gffs ):
    for group in next_group( gffs ):
        # line types
        pwm_matches = []
        clm = None
        mafblock = None
        # find the spanning range
        minleft = None    
        maxright = 0
        
        for gff in group:
            if gff.feature == 'pwm_match':
                pwm_matches.append( gff )
                left = gff.attributes[ 'column' ]
                right = left + gff.attributes[ 'width' ]
                maxright = max( right, maxright )
                if minleft == None: 
                    minleft = left
                else:
                    minleft = min( left, minleft )
            elif gff.feature == 'maf_block':
                mafblock = gff
            elif gff.feature == 'cladistic_motif':
                clm = gff

        if clm: yield { 'mafblock': mafblock, 'clm': clm, 'pwm_matches': pwm_matches, 'position': (minleft,maxright) }
        
def flatten(l):
    """
    Recursively expands stacked sequence types into a single, continuous list.
    """
    if not isinstance(l, (list, tuple)): return l
    out = []
    for item in l:
      if isinstance(item, (list, tuple)):
        out.extend(flatten(item))
      else:
        out.append(item)
    return out

def convert_type( val ):
    """
    Recursive function to convert values in a list or string to numeric types. String values are split on commas if found, thus
    initiating another application of convert_type on each of the members to attempt conversion of scalar values to float or
    int types.
    """
    if isinstance( val, list ): 
        for i in range( len( val ) ):
            val[i] = convert_type( val[i] )
    elif isinstance( val, tuple ):
        val = list(val)
        for i in range( len( val ) ):
            val[i] = convert_type( val[i] )
        return tuple(val)
    elif isinstance( val, str ) and val.find(',') >= 0:
        val = convert_type( val.split(',') )
    else:
        try: 
            fvalue = float( val )
            try:
                ivalue = int( val )
                if fvalue == ivalue: 
                    return ivalue
            except:
                return fvalue
        except:
            return val
    return val

class GFF:
    """
    General handling object for a line of GFF Gene Feature Format data.

    Standard fields include         Example
        seqname                     chr7
        source                      cladimo
        feature                     "maf_block", "pwm_match", "cladistic_motif"
        start                       [integer]
        end                         [integer]
        score                       [threshold between 0-1]
        strand                      +/-
        frame                       [012.] A '.' indicates that frame is not applicable.

        If there is a 9th field, it is assumed to be a ';'-delimited list of key-value pairs, which are parsed
        and stored as the dict "attributes" using shlex to parse quoted strings and whitespace. Some lists are
        passed through as comma-separated values and converted to a python list. See termfunc for details.
    """
    def __init__(self, src, **options):
        """
        GFF(src) can be initialized with a line (str) of input, tab-delimited. Or as a tuple.

        GFF(src, commasplit=True) will attempt to marshall comma-seperated strings in the 9th field as a list.
        """
        self.attributes = {}
        self.options = options
        if isinstance(src, str):
            text = src
            fields = text.split('\t')
        elif isinstance( src, (list, tuple) ):
            fields = src
        else:
            raise TypeError

        try:
            seqname, source, feature, start, end, score, strand, frame = fields[:8]
        except:
            print >>sys.stderr, "failed to parse %s:" % str(src), fields
            raise
        self.seqname = seqname
        self.source = source
        self.feature = feature
        self.start = int( start )
        self.end = int( end )
        self.score = float( score )
        self.strand = strand
        self.frame = frame
        if len( fields ) > 8: 
            attr_str = fields[8]
            for attr  in attr_str.split( ';' ):
                terms = shlex.split( attr.strip() )
                if not terms: 
                    continue

                key = terms.pop( 0 )
                self.attributes[ key ] = self.termfunc( terms )

    def write(self, fp):                    
        """
        Write the GFF object as a valid, tab-delimited line of GFF. Attributes are repacked into a semicolon-delimited list;
        """
        fp.write( ("%s %s %s %d %d %f %s %s " % (self.seqname, self.source, self.feature, self.start, self.end, self.score, self.strand, self.frame) ).replace(' ','\t') )
        self.write_attributes( fp )
        fp.write( "\n" )
    def write_attributes(self, fp):
        """
        Pack the attribute dict into a semicolon-delimited list.
        """
        hashlist = [ [i,j] for i,j in self.attributes.items() ]
        h = map( flatten, hashlist )
        h2 = [ map( str, item ) for item in h ]
        h3 = [' '.join(item) for item in h2 ] # god damnit
        attr_str = '; '.join( h3 )
        fp.write( attr_str )

    def __repr__(self):
        """
        return "<GFF %s %d %d %s %s %.3f %s>" % (self.seqname, self.start, self.end, self.feature, self.strand, self.score, str(self.attributes))
        """
        return "<GFF %s %d %d %s %s %.3f %s>" % (self.seqname, self.start, self.end, self.feature, self.strand, self.score, str(self.attributes))

    def __eq__(self, other):
        """
        Is self equal to other in the sort order defined by __cmp__.
        """
        if other is None: 
            return False
        else:
            return self.__cmp__(other) == 0
    def __cmp__(self, other):
        """
        Sort alphabetically by seqname, then numerically by start, end, then feature, then source.
        """
        sortorder = [cmp(self.seqname, other.seqname), cmp(self.start, other.start), cmp(self.end, other.end), cmp(self.feature, other.feature), cmp(self.source, other.source)]
        for comp in sortorder:
            if comp != 0: return comp

        return 0

    def termfunc(self, terms):
        """
        Attempts to convert the value field of a key-value pair in the semicolon-delimited list to native objects. It uses
        the function convert_type to attempt numeric conversions, and if commasplit=True, split comma-delimited fields into 
        lists and attempt numeric conversions on resultant strings.
        """
        
        if len( terms ) > 1: 
            return tuple( convert_type( terms ) )
        else:
            try:
                datum = terms[0]
            except:
                print >>sys.stderr, "what is a ", terms
                raise
            if 'commasplit' in self.options and self.options['commasplit'] == True:
                if datum.find(',') >= 0: 
                    return tuple( convert_type( datum.split(',') ) )
            return convert_type( datum )

class BED:
    """
    Simple class for BED - Browser Extensible Data

    Fields      Type
    chrom       str
    start       int
    end         int
    vec         []
    """
    def __init__(self, src, delim=None):
        """
        If src is a string, parse a line of potential BED data into a BED object. 
        If src is a list or tuple, it expects a sequence of chromosome, start and end, the latter two being converted to integers.
        Raises TypeError if src is not any of the above.
        Raises Error if there aren't enough resultant fields to map into chromosome, start and end.

        If there are more than three fields, convert_type is run on them and packed into the "vec" attribute using flatten.
        """
        if isinstance(src, str):
            text = src
            fields = text.split(delim)
        elif isinstance( src, (list, tuple) ):
            fields = src
        else:
            raise TypeError
        try:
            chrom, start, end = fields[:3]
        except:
            print >>sys.stderr, "failed to parse %s:" % str(src), fields
            raise
    
        self.chrom = chrom
        self.start = int( start )
        self.end = int( end )
        if len(fields) > 3:
            vec = fields[3:]
            self.vec = flatten( convert_type( vec ) )
        else:
            self.vec = []

    def write(self, fp):                    
        """
        Write a line of bed data, followed by a string representing the additional data in the forth field, compacted by flatten.
        """
        vec = ' '.join(map(str, flatten( self.vec )))
        fp.write( ('%s %d %d %s\n' % (self.chrom, self.start, self.end, vec) ) )

    def __repr__(self):
        return 'BED(%s)' % str( flatten( [self.chrom, self.start, self.end] + self.vec ) )
    def __eq__(self, other):
        if other is None: 
            return False
        else:
            return self.__cmp__(other) == 0
    def __cmp__(self, other):
        """
        Sort alphanumerically on chrom, then numerically on chromosome position (via start, end).
        """
        for comp in [cmp(self.chrom, other.chrom), cmp(self.start, other.start), cmp(self.end, other.end)]:
            if comp != 0: return comp

        order = [ cmp(a, b) for a,b in zip(self.vec, other.vec) ]

        for o in order:
            if o != 0: return o

        return 0


if __name__ == "__main__":
    for line in file( sys.argv[1] ):
        if line.isspace() or line.startswith('#'):
            continue
        print GFF(line, commasplit=True)
                
