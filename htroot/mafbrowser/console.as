import mafComponents.MafUploader;
import mx.controls.Alert;
import mx.managers.*;
import mx.utils.*;

[Bindable]
/**
* MafUploader used for a file upload of a MAF.
* @see MafUploader
**/
public var uploader:MafUploader;

import mx.managers.BrowserManager;
import mx.managers.IBrowserManager;
import mx.utils.URLUtil;

private var bm:IBrowserManager;

import mx.rpc.http.HTTPService;
import mx.rpc.events.ResultEvent;
import mx.rpc.events.FaultEvent;
[Bindable]
/**
* Object to house locale settings.
* @see #get_locale()
**/
public var locale:Object;
/**
* Load locale settings from a file.
* @see #locale
**/
public function get_locale(configURL:String="config.xml"):void
{
    var service:HTTPService = new HTTPService();
    service.url = configURL;
    service.addEventListener("result", function(event:ResultEvent):void {
        main_console.locale = event.result.MAFbrowserLocale;
        //parseBrowserLine();
        //Alert.show(main_console.locale.handlers.MAF_UPLOAD);
    });
    service.addEventListener("fault", function(event:FaultEvent):void {
        Alert.show("failed to read locale settings from " + service.url);
    });

    service.send();
}

/**
* Creates a new instance of MafUploader, parses the browser line, and loads locale settings from a file.
* @see #uploader
* @see #get_locale()
* @see #parseBrowserLine()
**/
public function initApp():void
{
    uploader = new MafUploader(null, complete, "maf", main_console);
    main_console.maf_uploader = uploader;
    parseBrowserLine();
    get_locale(); // read config.xml from current URL base
}
/**
* Loads parameters from the browser line, such as "mafName=something.maf&amp;mafTitle=someTitle"
* <p>
* This function uses the <code>uploader</code> MafUploader instance to perform the interface to the data on the server.
* </p>
* @see MafUploader#process_tagname()
* @see MafUploader#process_data_tagname()
**/
public function parseBrowserLine():void
{
    bm = BrowserManager.getInstance();
    bm.init(null, "MAFBrowser");
    var urlbase:String = bm.base;
    var serverName:String = URLUtil.getServerName(urlbase);
    var urlsubdirs:String = urlbase.replace('http://' + serverName + '/', '');

    var params:Object = null;
    var mafName:String = '';
    var mafTitle:String = '';
    var tag:String = '';
    if (bm.fragment)
    {
        params = URLUtil.stringToObject(bm.fragment, "&");
        mafName = params.mafName;
        mafTitle = params.mafTitle;
        tag = params.tag;
    }
    if (mafName)
    {
        // load a maf from the tag on the browser URL
        if (! main_console.config_is_loaded)
            main_console.addEventListener("ConfigLoaded", function(evt:Event):void {
                uploader.process_filename(mafName, main_console.mafxmlhandler, mafTitle);
            });
        else
            uploader.process_filename(mafName, main_console.mafxmlhandler, mafTitle);
    }

    if (tag)
    {
        if (! main_console.config_is_loaded)
        {
            main_console.addEventListener("ConfigLoaded", function(evt:Event):void {
                uploader.process_tagname(tag);
            });
        }
        else
        {
            uploader.process_tagname(tag);
        }
    }

}
/**
* Loader for a file upload.
**/
public function launchUpload():void
{
    CursorManager.setBusyCursor();
    uploader.browse(main_console.mafxmlhandler);
}
/**
* Removes the upload button upon file upload.
**/
public function complete():void
{
    uploadButton.visible = false;
}

