package mafComponents
{
    import mx.rpc.events.ResultEvent;
    import flash.net.SharedObject;
    import flash.events.*;
    import mx.events.*;
    import mx.managers.*;
    import mx.containers.*;
    import mx.controls.*;
    import mx.utils.*;
    import mx.collections.ArrayCollection;
    import spark.components.Label;
    import spark.components.HGroup;

    /**
    * Pop-up window for managing highlight styles for annotations in the text. 
    * <p>Pop-up is invoked through DataSet</p>
    * @see DataSet
    * @see DataSet#popup()
    **/
    public class DataClass extends TitleWindow
    {
        /**
        * Highlight variables read in from SharedObject.getLocal
        * @see SharedObject#getLocal
        **/
        public var local_vars:SharedObject;
        /**
        * ColorPicker component for choosing the highlight color of annotations
        **/
        public var colorpicker:ColorPicker;
        /**
        * ColorPicker component for choosing the border color of annotations
        **/
        public var bordercolorpicker:ColorPicker;
        /**
        * Array of individual element instances.
        **/
        public var elements:Array;
        /**
        * The class name of the dataset.
        **/
        public var classname:String;
        /**
        * A reference to the DataSet object associated with this set of annotations.
        **/
        public var dataset:DataSet;
        /**
        * The Button class which launches this component
        **/
        public var buttonref:ButtonPanelButton;
        /**
        * The button that closes this component
        **/
        public var closebutton:Button;
        /**
        * A brief text summary of the annotations.
        **/
        public var short_descript:Text;
        /**
        * Name of the annotations
        **/
        public var datasetName:Text;
        /**
        * Display name of the annotations.
        **/
        //public var displayClassname:Text;
        /**
        * Another display name of the annotations.
        **/
        public var displayName:TextInput;
        /**
        * Checkbox to toggle annotation visibility.
        **/
        public var visibleCheck:CheckBox;
        /**
        * Checkbox to toggle annotation fill color.
        **/
        public var fillCheck:CheckBox;
        /**
        * Set of colorblind-safe colors.
        **/
        public var safecolors:Array;
        /**
        * Constructor.
        **/
        public function DataClass(_classname:String="")
        {
            classname = _classname;
            title = classname;
            elements = new Array();
            local_vars  = SharedObject.getLocal("cladimo-highlights");
            showCloseButton = true;
            addEventListener(CloseEvent.CLOSE, close);
            setStyle("borderAlpha", 1);
            safecolors = new Array(
                {name : 'black', color : 0x000000 }, 
                {name : 'blue', color : 0x0072B2 },
                {name:'sky_blue', color: 0x56B4E9 },
                {name:'yellow', color:  0xF0E442},
                {name:'yellow2', color:  0xFFFF00},
                {name:'bluish_green', color:  0x009E73},
                {name:'vermillion', color:  0xD55E00},
                {name:'orange', color:  0xE69F00},
                {name:'orange2', color:  0xFFA500},
                {name:'reddish_purple', color:  0xCC79A7});
        }
        override protected function commitProperties():void
        {
            super.commitProperties();
            displayName.text = buttonref.label;
            //displayClassname.text = classname;
            short_descript.text = "has " + elements.length + " elements";
        }
        /**
        * Adds an Element to the manager.
        * @see DataSet#addDataElement()
        **/
        public function push(el:Element):void
        {
            elements.push(el);
            invalidateProperties();
        }
        /**
        * Propogates a color change to the Element instances
        * <p>For each Element, calls set_color(newColor) and redraw() on that Element.</p>
        * @see Element#set_color
        * @see Element#redraw
        **/
        public function set_color(newcolor:uint):void
        {
            local_vars.data["color-" +  classname] = newcolor;
            for each (var el:Element in elements)
            {
                el.set_color(newcolor);
                el.redraw();
            }
            buttonref.setStyle("contentBackgroundColor", newcolor); // must be a spark style
        }
        /**
        * Responds to a ColorPickerEvent, invoking set_color.
        * @see #set_color
        **/
        public function color_changer(cp:ColorPickerEvent):void
        {
            set_color(cp.currentTarget.selectedColor);
        }
        /**
        * Changes the elements' border color.
        **/
        public function set_border_color(newcolor:Number):void
        {
            for each (var el:Element in elements)
            {
                el.borderColor = newcolor;
                el.redraw();
            }
            //buttonref.setStyle("borderColor", newcolor); // halo obsolete
            buttonref.borderColor = newcolor;
        }
        /**
        * Responds to a ColorPickerEvent, invoking set_border_color.
        * @see #set_border_color
        **/
        public function border_color_changer(cp:ColorPickerEvent):void
        {
            set_border_color(cp.currentTarget.selectedColor);
        }
        /**
        * Removes this component through the PopUpManager
        * @see PopUpManager#close
        **/
        public function close(evt:CloseEvent):void
        {
            PopUpManager.removePopUp(this);
            //buttonref.addEventListener(MouseEvent.CLICK, dataset.popup)
        }
        /**
        * Rename annotations
        **/
        public function changeDisplayName(evt:Event):void
        {
            buttonref.label = displayName.text;
        }
        /**
        * Sets the fill color on each Element
        **/
        public function setfill(evt:MouseEvent):void
        {
            var newcolors:*;
            var newalphas:*;
            var fillAlpha:uint = 1;
            if (fillCheck.selected != true) fillAlpha = 0;
            for each (var el:Element in elements) 
            {
                /** non-spark 
                if (fillCheck.selected == true)
                {
                    newcolors = [el.highlightColor, el.highlightColor, el.highlightColor, el.highlightColor];
                    newalphas = [ 0.6,0.4,0.75,0.65 ]
                }
                else
                {
                    newcolors = [0xffffff, 0xcccccc, 0xffffff, 0xeeeeee];
                    newalphas = [0,0,0,0];
                } 

                el.setStyle("fillAlphas", newalphas);
                **/

                el.setStyle("contentBackgroundColor", el.highlightColor); // spark style
                el.setStyle("contentBackgroundAlpha", fillAlpha); // spark style
            }
            buttonref.setStyle("contentBackgroundColor", el.highlightColor); // spark style
            //buttonref.setStyle("fillAlphas", newalphas);
            buttonref.setStyle("contentBackgroundAlpha", fillAlpha); // spark style
        }
        /**
        * Sets the visibility of each element to the value of visibleCheck.selected
        * @see #visibleCheck
        **/
        public function setvisibility(evt:MouseEvent):void
        {
            var el:Element;
            for each (el in elements) el.visible = visibleCheck.selected;
            if (visibleCheck.selected == true)
                buttonref.setStyle("color", 0);
            else
                buttonref.setStyle("color", 0xFFFFFF);
        }
        private var _fillColorLabel:spark.components.Label;
        public function get fillColorLabel():spark.components.Label { return _fillColorLabel; }
        private var _borderColorLabel:spark.components.Label;
        public function get borderColorLabel():spark.components.Label { return _borderColorLabel; }
        /**
        * In addition to super(), handles datasetName, visibleCheck, fillCheck, displayName, displayClassname, short_descript, colorpicker, and bordercolorpicker
        * <p>Child components<br/>
        * <ul>
        * <li>datasetName - Text</li>
        * <li>visibleCheck - CheckBox</li>
        * <li>fillCheck - CheckBox</li>
        * <li>displayName - TextInput</li>
        * <li>displayClassname - Text</li>
        * <li>short_descript - Text</li>
        * <li>colorpicker - ColorPicker</li>
        * <li>bordercolorpicker - ColorPicker</li>
        * </ul>
        * </p>
        * @see #datasetName
        * @see #visibleCheck
        * @see #fillCheck
        * @see #displayName
        * @see #displayClassname
        * @see #short_descript
        * @see #colorpicker
        * @see #bordercolorpicker
        **/
        override protected function createChildren():void
        {
            super.createChildren();
            if (! datasetName)
            {
                datasetName = new Text();
                addChild(datasetName);
                datasetName.text = dataset.name;
            }
            if (! visibleCheck)
            {
                visibleCheck = new CheckBox();
                visibleCheck.label = "visible";
                visibleCheck.selected = true;
                addEventListener(MouseEvent.CLICK, setvisibility);
                addChild(visibleCheck);
            }
            if (! fillCheck)
            {
                fillCheck = new CheckBox();
                fillCheck.label = "fill";
                fillCheck.selected = true;
                addEventListener(MouseEvent.CLICK, setfill);
                addChild(fillCheck);
            }
            if (! displayName)
            {
                displayName = new TextInput();
                displayName.addEventListener(Event.CHANGE, changeDisplayName);
                addChild(displayName);
            }
            /*if (! displayClassname)
            {
                displayClassname = new Text();
                addChild(displayClassname);
            }*/
            if (! short_descript)
            {
                short_descript = new Text();
                addChild(short_descript);
            }
            if (! _infoRow0)
            {
                _infoRow0 = new HGroup();
                addChild(_infoRow0);
            }
            if (! colorpicker)
            {
                colorpicker = new ColorPicker();
                colorpicker.dataProvider = safecolors;
                colorpicker.colorField = "color";
                colorpicker.labelField = "name";
                colorpicker.selectedColor = elements[0].highlightColor;
                colorpicker.addEventListener(ColorPickerEvent.CHANGE, color_changer);
                _infoRow0.addElement(colorpicker);
            }
            if (! _fillColorLabel)
            {
                _fillColorLabel = new spark.components.Label();
                _fillColorLabel.text = "Fill color";
                _infoRow0.addElement(_fillColorLabel);
            }
            if (! _infoRow1)
            {
                _infoRow1 = new HGroup();
                addChild(_infoRow1);
            }
            if (! bordercolorpicker)
            {
                bordercolorpicker = new ColorPicker();
                bordercolorpicker.dataProvider = safecolors;
                bordercolorpicker.colorField = "color";
                bordercolorpicker.addEventListener(ColorPickerEvent.CHANGE, border_color_changer);
                _infoRow1.addElement(bordercolorpicker);
            }
            if (! _borderColorLabel)
            {
                _borderColorLabel = new spark.components.Label();
                _borderColorLabel.text = "Border color";
                _infoRow1.addElement(_borderColorLabel);
            }
        }
        private var _infoRow0:HGroup;
        private var _infoRow1:HGroup;
    }
}

