/* Block:VBox
*   tracks:Array 
*   addTrack(newText:String="")
*
*/
package mafComponents
{
    import flash.geom.Rectangle;
    import flash.geom.Point;
    import mx.events.*;
    import flash.display.*;
    import flash.text.*;
    import mx.core.EdgeMetrics;
    import mx.core.UIComponent;
    import mx.controls.Alert;
    import mx.controls.Text;
    import mx.controls.TextArea;
    import mx.containers.*;
    import mx.rpc.events.ResultEvent;
    import flash.events.*;
    import mx.utils.ObjectUtil;
    import mafComponents.*;
    import mafComponents.events.*;

    
    /**
    * Dispatched by the Block's MiniBlock, after MiniBlock.draw() has finished.
    * @see MiniBlock#draw()
    **/
    [Event(name="MiniBlockDrawn", type="mx.events.Event")]

    /**
     * The <code>Block</code> represents a single MAF block of data, and serves as container for <code>Track</code> and <code>ComponentTrack</code> 
     * representations of such.
     *
     * <p><code>Blocks</code> are arrayed horizontally inside the <code>MafViewer</code>'s <code>HBox</code>.
     * </p>
    **/
    public class Block extends VBox
    {
        [Deprecated(replacement="track_height")]
        public var single_base_height:int; // pixels

        [Deprecated(replacement="char_width")]
        public var single_base_width:int; // pixels

        public var track_width:int; //pixels
        [Deprecated]
        public var block_height:int;//pixels
        public var msg:TextArea;
        /**
        * The container for this component. The actual container might be a sub-component of the BlockScroller.
        **/
        public var scroller:BlockScroller;
        private var _selection:IBlockSelection;

        [Deprecated(replacement="SpacerBlock")]
        public var is_spacer_block:Boolean;
        private var _text_size:int=0;
        private var _tracks:Array;

        private var _miniblock:MiniBlock = null;
        private var _miniblockChanged:Boolean = false;
        /**
        * A link to the mini component drawn on the <code>MafGraph</code>.
        *
        * @see MafGraph
        **/
        public function set miniblock(value:MiniBlock):void
        {
            _miniblock = value;
            _miniblockChanged = true;
            invalidateProperties();
        }
        public function get miniblock():MiniBlock
        {
            return _miniblock;
        }

        /**
        * The number of the component's <code>Track</code> or <code>ComponentTrack</code> children. 
        **/
        public function get ntracks():int
        {
            if (_tracks) return _tracks.length;
            return 0;
        }

        private var newTracks:Array;
        private var newTracksChanged:Boolean = false;

        public var drawRulers:Boolean;

        private var apparent_left_padding:Number = 0;
        /**
        * Constructor. Creates a <code>Block</code> instance.
        **/
        public function Block()
        {
            super();
            addEventListener(ChildExistenceChangedEvent.CHILD_ADD, refresh_tracks);
            horizontalScrollPolicy = "off";
            verticalScrollPolicy = "off";
            //setStyle("paddingLeft",-2);
            //setStyle("paddingRight",-2);
            //setStyle("paddingLeft", 0);
            //setStyle("paddingRight", 0);
            //setStyle("paddingLeft", 0);
            setStyle("paddingRight", 0);
            setStyle("leading",0);
            setStyle("borderVisible", true);
            setStyle("verticalGap", 0);
            setStyle("HorizontalGap", 0);
            setStyle("contentBackgroundAlpha", 0);
            setStyle("borderStyle", "solid");
            drawRulers = true;

            newTracks = new Array();
            _tracks = new Array();
        }
        private var _mouseDownEvt:TrackMouseEvent = null;
        /**
        * Interprets <code>TrackMouseEvents</code> dispatched by the component's <code>Track</code> and <code>ComponentTracks</code>.
        * @see #signalBlockSelection
        **/
        protected function handleTrackMouseEvents(evt:TrackMouseEvent):void
        {
            trace(name + '.hearing', evt.type, evt.currentTarget.name, evt.target.name, evt.textIndex);
            if (evt.charBounds)
            {
                if (evt.type == TrackMouseEvent.CLICK)
                {
                    signalBlockSelection(evt);
                }
                else if (evt.type == TrackMouseEvent.MOUSE_DOWN)
                {
                    _mouseDownEvt = evt;
                }
                else if ((evt.type == TrackMouseEvent.MOUSE_OUT) || (evt.type == TrackMouseEvent.MOUSE_UP))
                {
                    if (_mouseDownEvt) // user has the mouse button down: is sweeping over the block
                    {
                        signalBlockSelection(evt);
                    }
                }
                var charBounds:Rectangle = evt.charBounds;
                var targetTrack:Track = Track(evt.target);
                trace("charBounds:", charBounds.x, charBounds.y, charBounds.width, charBounds.height);
                trace("stageX,Y  :", evt.stageX, evt.stageY);
            }
        }
        /**
        *   Dispatched when <code>TrackMouseEvents</code> lead to a selection in the <code>Block</code>.
        *
        *   <p>Should be called in two cases:
        *   <ol>
        *   <li>The user has CLICKed a single character on a track.</li>
        *   <li>The user has completed, or is engaged in, a sweeping selection.</li>
        *   </ol>
        *   Situation 2 is distinguishible from 1 by the presence of _mouseDownEvt.</p>
        **/
        protected function signalBlockSelection(evt:TrackMouseEvent):void
        {
            var blockArea:Rectangle;
            var blockCoordinates:Rectangle;
            // Compute Rectangles and dispatch the event.
            var mouseCurrentTrack:Track = Track(evt.target);
            var mouseCurrentTrackIndex:int = tracks.indexOf(mouseCurrentTrack);

            var blockCoords:Rectangle = new Rectangle(evt.textIndex, mouseCurrentTrackIndex, 1, 1);
            var blockBounds:Rectangle = evt.charBounds.clone();
            var mouseCurrentGlobalPoint:Point = mouseCurrentTrack.localToGlobal(new Point(blockBounds.x, blockBounds.y));
            var mouseCurrentBlockPoint:Point = globalToLocal(mouseCurrentGlobalPoint);
            blockBounds.x = mouseCurrentBlockPoint.x;
            blockBounds.y = mouseCurrentBlockPoint.y;

            blockCoordinates = blockCoords;
            blockArea = blockBounds;

            if (_mouseDownEvt)
            {
                var mouseDownTrack:Track = Track(_mouseDownEvt.target);
                var mouseDownTrackIndex:int = tracks.indexOf(mouseDownTrack);

                var blockDownCoords:Rectangle = new Rectangle(_mouseDownEvt.textIndex, mouseDownTrackIndex, 1, 1);
                var blockDownBounds:Rectangle = _mouseDownEvt.charBounds.clone();
                var mouseDownGlobalPoint:Point = mouseDownTrack.localToGlobal(new Point(blockDownBounds.x, blockDownBounds.y));
                var mouseDownBlockPoint:Point = globalToLocal(mouseDownGlobalPoint);
                blockDownBounds.x = mouseDownBlockPoint.x;
                blockDownBounds.y = mouseDownBlockPoint.y;

                blockCoordinates = blockCoordinates.union(blockDownCoords);
                blockArea = blockArea.union(blockDownBounds);
            }

            var eventType:String = "";

            if (evt.type == TrackMouseEvent.CLICK) { eventType = BlockSelectionEvent.BLOCK_SELECTION_FINISH; }
            if (evt.type == TrackMouseEvent.MOUSE_UP) { eventType = BlockSelectionEvent.BLOCK_SELECTION_FINISH; }
            if (evt.type == TrackMouseEvent.MOUSE_OUT) { eventType = BlockSelectionEvent.BLOCK_SELECTION_DRAW; }
            dispatchEvent(new BlockSelectionEvent(eventType, blockCoordinates, blockArea));
                
            // reset if selection finished
            if (evt.type == TrackMouseEvent.MOUSE_UP) 
            {  
                _mouseDownEvt = null;
            }
        }

        /**
        * Component-specific ops: adds new Tracks if they have been added by the datasource.
        *
        * <p>
        * When a <code>Track/ComponentTrack</code> is added by the datasource, it is sent to an internal array and the <code>invalidateProperties()</code>
        * method is called. During <code>commitProperties()</code>, the actual <code>Track</code> is added to the display list and to 
        * the <code>tracks</code> variable,
        * and a <code>"ComponentTracksChanged"</code> event is created and dispatched.
        * </p>
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (newTracksChanged)
            {
                do 
                {
                    var t:Track = newTracks.pop();
                    //var t:ComponentTrack = new ComponentTrack(newTrack.seq_src, newTrack.seq_start, newTrack.src_size, newTrack.seq_size, newTrack.strand, newTrack.newText);
                    // add new track to the beginning of each list
                    addChildAt(t,0);
                    tracks.unshift(t);

                    var trackMouseEvents:Array = [
                            TrackMouseEvent.CLICK,
                            TrackMouseEvent.MOUSE_UP,
                            TrackMouseEvent.MOUSE_DOWN//,
                            //TrackMouseEvent.MOUSE_MOVE,
                            //TrackMouseEvent.MOUSE_OUT 
                    ];

                    /*for each (var evtType:String in trackMouseEvents)
                    {
                        t.addEventListener(evtType, handleTrackMouseEvents);
                    }*/

                    // does this shit still belong here?
                    var nbDim:TextLineMetrics = t.measureText(t.text);
                    track_width = Math.max(track_width, nbDim.width);
                    /**
                    block_height += nbDim.height;
                    nbDim = t.measureText("A");
                    single_base_width = nbDim.width;
                    single_base_height = nbDim.height;
                    **/

                    /*track_width = Math.max(track_width, t.track_width);
                    block_height += t.single_base_height; // doesn't work
                    single_base_width = t.single_base_width;*/
                    //return t;


                } while (newTracks.length > 0);
                find_track_text_size();
                newTracksChanged = false;
                dispatchEvent(new Event("ComponentTracksChanged"));
            }
            if (_text_size_changed)
            {
                trace(name + ".commitProperties() -> _text_size_changed");
                _text_size_changed = false;
                invalidateSize();
            }

            if(_miniblockChanged)
            {
                dispatchEvent(new Event("MiniBlockChanged"));
                _miniblockChanged = false;
            }
        }
        public function slice(col_start:int=0, row_start:int=0, ncols:int=0, nrows:int=0):Block
        {
            if (nrows==0) nrows = ntracks;
            if (ncols==0) ncols = text_size;
            var newBlock:Block = new Block();
            for (var t:int=row_start; t < row_start + nrows; t++)
            {
                trace(name + ':' + _tracks[t].name + ':-slice(' + col_start + ', ' + ncols + ')'); 
                var newTrack:Track = newBlock.addTrack(_tracks[t].slice_text(col_start, ncols));
                trace(name + ':' + newTrack.name + "`" + newTrack.text + "'");
                //var nbDim:TextLineMetrics = newTrack.measureText(newTrack.text);

                /*
                // newTrack hasn't been initialized yet, substitute with the current?
                var nbDim:TextLineMetrics = _tracks[t].measureText(newTrack.text);
                newBlock.track_width = Math.max(newBlock.track_width, nbDim.width);
                newBlock.block_height += nbDim.height;

                // same problem with initialization
                nbDim = _tracks[t].measureText("A");
                newBlock.single_base_width = nbDim.width;
                newBlock.single_base_height = nbDim.height;
                //newBlock.addChild(newTrack);
                */
            }
            return newBlock;
        }
        /**
        * Returns the index of the component in the <code>BlockScroller</code>.
        **/
        public function getIndex():int
        {
            return scroller.blocks.indexOf( this );
        }

        /**
        * Returns the full text of the <code>Track</code>s contained within.
        **/
        public function get text():String
        {
            var txt:String = '';
            trace(name + '.get text():', ntracks, newTracks.length);
            if (_tracks)
            {
                for (var t:int=0; t < ntracks; t++)
                {
                    txt += _tracks[t].text;
                    if (t < (ntracks-1)) txt += "\n";
                }
            }
            // Have to add this as a workaround to the delayed track-addition model
            // until I can get BlockSelection to work by NOT creating a new (but never added to the displayList) Block.
            else if (newTracks)
            {
                for (var tt:int=0; tt < newTracks.length; tt++)
                {
                    txt += newTracks[tt].text;
                    if (t < (newTracks.length-1)) txt += "\n";
                }
            }
            return txt;
        }
        /**
        * Returns a set of lines corresponding to the BlockSelection area.
        *
        * @param area Identifies the selected area by indices, using a Rectangle object.
        * @see IBlockSelection
        **/
        public function getSubText(area:Rectangle):Array
        {
            trace(name + '.getSubText(' + area + ')');
            var lines:Array = new Array();
            for (var row_i:int = area.y; 
                 row_i < area.bottom;  
                 row_i++)
            {
                var slicedText:String = _tracks[row_i].slice_text(area.x, Math.max(area.width + 1, 1));  // add 1 to width to get: inclusive x range
                lines.push(slicedText);
            }
            return lines;
        }
        /**
         * Returns a set of genome positions corresponding to a square area, as specified by SparkBlockSelection
         *
         * <p>Implemented by calling <code>genomeCoordinatesForRange()</code> on every <code>ComponentTrack</code> specified by the <code>selectionRect</code>.</p>
         * @see ComponentTrack#genomeCoordinatesForRange
        **/
        public function getSquareCoordinates(selectionRect:Rectangle):Array
        {
            trace(name + '.getSquareCoordinates', selectionRect, selectionRect.x, selectionRect.right);
            var lines:Array = new Array();
            for (var row_i:int = selectionRect.y; 
                 row_i <= selectionRect.bottom;  // row_i less than or EQUAL TO means: inclusive y range
                 row_i++)
            {
                if (_tracks[row_i] is ComponentTrack)
                {
                    var genomePosition:Object = _tracks[row_i].genomeCoordinatesForRange(selectionRect.x, selectionRect.right);
                    for (var propname:String in genomePosition)
                    {
                        trace('        genomePosition.' + propname + '`' + genomePosition[propname] + '\'');
                    }
                    genomePosition.species = _tracks[row_i].species;
                    lines.push(genomePosition);
                    trace('    ' + genomePosition);
                }
            }
            return lines;
        }
        /**
        * The Array of Track or ComponentTracks in the component.
        **/
        public function get tracks():Array
        {
            return _tracks;
        }
        private function refresh_tracks(evt:ChildExistenceChangedEvent):void
        {
            return;
            trace(name + ".refresh_tracks called");
            _tracks = new Array();
            for (var i:int=0; i<numChildren; i++)
            {
                var obj:Object = getChildAt(i);
                if (obj is Track) _tracks.push(obj);
            }
        }

        private var _mafviewer:MafViewer;
        /**
        * A read-only link to the containing MafViewer.
        **/
        public function get viewer():MafViewer
        {
            if (! _mafviewer) _mafviewer = get_viewer();
            return _mafviewer;
        }
        /**
        * Traverses parent and parent.parent until finding the <code>MafViewer</code> container.
        **/
        public function get_viewer():MafViewer
        {
            var obj:Object = parent;
            while (!(obj is MafViewer))
            {
                obj = obj.parent;
            }
            return obj as MafViewer;
        }

        public function get selection():IBlockSelection { return _selection; }

        /**
        * Attaches the component to an IBlockSelection instance.
        *
        * @see IBlockSelection
        **/
        public function set selection(s:IBlockSelection):void
        {
            _selection = s;
            _selection.watch(this);
        }
        /**
        * @private
        **/
        public function set draggable_tracks(d:Boolean):void
        {
            if (d)
            {
                for each (var t:Track in _tracks) 
                {
                    t.ta.buttonMode = true;
                    t.ta.useHandCursor = true;
                    t.draggable = d;
                }
            }
        }
        override protected function measure():void
        {
            //trace(name + ".measure()");
            super.measure();
            var total_height:int = 0;
            var max_width:int = 0;
            for each (var t:Track in _tracks)
            {
                total_height += Math.max(t.height, 16);
                max_width = Math.max(t.getExplicitOrMeasuredWidth(), max_width);
            }
            measuredHeight = total_height;
            /*if (max_width > 0)
            {
                for each (t in _tracks)
                {
                    t.width = max_width;
                    trace("    ", t.name + ".gEMW() =", t.getExplicitOrMeasuredWidth());
                }
            }*/
            /*for (var i:int=0; i < numChildren; i++)
            {
                var obj:UIComponent = UIComponent(getChildAt(i));
                trace("    ", obj.name + ".gEMW() =", obj.getExplicitOrMeasuredWidth());
            }
            trace(name + ".measure(): measuredWidth", measuredWidth, "measuredHeight", measuredHeight, "max_width", max_width);*/
        }

        /**
        * Places <code>Track</code>s as an <code>HBox</code>, but places <code>IBlockSelection</code> components according to their internal position.
        *
        * @see IBlockSelection
        * @see BlockSelection
        * @see SparkBlockSelection
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            // Get information about the container border area. 
            // The usable area of the container for its children is the 
            // container size, minus any border areas.
            //var vm:EdgeMetrics = viewMetricsAndPadding;

            var gap:Number = getStyle("verticalGap");
            var yOfComp:Number = 0;
            //var obj:UIComponent;

            var maxTrackWidth:Number = 0;
            for each (var track:Track in tracks)
            {
                maxTrackWidth = Math.max(track.getExplicitOrMeasuredWidth(), maxTrackWidth);
            }
                
            trace(name + ".updateDisplayList()");
            for (var i:int = 0; i < numChildren; i++)
            {
                var obj:UIComponent = UIComponent(getChildAt(i));
                //trace("    " + typeof(obj) + ":" + i + ":" + obj);
                var objHeight:Number = obj.getExplicitOrMeasuredHeight();
                if (obj is IBlockSelection)
                {
                    var s:IBlockSelection = obj as IBlockSelection;
                    s.move(s.alt_x - 1, s.alt_y);
                    //s.height = s.alt_height;
                    //s.width = s.alt_width;
                    //s.setActualSize(s.alt_width, s.alt_width);
                }
                else
                {
                    obj.move(obj.x, yOfComp);
                    obj.setActualSize(maxTrackWidth+7, objHeight);
                    yOfComp = yOfComp + objHeight + gap;
                }
            }

            if (drawRulers)
            {
                var horizontalLineColorTop:uint = 0x8bff8b; 
                var horizontalLineColorBot:uint = 0x008800;
                if ((! (this is SpacerBlock)) && char_width && track_height)
                {
                    var xBase:Number = apparent_left_padding;
                    var yBase:Number = 3;

                    var apparent_char_width:Number = (unscaledWidth-xBase) / text_size;
                    var geW:Number = getExplicitOrMeasuredWidth();
                    //trace(name + '.updateDisplayList():', char_width, apparent_char_width, unscaledWidth, geW);
                    graphics.clear();
                    for (var vrule:int=0; vrule < text_size; vrule+=5)
                    {
                        if ((vrule % 10) == 0)
                        {
                            graphics.lineStyle(1, 0x0000FF, 0.125);
                        }
                        else
                        {
                            graphics.lineStyle(1, 0xFF0000, 0.125);
                        }

                        // vertical line from top to bottom
                        var xOff:Number = apparent_char_width * vrule; 
                        graphics.moveTo(xOff + xBase, yBase);
                        graphics.lineTo(xOff + xBase, unscaledHeight);
                    }

                    for (var hrule:int=0; hrule < ntracks; hrule++)
                    {
                        // should go through the top of the text
                        yBase  = 4;
                        var yOff:Number = track_height * hrule;
                        graphics.lineStyle(1, horizontalLineColorTop, 0.25);
                        graphics.moveTo(xBase, yOff + yBase);
                        graphics.lineTo(unscaledWidth, yOff + yBase);
                        // should go through the base of the text
                        yBase = 2;
                        yOff = track_height * (hrule+1);
                        graphics.lineStyle(1, horizontalLineColorBot, 0.25);
                        graphics.moveTo(xBase, yOff - yBase);
                        graphics.lineTo(unscaledWidth, yOff - yBase);
                    }
                }
            }
        }
        public function get last_track():Track
        {
            if (tracks) return tracks[tracks.length-1];
            return null
        }

        public function get reference_species():String
        {
            return scroller.reference_species;
        }

        public function get_track_for_interval(interval:Object):ComponentTrack
        {
            //trace("        Block.get_track_for_interval:", interval.@start);
            var species:String = interval.@species;
            if (!species) species = reference_species;

            for each (var track:Track in tracks)
            {
                if (track is ComponentTrack)
                {
                    var trackbounds:Object = ComponentTrack(track).bounds;
                    //trace("             species", species, "vs trackbounds.species", trackbounds.species);
                    if (species == trackbounds.species)
                    {
                        if ((trackbounds.seq_start < Number(interval.@end)) && (Number(interval.@start) < trackbounds.seq_end))
                            return ComponentTrack(track);
                        else
                            return null;
                    }
                }
            }
            return null;
        }

        /**
        * Return an array of all track bounds.
        *
        * @see ComponentTrack#bounds
        **/
        public function get bounds():Array
        {
            var r:Array = new Array();
            for each (var track:Track in tracks)
            {
                if (track is ComponentTrack) r.push(ComponentTrack(track).bounds);
                else r.push(null);
            }
            return r;
        }

        /**
        * Creates an <code>EmptyTrack</code> for a species/sequence that is not aligned to the current MAF block.
        *
        * @param forSource The species or sequence name of the anticipated component that is missing.
        **/
        public function addEmptyTrack(forSource:String):void
        {
            var newTrack:EmptyTrack = new EmptyTrack(forSource);
            newTracks.push(newTrack);
            newTracksChanged = true;
            invalidateProperties();
            invalidateSize();
            invalidateDisplayList();
        }

        /**
        * Creates a <code>ComponentTrack</code>, called by <code>MafXML</code>.
        *
        * <p>
        * The <code>ComponentTrack</code> is created, but not added to the display list until the component goes through <code>commitProperties()</code>.
        * Perhaps this is the cause of a major lag during initialization of larger MAFs.
        * </p>
        * @param _seq_src Usually the species plus chromosome, joined by a '.', like mouse.chr7
        * @param _seq_start Chromosome/sequence start position.
        * @param _seq_end Chromosome/sequence end position.
        * @param _seq_size Chromosome/sequence total size. Used to calculate positions on negative strands.
        * @param _strand The strand is positive '+' or negative '-'
        * @param _newText The alignment text [ACGT-].
        * @see ComponentTrack
        * @see MafXML
        **/
        public function addComponentTrack(_seq_src:String, _seq_start:int, _src_size:int, _seq_size:int, _strand:String, _newText:String):void
        {
            /*var newTrack:Object = {  seq_src : _seq_src,
                    seq_start : _seq_start,
                    src_size  : _src_size,
                    seq_size  : _seq_size,
                    strand    : _strand,
                    newText   : _newText
                };*/
            var newTrack:ComponentTrack = new ComponentTrack(_seq_src, _seq_start, _src_size, _seq_size, _strand, _newText);

            newTracks.push(newTrack);
            newTracksChanged = true;
            invalidateProperties();
            invalidateSize();
            invalidateDisplayList();
        }
        public function addTracks(track_strings:Array):void
        {
            for each (var track_string:String in track_strings)
                addTrack(track_string);
        }
        public function get track_height():Number
        {
            if (tracks)
                return tracks[0].height;
            return 0;
        }
        public function get text_size():int
        {
            return _text_size;
        }
        private var _text_size_changed:Boolean = false;
        private function set_text_size(val:int):void
        {
            if (val != _text_size)
            {
                _text_size = val;
                trace("~Block[", name, "].set_text_size(", _text_size, ")");
                dispatchEvent(new Event("TextSizeChanged"));
                _text_size_changed = true;
                invalidateProperties();
            }
        }
        public function find_track_text_size():void
        {
            var textSize:int = 0;
            if (_tracks)
            {
                for each (var t:Track in _tracks)
                {
                    textSize = Math.max(t.text.length, textSize);
                }
            }
            set_text_size(textSize);
        }
        /**
        * The width of a character in the <code>Block</code>.
        *
        * <p>Gives the value of <code>_tracks[0].char_width</code> or 0 if there are no tracks or there was an error.</p>
        * @see Track#char_width
        **/
        public function get char_width():Number
        {
            try 
            {
                if (_tracks) return _tracks[0].char_width;
            }
            catch (e:Error)
            {
                trace(name + '.get char_width:', e);
            }
            return 0;
        }
        /**
        * Gives the measured (or actual) width of a character computed by <code>getExplicitOrMeasuredWidth()</code> / <code>text_size</code>.
        *
        * @see #text_size
        **/
        public function get apparent_char_width():Number
        {
            try // some things might not be initialized or loaded, so return 0 instead of error
            {
                var ts:Number = text_size;
                var geW:Number = getExplicitOrMeasuredWidth() - apparent_left_padding;
                return geW/ts;
            }
            catch (e:Error)
            {
                return 0;
            }
            return 0;
        }
        /**
        * Returns the height of a character in the first track.
        *
        * @see Track#char_height
        **/
        public function get char_height():Number
        {
            if (tracks) return tracks[0].char_height;
            return 0;
        }
        public function addTrack(newText:String=""):Track
        {
            var newTrack:Track = new Track(newText);

            newTracks.push(newTrack);
            newTracksChanged = true;
            invalidateProperties();
            invalidateSize();
            invalidateDisplayList();
            return newTrack;
        }
        /**
        * Gives the coordinates (in indices) of a <code>Point</code> representing a pixel location, i.e. that of the mouse.
        *
        * @param point The <code>Point</code>, in pixels, to translate into track and text offsets.
        * @return A <code>Point</code>, in indices, representing the text offset (x) and track offset (y) under the pixel point.
        **/
        public function coord_under_point(point:Point):Point
        {  
            return new Point(Math.floor(point.x / apparent_char_width),
                             Math.floor(point.y / track_height));
        }
        /**
        * Gives the Rectangle pertaining to the character under the pixel location.
        *
        * @param point The <code>Point</code>, in pixels, to translate into track and text offsets.
        * @return A <code>Rectangle</code> surrounding the character under the pixel location.
        **/
        public function coordRect_under_point(point:Point):Rectangle
        { // values are pixels
            var coord:Object = coord_under_point(point);
            return new Rectangle(coord.x*apparent_char_width, coord.y*track_height, apparent_char_width, track_height);
        }
        /**
        * Calls <code>coord_under_point</code> with <code>new Point(mouseX, mouseY)</code>.
        *
        * @return A <code>Point</code>, in indices, representing the text offset (x) and track offset (y) under the pixel point.
        **/
        public function coord_under_mouse():Point
        { 
            return coord_under_point(new Point(mouseX, mouseY));// values are indices
        }
        /**
        * Calls <code>coordRect_under_point</code> with <code>new Point(mouseX, mouseY)</code>.
        *
        * @return A <code>Rectangle</code> surrounding the character under the pixel location, which is the mouse location.
        **/
        public function coordRect_under_mouse():Rectangle
        { 
            return coordRect_under_point(new Point(mouseX, mouseY));// values are pixels
        }
    }
}

