package mafComponents
{
    import flash.events.MouseEvent;
    import flash.events.KeyboardEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import spark.components.*;
    import spark.components.supportClasses.SkinnableComponent;
    import mafComponents.*;
    import mafComponents.utils.geomUtils;

    import mx.core.FlexGlobals;
    import flash.ui.Keyboard;
    import flash.desktop.Clipboard;
    import flash.desktop.ClipboardFormats;

    [SkinState("noSelection")]
    [SkinState("drawingSelection")]
    [SkinState("finishedSelection")]

    [Event(name="selectionChange", type="flash.events.Event")]

    import mx.states.State;
    /**
     * A selection box that does not align a superficial text field over the selection area, as that proved too difficult to pull off.
    **/
    public class SparkBlockSelection extends SkinnableComponent implements IBlockSelection
    {
        // the component's resolved position
        private var _alt_x:Number = 0;
        private var _alt_y:Number = 0;
        private var alt_x_changed:Boolean = false;
        private var alt_y_changed:Boolean = false;
        // the component's resolved dimensions
        private var _alt_width:Number = 0;
        private var _alt_height:Number = 0;
        private var alt_width_changed:Boolean = false;
        private var alt_height_changed:Boolean = false;
        // the component's containing block... changes upon mouse interaction
        private var _selectedBlock:Block = null;
        private var selectedBlockChanged:Boolean = false;
        // going to sync the component's currentState with the skin class's current state
        private var _currentState:String = "noSelection";

        private var dragStartPoint:Point = null;
        /**
        * Creates an instance of the SparkBlockSelection class.
        **/
        public function SparkBlockSelection()
        {
            super();
            // add keyboard listener
            FlexGlobals.topLevelApplication.addEventListener(KeyboardEvent.KEY_DOWN, keyboardHandler);
            //mouseEnabled = false;
        }
        /**
        * Called by each Block as it is being created to add mouse and keyboard event listeners.
        * @see #mouseHandler()
        **/
        public function watch(block:Block):void // required to implement IBlockSelection
        {
            // add mouse eventListeners
            block.addEventListener(MouseEvent.MOUSE_UP, mouseHandler);
            block.addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
            block.addEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
            block.addEventListener(MouseEvent.CLICK, mouseHandler);
        }
        /**
        * @copy IBlockSelection#reset()
        **/
        public function reset(block:Block):void // required to implement IBlockSelection
        {
            dragStartPoint = null;
            alt_width = 0;
            alt_height = 0;
        }
        /**
        * @copy IBlockSelection#alt_x
        **/
        public function get alt_x():Number { return _alt_x; } // required to implement IBlockSelection
        public function set alt_x(value:Number):void
        {
            if (value != _alt_x)
            {
                _alt_x = value;
                alt_x_changed = true;
                invalidateProperties();
            }
        }
        /**
        * @copy IBlockSelection#alt_y
        **/
        public function get alt_y():Number { return _alt_y; } // required to implement IBlockSelection
        public function set alt_y(value:Number):void
        {
            if (value != _alt_y)
            {
                _alt_y = value;
                alt_y_changed = true;
                invalidateProperties();
            }
        }
        [Bindable]
        /**
        * @copy IBlockSelection#alt_width
        **/
        public function get alt_width():Number { return _alt_width; } // required to implement IBlockSelection
        public function set alt_width(value:Number):void
        {
            if (value != _alt_width)
            {
                _alt_width = value;
                alt_width_changed = true;
                invalidateProperties();
            }
        }
        [Bindable]
        /**
        * @copy IBlockSelection#alt_height
        **/
        public function get alt_height():Number { return _alt_height; } // required to implement IBlockSelection
        public function set alt_height(value:Number):void
        {
            if (value != _alt_height)
            {
                _alt_height = value;
                alt_height_changed = true;
                invalidateProperties();
            }
        }
        public function get selectedBlock():Block { return _selectedBlock; }
        public function set selectedBlock(value:Block):void
        {
            if (value != _selectedBlock)
            {
                trace(name, 'set selectedBlock to', value, 'currentState', currentState);
                if (_selectedBlock) // clear selection from the old block
                {
                    reset(_selectedBlock);
                }
                selectedBlockChanged = true;
                _selectedBlock = value;
                _selectedBlock.addChild(this);
                invalidateProperties();
            }
        }
        override public function get currentState():String { return _currentState; }
        /**
         * Mirrors the skin state, and calls <code>updateSelectionRect()</code> when set to <code>finishedSelection</code>.
         * @see #updateSelectionRect()
        **/
        override public function set currentState(value:String):void
        {
            trace(name + '.set currentState(' + value + '): dragStartPoint', dragStartPoint);
            if (value == "noSelection")
                selectionRect == null;
            if (value == "finishedSelection")
            {
                updateSelectionRect();
                toolTip = "ctrl-c to copy text";
            }
            else
            {
                toolTip = null;
            }

            if (value != _currentState)
            {
                _currentState = value;
                if (_currentState == "finishedSelection") dragStartPoint = null;
                invalidateSkinState();
            }
        }

        private var _selectionRect:Rectangle = null;
        private var selectionRectChanged:Boolean = false;
        /** 
         * Index-based Rectangle specifying the text underneath the selection square (this component).
         *
         * <p>Calls <code>invalidateProperties()</code> to schedule a change to the <code>selectionText</code>.</p>
         * @see #commitProperties() 
         * @see #selectionText
        */
        public function set selectionRect(value:Rectangle):void
        {
            if ((_selectionRect == null) || (value == null) || (! _selectionRect.equals(value)))
            {
                _selectionRect = value;
                selectionRectChanged = true;
                invalidateProperties();
            }
        }
        public function get selectionRect():Rectangle { return _selectionRect; }

        private var _selectionText:String = "";
        /**
        * The text underneath the selection area.
        **/
        public function get selectionText():String { return _selectionText; }

        /** 
        * Triggered any time <code>currentState</code> is set to (not just changed to) <code>finishedSelection</code>
        *
        * @see #currentState
        **/
        protected function updateSelectionRect():void
        { 
            trace(name + '.updateSelectionRect()');
            var alt_area:Rectangle = new Rectangle(alt_x, alt_y, alt_width, alt_height);
            trace('     alt_area',alt_area);
            var index1:Point = _selectedBlock.coord_under_point(alt_area.topLeft);
            trace('     _selectedBlock.coord_under_point(alt_p) = index1',index1);
            var index2:Point = _selectedBlock.coord_under_point(alt_area.bottomRight);
            /*if (dragStartPoint)
                index2 = _selectedBlock.coord_under_point(dragStartPoint);
            else
                index2 = index1;*/
            

            trace('     _selectedBlock.coord_under_point(dragStartPoint) = index2',index2);

            selectionRect = geomUtils.spanningRectBetweenPoints(index1, index2);

            trace('     selectionRect = geomUtils.spanningRectBetweenPoints(index1, index2)', selectionRect);
        }
        /**
        * Synchronizes multiple component-specific properties in addition to <code>super.commitProperties()</code>.
        *
        * <p>
        * A new or changed selection triggers multiple downstream data changes in this component:
        * <ul>
        * <li>The pixel location of the selection within the <code>Block</code>: <code>alt_x, alt_y</code>. Changes to these properties result in a call to <code>Block(parent).invalidateDisplayList()</code>.</li>
        * <li>The pixel dimensions of the selection within the <code>Block</code>: <code>alt_width, alt_height</code>. Changes to these properties result in a call to <code>invalidateSize()</code>.</li>
        * <li>The index-based dimensions of the selection within the block, as indicated by differences in <code>selectionRect</code>. A change to this property results in a resetting of the <code>selectionText</code>, compiled by calling <code>selectedBlock.getSubText()</code></li>
        * </ul>
        * </p>
        * @see Block#getSubText
        **/
        override protected function commitProperties():void
        {
            //if (currentState == null) currentState = "noSelection";
            super.commitProperties();
            if (selectedBlockChanged)
            {
                selectedBlockChanged = false;
                /* 
                must re-parent the component to the new block
                 */
                trace(name, "selectedBlock", _selectedBlock);
            }
            if (alt_x_changed || alt_y_changed)
            {
                alt_x_changed = false;
                alt_y_changed = false;
                move(_alt_x, _alt_y);
                invalidateDisplayList(); // ?
                if (parent && parent is Block)
                {
                    Block(parent).invalidateDisplayList();
                }
            }
            if (alt_width_changed || alt_height_changed)
            {
                alt_width_changed = false;
                alt_height_changed = false;
                setActualSize(_alt_width, _alt_height);
                invalidateSize(); // ?
                if (parent && parent is Block)
                {
                    Block(parent).invalidateDisplayList();
                }
            }
            if (selectionRectChanged) // load the new text or clear old text
            { 
                selectionRectChanged = false;
                if (_selectionRect)
                { 
                    var selectionTextLines:Array = _selectedBlock.getSubText(_selectionRect);
                    _selectionText = selectionTextLines.join('\n');
                    trace(name + '.commitProperties(): selectionRectChanged: selectionRect:\n' + _selectionRect);
                    trace(name + '.commitProperties(): selectionRectChanged: selectionText:\n' + _selectionText);
                }
                else
                {
                    _selectionText = "";
                }
                dispatchEvent(new Event("selectionChange"));
            }
        }
        /**
        * The genome positions of the active selection
        * 
        * <p>Implemented by calling <code>getSquareCoordinates()</code> on the <code>selectedBlock</code>, with the <code>selectionRect</code>.</p>
        * @see Block#getSquareCoordinates
        **/
        public function get bounds():Array
        {
            return _selectedBlock.getSquareCoordinates(_selectionRect);
        }
        /**
        * Aside from calling <code>super.measure()</code>, <code>measuredWidth</code> is set to <code>alt_width</code> and <code>measuredHeight</code> to <code>alt_height</code>
        **/
        override protected function measure():void
        {
            super.measure();
            measuredWidth = alt_width;
            measuredHeight = alt_height;
            measuredMinWidth = measuredWidth;
            measuredMinHeight = measuredHeight;
        }
        import mx.core.IUIComponent;
        /*override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            for (var i:int = 0; i < numChildren; i++)
            {
                var obj:IUIComponent = IUIComponent(getChildAt(i));
                obj.move(0, 0);
                obj.setActualSize(alt_width, alt_height);
                var obj_w:Number = obj.getExplicitOrMeasuredWidth();
                var obj_h:Number = obj.getExplicitOrMeasuredHeight();
                trace(name + '.updateDisplayList', i, obj.name, obj.x, obj.y, obj_w, obj_h);
            }
        }*/

        override protected function getCurrentSkinState(): String
        {
            return currentState;
        }

        /**
        * Translate keyboard commands into actions
        **/
        protected function keyboardHandler(evt:KeyboardEvent):void
        {
            trace(name + '.keyboardHandler:' + evt.keyCode, '`' + String.fromCharCode(evt.keyCode) + '\'')
            // user copies to the clipboard
            if ((evt.keyCode == Keyboard.C) && (evt.ctrlKey))
            {
                copySelection();
            }
            
        }
        /**
        * Attempts to resolve mouse events into a selection area.
        **/
        protected function mouseHandler(evt:MouseEvent):void
        {
            if ((evt.type == MouseEvent.CLICK) || (evt.type == MouseEvent.MOUSE_DOWN) || dragStartPoint)
            {
                selectedBlock = evt.currentTarget as Block;
                // pixels
                var coord:Rectangle = selectedBlock.coordRect_under_mouse();
                //var mousePoint:Point = new Point(Number(coord.x.toFixed(3)), coord.y);
                var mousePoint:Point = coord.topLeft;
                // indices
                var coordIndices:Point = selectedBlock.coord_under_mouse();
                var mousePointIndices:Point = new Point(coordIndices.x, coordIndices.y);
            }

            if (evt.type == MouseEvent.MOUSE_DOWN)
            {
                alt_x = mousePoint.x;
                alt_y = mousePoint.y;
                currentState = "drawingSelection";
                dragStartPoint = mousePoint;
            }
            else if (evt.type == MouseEvent.CLICK)
            {
                alt_x = mousePoint.x;
                alt_y = mousePoint.y;
                alt_width = coord.width;
                alt_height = coord.height;
                currentState = "finishedSelection";
            }
            else if (dragStartPoint)
            {
                alt_x = Math.min(mousePoint.x, dragStartPoint.x);
                alt_y = Math.min(mousePoint.y, dragStartPoint.y);
                alt_width = Math.abs(mousePoint.x - dragStartPoint.x) + selectedBlock.apparent_char_width;
                alt_height = Math.abs(mousePoint.y - dragStartPoint.y) + selectedBlock.track_height;

                // sometimes the CLICK doesn't register, 
                // but there is a MOUSE_DOWN followed by a MOUSE_UP
                if (alt_width <= 0) alt_width = coord.width;
                if (alt_height <= 0) alt_height = coord.height;

                if (evt.type == MouseEvent.MOUSE_OUT) 
                {
                    currentState = "drawingSelection";
                }
                else if (evt.type == MouseEvent.MOUSE_UP)
                {
                    currentState = "finishedSelection";
                }
                else
                {
                    trace(name + '.mouseHandler|' + evt.type + '| shouldn\'t be here');
                }
            }

            var ax:String = alt_x.toFixed(3);
            var aw:String = alt_width.toFixed(3);
            var alt_area:Rectangle = new Rectangle(Number(ax), alt_y, Number(aw), alt_height);
            trace(name + '.mouseHandler|' + evt.type + '|,', currentState, "alt_area", alt_area, "coordIndices", coordIndices, "dragStartPoint", dragStartPoint);
        }
        /**
        * Copy the selectionText to the operating system clipboard.
        **/
        public function copySelection():void
        {
            if (_selectionText)
            {
                Clipboard.generalClipboard.clear();
                Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, _selectionText);
            }
        }
    }
}

