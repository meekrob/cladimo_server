/* SpacerBlock:Block - No unique functionality yet, just making a logical distinction with the subclass
 *   
 *
*/
package mafComponents
{
    import mafComponents.Block;
    /**
    * A (logically) empty Block for informative purposes.
    **/
    public class SpacerBlock extends Block
    {
        /**
        * Constructor.
        **/
        public function SpacerBlock()
        {
            super();
        }
    }
}
