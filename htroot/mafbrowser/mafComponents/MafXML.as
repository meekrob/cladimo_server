/* MafXML.as - Class intended to encapsulate a MAF file received via HTTPService or FileReference.
 * The data is read as XML and converted, as necessary, to Actionscript Objects or Arrays.
 */
package mafComponents
{
    import mx.utils.ObjectUtil;
    import mx.controls.*;
    import mx.collections.ArrayCollection;
    import mx.managers.CursorManager;
    import mafComponents.*;
    import mafComponents.Block;
    import mx.rpc.events.ResultEvent;
    import flash.events.*;
    import mx.events.FlexEvent;

    public class MafXML
    {
        public var total_width:uint;
        public var nspecies:int;
        public var species_list:Object;
        public var species_list_str:Array;
        public var nblocks:uint;
        public var blocks:Array;
        public var msg:TextArea;
        public var filename:String;
        public var mafconsole:MafConsole;
        public var mafviewer:MafViewer;

        public function MafXML(_mafconsole:MafConsole, _mafviewer:MafViewer=null)
        {
            mafconsole = _mafconsole;
            mafviewer = _mafviewer;
            blocks = new Array();
        }

        public function parse_species_list_obj():Array
        {
            var str_list:Array = new Array();
            trace("MafXML.parse_species_list_obj()");
            for each (var sp:Object in species_list)
            {
                trace("-----adding", sp.@id);
                str_list.push(sp.@id);
            }
            trace("-----str_list", str_list);
            return str_list;
        }

        /*public function handle2(event:ResultEvent):void
        {
            nblocks = event.result.maf.block.length;
            var result_blocks:Array;
            if (event.result.maf.block is ArrayCollection)
                result_blocks = event.result.maf.block.toArray();
            else
                result_blocks = [event.result.maf.block];

            nblocks = result_blocks.length;
            if (event.result.maf.species_list.species is ArrayCollection)
                species_list = event.result.maf.species_list.species;
            else
                species_list = [event.result.maf.species_list.species];

            species_list_str = parse_species_list_obj();
            nspecies = species_list.length;

            for (var i:int=0; i < nblocks; i++) 
                mafviewer.scroller.importBlock(result_blocks[i], species_list_str);
        }*/
        public function handle(event:ResultEvent):void
        {
            var maf:Object = event.result.maf;
            handleObject(maf);
        }

        import mx.managers.IBrowserManager;
        import mx.managers.BrowserManager;
        public function handleObject(maf:Object):void
        {
            mafviewer.title = maf.@name;
            var bm:IBrowserManager = BrowserManager.getInstance();
            bm.setTitle("MAFBrowser - " + mafviewer.title);
            trace(mafviewer.title);
            trace("MafXML.handleObject: maf.block is XML:", maf.block is XML);
            trace("MafXML.handleObject: maf.block is XMLList:", maf.block is XMLList);
            var i:uint;
            var j:uint;
            var result_blocks:XMLList;
            try
            {
                nblocks = maf.block.length();
                result_blocks = maf.block;
            }
            catch(e:Error)
            {
                Alert.show(e.name + ": Error in nblocks - " + e.message);
            }

            trace("MafXML.handleObject: nblocks:", nblocks);
            if (nblocks == 0)
            {
                trace("MafXML.handleObject: maf.block:", maf.block);
            }

            try
            {
                /*total_width = 0;
                if (maf.species_list.species is ArrayCollection)
                    species_list = maf.species_list.species;
                else if (maf.species_list.species is XMLList)
                    species_list = maf.species_list.species;
                else
                    species_list = [maf.species_list.species];*/

                species_list = maf.species_list.species;
                species_list_str = parse_species_list_obj();
                nspecies = species_list.length();
            }
            catch(e:Error)
            {
                Alert.show(e.name + ": Error in nspecies - " + e.message);
            }
            trace("MafXML.handleObject: nspecies:", nspecies);

            //dispatchEvent(new Event("MafXMLFinishedLoading"));

            // distribute blocks into the visual components
            var newBlock:Block;
            var block_i:int = 0;
            for each (var block:Object in result_blocks)
            {
                newBlock = new Block();
                newBlock.name = "MAFBlock_" + block_i;
                newBlock.addEventListener(FlexEvent.CREATION_COMPLETE, function(e:FlexEvent):void { trace(e.currentTarget.name, "creationComplete"); });
                var components:Object = block.component;
                var ncomponents:uint = components.length();

                for (j=0; j < nspecies; j++)
                {
                    // match species j to its component in the current block, if it exists
                    var comp:Object = null;
                    //var comp_i:int;
                    //for (comp_i=0; comp_i < ncomponents; comp_i++)
                    //trace("scanning components in block:");
                    for each (var component:Object in components)
                    {
                        if (species_list_str[j] == component.@species) 
                        {
                            comp = component;
                            break;
                        }
                    }
                    
                    if (comp != null)
                    {
                        newBlock.addComponentTrack(comp.@src, comp.@start, comp.@src_size, comp.@size, comp.@strand, comp.@text);
                    }
                    else
                    {
                        newBlock.addEmptyTrack(species_list_str[j]);
                    }
                }
                mafviewer.addBlock(newBlock);
                blocks.push(newBlock);
                block_i++;
            }
        }
    }
}
