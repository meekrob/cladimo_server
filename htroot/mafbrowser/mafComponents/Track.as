/* Track: Canvas
*/
package mafComponents
{
    import flash.events.*;
    import flash.display.*;
    import flash.text.*;
    import mx.controls.*;
    import mx.containers.*;
    import mx.utils.*;
    import mafComponents.*;
    import mafComponents.errors.*;
    import mafComponents.events.TrackMouseEvent;
    import flash.events.MouseEvent;

    /**
    * The basic component for showing text in a <code>Block</code>.
    *
    * <p>The <code>Track</code> is a <code>Canvas</code> with a text component that can also place annotation elements on the text.
    * In particular, the <code>ComponentTrack</code> places <code>Element</code>s on alignment text.
    * </p>
    **/
    public class Track extends Canvas
    {
        private var _char_width:int;
        private var _char_height:int;
        private var char_highlight:Shape;
        private var _viewer:MafViewer;
        public var ta:TrackTextArea;
        public function Track(newText:String="")
        {
            super();
            //setText(newText);
            text = newText;
            verticalScrollPolicy = "off";
            horizontalScrollPolicy = "off";
            setStyle("borderStyle", "none");
            setStyle("left", 0);
            //addMouseEventListeners();
        }
        private function addMouseEventListeners():void
        {
            var allMouseEvents:Array = [
                  MouseEvent.CLICK, MouseEvent.DOUBLE_CLICK, MouseEvent.MOUSE_DOWN, 
                  MouseEvent.MOUSE_MOVE, MouseEvent.MOUSE_OUT, MouseEvent.MOUSE_OVER, 
                  MouseEvent.MOUSE_UP, MouseEvent.MIDDLE_CLICK, MouseEvent.MIDDLE_MOUSE_DOWN, 
                  MouseEvent.MIDDLE_MOUSE_UP, MouseEvent.RIGHT_CLICK, MouseEvent.RIGHT_MOUSE_DOWN, 
                  MouseEvent.RIGHT_MOUSE_UP, MouseEvent.MOUSE_WHEEL, MouseEvent.ROLL_OUT, MouseEvent.ROLL_OVER];
            for each (var mouseEvent:String in allMouseEvents)
            {
                addEventListener(mouseEvent, dispatchTrackMouseEvent);
            }
        }
        private function dispatchTrackMouseEvent(e:MouseEvent):void
        {
            var evt:TrackMouseEvent = new TrackMouseEvent(e);
            dispatchEvent(evt);
        }
        public function set draggable(d:Boolean):void
        {
            if (d)
            {
                addEventListener(MouseEvent.MOUSE_DOWN, start_drag);
                addEventListener(MouseEvent.MOUSE_UP, stop_drag);
                toolTip = "drag to reorder MAF rows";
                ta.selectable = true;
            }
        }
        /**
        * Return a new <code>Track</code> with a subset of text.
        * 
        * <p>Calls <code>text.substr(col_start, ncols)</code> for use in a new <code>Track</code> object.</p>
        * @return A new <code>Track</code> with the subtext.
        **/
        public function slice(col_start:int, ncols:int):Track
        {
            return new Track(text.substr(col_start, ncols));
        }
        public function get_viewer():MafViewer
        {
            var obj:Object = parent;
            while (!(obj is MafViewer))
            {
                obj = obj.parent;
            }
            return obj as MafViewer;
        }
        public function get viewer():MafViewer
        {
            if (! _viewer) _viewer = get_viewer();
            return _viewer;
        }
        public function start_drag(event:MouseEvent):void
        {
            var start_index:int = viewer.src_panel.getChildIndex(this);
            trace("Track.start_drag():", start_index);
            viewer.reorder_start_track = this;
            viewer.reorder_start_index = start_index;
            startDrag(false, viewer.src_panel.getRect(parent));
            ta.setStyle("color", 0xFF0000);
        }
        public function stop_drag(event:MouseEvent):void
        {
            trace("Track.stop_drag()");
            stopDrag();
            var obj:Object;
            viewer.reorder_start_track.ta.setStyle("color", 0);
            if (dropTarget == null) // moving a track DOWN
            {
                trace("     No dropTarget.");
                var current_i:int = viewer.src_panel.getChildIndex(this);
                obj = viewer.src_panel.tracks[y/height];
                trace("     current_i", current_i, "obj", obj);
                viewer.src_panel.addChildAt(viewer.reorder_start_track, current_i);

                var spliced:Object = viewer.console.species_list.splice(viewer.reorder_start_index,1);
                viewer.console.species_list.splice(current_i, 0, spliced);

                for each (var bb:Block in viewer.scroller.blocks)
                {
                    bb.addChildAt(bb.getChildAt(viewer.reorder_start_index), current_i);
                }
                for each (var mb:MiniBlock in viewer.mafgraph.miniblocks)
                {
                    mb.addChildAt(mb.getChildAt(viewer.reorder_start_index), current_i);
                }
            }
            else if (dropTarget) 
            {
                trace("     dropTarget.", dropTarget);
                obj = dropTarget;
                while (!(obj is Track))
                {
                    obj = obj.parent;
                    if (obj is Block)
                    {
                        Alert.show("reorder: found block");
                        break;
                    }
                }
                if (obj is Track)
                {
                    var parentblock:Block;
                    if (parent is Block)
                    {
                        parentblock = parent as Block
                        var obj_track:Track = obj as Track;
                        var obj_tracks_i:int = parentblock.tracks.indexOf(obj);
                        var obj_child_i:int = parentblock.getChildIndex(obj_track);

                        var this_track:Track = this as Track;
                        var this_tracks_i:int = parentblock.tracks.indexOf(this);
                        var this_child_i:int = parentblock.getChildIndex(this_track);

                        var mf:MafViewer = parentblock.get_viewer();
                        if (mf is MafViewer)
                        {
                            mf.src_panel.addChildAt(mf.src_panel.getChildAt(this_child_i), obj_child_i);
                            spliced = viewer.console.species_list.splice(this_child_i-1,1);
                            viewer.console.species_list.splice(obj_child_i-1, 0, spliced);
                            for each (var b:Block in mf.scroller.blocks)
                            {
                                b.addChildAt(b.getChildAt(this_child_i), obj_child_i);
                            }

                            for each (mb in viewer.mafgraph.miniblocks)
                            {
                                mb.addChildAt(mb.getChildAt(this_child_i), obj_child_i);
                            }
                        }
                        else
                        {
                            Alert.show(parentblock.parent + "!");
                        }
                    }
                }
            }
            var nsp:Array = new Array();
            for (var i:int=0; i< viewer.src_panel.numChildren; i++)
            {
                var o:Object = viewer.src_panel.getChildAt(i);
                if (o is Track)
                    nsp.push( Track(o).text );
            }
            viewer.mafgraph.set_panel( nsp );
        }
        public function get text():String
        {
            return _text;
        }
        private var _text:String;
        private var _textChanged:Boolean = false;
        /**
        * The text displayed by the component.
        **/
        public function set text(ntext:String):void
        {
            if (_text != ntext)
            {
                _text = ntext;
                _textChanged = true;
                invalidateProperties();
            }
        }
        /**
        * The <code>textWidth</code> value on the text field.
        **/
        public function get textWidth():Number
        {
            if (ta) return ta.textWidth;
            return NaN;
        }
        /**
        * The <code>textHeight</code> value on the text field.
        **/
        public function get textHeight():Number
        {
            if (ta) return ta.textHeight;
            return NaN;
        }
        /**
        * The <code>length</code> value on the text field.
        **/
        public function get length():int
        {
            if (ta) return ta.length;
            //return NaN;
            throw new ChildNotCreatedYetError("`length' requested on ta:TrackTextArea, not created yet");
        }
        import flash.geom.Rectangle;
        import flash.geom.Point;
        /** 
        * Returns the pixel boundaries of a character on the text component.
        * @throws mafComponents.ChildNotCreatedYetError
        **/
        public function getCharBoundaries(charIndex:int):Rectangle
        {
            if (ta && ta.txtField)
            {
                var rect:Rectangle = ta.txtField.getCharBoundaries(charIndex);
                if (rect)
                {
                    var taPoint:Point = new Point(rect.x, rect.y);
                    var stagePoint:Point = ta.localToGlobal(taPoint);
                    var localPoint:Point = globalToLocal(stagePoint);
                    rect.x = localPoint.x;
                    rect.y = localPoint.y;
                }
                return rect;
            }
            throw new ChildNotCreatedYetError("`txtField'.getCharBoundaries requested on ta:TrackTextArea, not created yet");
        }
        public function getCharIndexAtPoint(x:Number, y:Number):int
        {
            if (ta && ta.txtField)
            {
                // do something
                return ta.txtField.getCharIndexAtPoint(x,y);
            }
            throw new ChildNotCreatedYetError("`txtField'.getCharIndexAtPoint(x,y) requested on ta:TrackTextArea, not created yet");
        }

        /**** overridden container/control methods *******/
        protected var textSet:Boolean = false;
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (ta && _textChanged)
            {
                _textChanged = false;
                ta.text = _text;
                textSet = true;
                dispatchEvent(new Event("TrackTextChanged"));
            }
        }
        override protected function createChildren():void
        {
            super.createChildren();
            if (! ta)
            {
                ta = createTextArea();
                addChild(ta);
            }
        }
        private function createTextArea():TrackTextArea
        {
            ta = new TrackTextArea();
            ta.selectable = false;
            ta.wordWrap = false;
            ta.editable = false;
            ta.text = "empty track";
            //ta.setStyle("paddingLeft",-2);
            ta.setStyle("paddingLeft",-2);
            ta.setStyle("paddingRight",0);
            ta.setStyle("backgroundAlpha", 0);
            ta.setStyle("fontFamily", "Courier");
            ta.setStyle("fontSize", "12");
            ta.setStyle("focusAlpha", 0);
            ta.setStyle("focusThickness", 0);
            ta.setStyle("leading",0);
            ta.setStyle("borderStyle", "none");
            //ta.setStyle("borderThickness", 1);
            //ta.setStyle("borderSides", "bottom top");
            //ta.setStyle("borderColor", 0);
            ta.setStyle("textAlign", "right");
            return ta;
        }

        override public function measureText(str:String):TextLineMetrics
        {
            return ta.measureText(str);
        }
        /**
        * Returns <code>text.substr(at, len)</code>
        **/
        public function slice_text(at:int, len:int):String
        {
            return text.substr(at, len);
        }
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            //trace(name + ".updateDisplayList() viewMetricsAndPadding.left", viewMetricsAndPadding.left);
            //trace(name + ".updateDisplayList() getStyle(\"paddingLeft\")", getStyle("paddingLeft"));
            if (ta)
            {
                ta.move(-4, 0); // what the fuck is going on here? this seems to move it so it's in view
                ta.setActualSize(unscaledWidth, unscaledHeight);
            }
        }
        override protected function measure():void 
        {
            super.measure();
            if (ta) 
            {
                ta.width = ta.textWidth;
                var nbDim:TextLineMetrics = ta.measureText("A");

                _char_width= nbDim.width;
                _char_height= nbDim.height;
                //measuredMinWidth = measuredWidth = nbDim.width * (text.length);
                measuredMinWidth = measuredWidth = ta.getExplicitOrMeasuredWidth() + 0;
                //trace(this + ".measure():", nbDim.width, text.length, "=", nbDim.width*text.length, "versus", ta.getExplicitOrMeasuredWidth());
                measuredMinHeight = measuredHeight = nbDim.height + 2;
            }
        }
        /**
        * The width of a single character in the component.
        *
        * <p>It is measured by calling measureText("A") on the text component, which returns a TextLineMetrics object. The <code>width</code> property
        * is used for <code>char_width</code>, while the <code>height</code> property is used for <code>char_height</code>.
        * </p>
        * @see #char_width
        **/
        public function get char_width():int
        {
            return _char_width;
        }
        /**
        * The height of a single character in the component.
        *
        * <p>It is measured by calling measureText("A") on the text component, which returns a TextLineMetrics object. The <code>width</code> property
        * is used for <code>char_width</code>, while the <code>height</code> property is used for <code>char_height</code>.
        * </p>
        * @see #char_height
        **/
        public function get char_height():int
        {
            return _char_height;
        }
        /**
        * Draw a graphical representation of this component.
        *
        * <p>This method is overridden by <code>ComponentTrack</code> to represent alignment text. The <code>Track</code> method does nothing.</p>
        *
        * @param canvas A component (such as a TextArea) with a graphics element that can recieve drawn commands.
        * @param yshift The y-offset to draw the track graphic on the <code>MiniBlock.</code>
        * @see ComponentTrack#draw_segments
        * @see MiniBlock
        **/
        public function draw_segments(canvas:Sprite, yshift:Number, trackColor:int = 0x000033, gapColor:int = 0x888888):void
        {
            return;
            /** why is this stuff here? **/
            var grey:int = 0x888888;
            var red:int = 0xFF0000;
            canvas.graphics.clear();
            canvas.graphics.beginFill(trackColor);
            canvas.graphics.drawRect(0,0,length, 10);
            canvas.graphics.endFill();
            canvas.graphics.moveTo(0,yshift + 6);
            canvas.graphics.lineStyle(0,red);
            canvas.graphics.lineTo(text.length,yshift +6);
            
        }
        /**
        * Placeholder.
        **/
        public function draw_elements(canvas:Sprite, yshift:Number):void
        {
        }
    }
}

