package mafComponents
{
    import mafComponents.*;
    import mx.rpc.events.ResultEvent;
    import mx.controls.*;
    import mx.containers.Panel;
    import mx.rpc.http.HTTPService;
    import mx.rpc.events.*;
    import mx.utils.ObjectUtil;

    import mx.managers.CursorManager;
    import flash.net.*;
    import flash.events.*;

    import flash.display.Sprite;
    /**
    * Interface for uploading a file or retrieving information from the server's repository.
    **/
    public class MafUploader extends Sprite
    {
        /**
        * Executable on the local server that returns an uploaded MAF file as XML. Set using the locale.
        * @see #setHandlersToLocale()
        **/
        public var uploadScript:String;
        /**
        * UNUSED. Set by locale.
        * @see #setHandlersToLocale()
        **/
        [Deprecated(replacement="setHandlersToLocale()")]
        public var mafXMLScript:String;
        /**
        * UNUSED. Set by locale.
        * @see #setHandlersToLocale()
        **/
        [Deprecated(replacement="setHandlersToLocale()")]
        public var dataXMLScript:String;
        /**
        * UNUSED. Set by locale.
        * @see #setHandlersToLocale()
        **/
        [Deprecated(replacement="setHandlersToLocale()")]
        public var BED_XMLScript:String;
        /**
        * The URL of the CGI script which retrieves data from the server repository.
        **/
        public var dataimporter:String;
        /**
        * FileReference used for the local file upload.
        **/
        public var fileRef:FileReference;
        /**
        * File processing function, passed in to process_filename()
        * @see #process_filename()
        **/
        public var datahandler:Function;
        /**
        * UNUSED.
        **/
        [Deprecated(replacement="null")]
        public var panelref:Panel;
        /**
        * Called (incorrectly) by process_xml.
        * @see #process_xml()
        **/
        public var completeFunc:Function;
        /**
        * The file name either of the upload, or the XML retrieved from the repository.
        * @see #process_filename()
        * @see #title
        **/
        public var filename:String;
        /**
        * Dataset or alignment title, may be the same as or similar to filename.
        * @see #filename
        **/
        public var title:String;
        /**
        * Debugging String.
        **/
        public var log:String;

        private var _type:String;
        /** 
        * Values can be "bed", "maf" or "data", determining the handler to use for file/data processing.
        **/
        public function get type():String 
        { 
            /*if (fileRef && fileRef.type)
            {
                if (fileRef.type == '.gff') return 'data';
                if (fileRef.type == '.bed') return 'data';
                if (fileRef.type == '.maf') return 'maf';
            }*/
            return _type; 
        }

        /**
        * Reference to the parent console.
        * @see MafConsole
        **/
        public var mainConsole:MafConsole;

        /**
        * Constructor. Links to the MafConsole, sets handlers using the locale.
        * @see #setHandlersToLocale()
        **/
        public function MafUploader(panel:Panel=null, onComplete:Function=null, __type:String="maf", mainC:MafConsole=null)
        {
            log = '';
            _type = __type;
            mainConsole = mainC;
            //Alert.show("[MafUploader_" + type + "].mainConsole=" + mainConsole);
            fileRef = new FileReference();
            configureListeners(fileRef);

            if (mainConsole.config_is_loaded)
                setHandlersToLocale();
            else
            {
                mainConsole.addEventListener("configLoaded", setHandlersToLocale);
            }


            //if (panel) panelref = panel;
            completeFunc = onComplete;
        }
        private function configureListeners(dispatcher:IEventDispatcher):void 
        {
            dispatcher.addEventListener("select", try_upload);
            dispatcher.addEventListener("complete", upload_complete);
            dispatcher.addEventListener("cancel", evt_cancel);
            dispatcher.addEventListener("open", evt_open);
            dispatcher.addEventListener("progress", evt_progress);
            dispatcher.addEventListener("uploadCompleteData", evt_uploadCompleteData);
            dispatcher.addEventListener("ioError", evt_ioError);
            dispatcher.addEventListener("securityError", evt_securityError);
            dispatcher.addEventListener("httpStatus", evt_httpStatus);
            dispatcher.addEventListener("httpResponseStatus", evt_httpResponseStatus);
            if (completeFunc != null)
                dispatcher.addEventListener("complete", completeFunc);
        }
        /**
        * Sets dataimporter to a locale-defined CGI-script.
        * <p>Uses MafConsole.locale to access one of (handlers.MAF_UPLOAD, handlers.MAF_XML, handlers.BED_XML, handlers.DATA_XML), 
        * which are XML-processing CGI-script URLs.</p>
        * @see MafConsole#locale
        * @see #dataimporter
        **/
        protected function setHandlersToLocale():void
        {
            uploadScript = mainConsole.locale.handlers.MAF_UPLOAD;
            if (type=="maf")
                dataimporter = mainConsole.locale.handlers.MAF_XML;
            else if (type=="bed")
                dataimporter = mainConsole.locale.handlers.BED_XML;
            else if (type=="data")
                dataimporter = mainConsole.locale.handlers.DATA_XML;
        }
        /* general uploading handlers */
        private function evt_cancel(event:Event):void // user cancelled during browse
        {
            //log += 'user cancelled<br/>\n';
            trace('user cancelled');
        } 
        private function evt_open(event:Event):void // file is being uploaded
        {
            trace("evt_open:Loading...");
        } 
        private function evt_progress(event:ProgressEvent):void // periodically dispatched during upload
        {
            trace("Progress:" , event.toString());
        } 
        private function evt_uploadCompleteData(event:DataEvent):void // uploading has finished
        {
            var xmldata:XML = XML(event.data);
            trace('uploadCompleteData', xmldata.name());
            if (xmldata.name() == 'maf')
            {
                /* // couldn't access xmldata.maf, because maf was already the root
                var result:XML = <maf></maf>;
                result.maf.@name = xmldata.@name;
                title = result.maf.@name;
                result.maf.setChildren(xmldata.children());*/
                mainConsole.mafxmlhandle_object(xmldata);
            }
        } 
        private function evt_httpResponseStatus(event:HTTPStatusEvent):void // success
        {
            trace('The upload operation completed successfully and the server returned a response URL and response headers');
            trace('HTTP status:', event.status);
        } 
        private function evt_ioError(event:IOErrorEvent):void // ioerror
        {
            trace("IOError..." , event.text);
        } 
        private function evt_httpStatus(event:HTTPStatusEvent):void // error
        {
            trace('HTTP error: ' , event.status);
        } 
        private function evt_securityError(event:SecurityErrorEvent):void // security error
        {
            trace("securityError: " , event.toString());
        } 
        /**
        * Launch the web browser's file browser
        **/
        public function browse(_datahandler:Function):Boolean
        {
            try
            {
                var success:Boolean = fileRef.browse();
            }
            catch (error:Error)
            {
                Alert.show("Unable to browse for files.");
            }
            datahandler = _datahandler;
            return success;
        }
        /**
        * Attempt the file upload, called upon "select" event dispatched by the fileRef.
        * @see #fileRef
        * @see #uploadScript
        **/
        public function try_upload(event:Event):void
        {
            filename = fileRef.name;
            trace("try_upload: going to process",filename);
            var request:URLRequest = new URLRequest(uploadScript);
            try
            {
                fileRef.upload(request, "MAFBrowserFileUpload"); // this will be the field in the form
            }
            catch (error:Error)
            {
                Alert.show("Unable to upload file.");
            }
        }
        private function upload_complete(event:Event):void
        {
            trace("Upload complete");
            CursorManager.removeBusyCursor();
        }
        /**
        * WHO CALLS THIS FUNCTION?
        **/
        [Deprecated]
        public function process_xml(event:Event):void
        {
            var datahandler:Function = mainConsole.datasethandler;
            var service:HTTPService = new HTTPService();
            service.url = dataimporter;
            service.addEventListener("result", datahandler);
            service.addEventListener("invoke", // what the hell is going on?
                function(event:InvokeEvent):void {
                    Alert.show(event.message.toString());
                }
            );
            service.send({'tag':event.target.name, 'xml':'yes'}); // filename in jobdir
            if (completeFunc != null) completeFunc();
        }
        /**
        * Get both MAF and data from a single tag; called by console.mxml to process the browser line.
        * @see console#parseBrowserLine()
        * @see #process_data_tagname()
        **/
        public function process_tagname(tag:String):void
        {
            trace("process_tagname(", tag, ")");
            var service:HTTPService = new HTTPService();
            service.resultFormat = "e4x";
            // get alignment
            service.url = mainConsole.locale.handlers.MAF_XML;
            service.addEventListener("result", mainConsole.mafxmlhandler);
            // get data
            service.addEventListener("result", function():void {process_data_tagname(tag)});

            // send request for maf
            service.send({'tag':tag}); 
        }
        /**
        * Result handler for (data) for process_tagname.
        * @see #process_tagname()
        * @see Console#parseBrowserLine()
        **/
        public function process_data_tagname(tag:String):void
        {
            trace("process_data_tagname(", tag, ")");
            var service:HTTPService = new HTTPService();
            service.resultFormat = "e4x";
            service.url = mainConsole.locale.handlers.DATA_XML;
            service.addEventListener("result", mainConsole.datasethandler);

            // send request for data
            service.send({'tag':tag}); 
        }
        /**
        * Process an XML-format file from the server data repository. Called by MafConsole.load_cmd().
        * <p>
        * This routine is called by <code>MafConsole.load_cmd()</code>, which processes tags from the browser line.
        * </p>
        * @see MafConsole#load_cmd()
        **/
        public function process_filename(_filename:String, _datahandler:Function, _title:String=""):void
        {
            filename = _filename;
            datahandler = _datahandler;
            title = _title;
            var service:HTTPService = new HTTPService();
            service.resultFormat = "e4x";
            if (type == 'maf')
            {
                service.url = mainConsole.locale.handlers.MAF_XML;
                service.addEventListener("result", datahandler);
            }
            else if (type == 'data')
            {
            }
            service.addEventListener("fault", fault);
            service.send({'tag':filename, 'xml':'yes'}); // filename in the tmp directory

            if (completeFunc != null) completeFunc();
        }
        private function invoke(event:Event):void
        {
            Alert.show("invoked:" + event.toString());
        }
        private function fault(event:FaultEvent):void
        {
            Alert.show("fault:" + event.fault);
        }
    }
}

