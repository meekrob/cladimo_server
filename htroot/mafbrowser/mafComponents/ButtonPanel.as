package mafComponents
{
    import flash.display.*;
    import mx.containers.*;
    import mx.controls.*;
    import mafComponents.*;
    import mx.events.*;

    /**
    * Canvas that displays dataset buttons in the main panel.
    **/
    public class ButtonPanel extends Canvas
    {
        /**
        * The space between dataset buttons
        **/
        public var horizontalGap:int;
        /**
        * The space between rows of dataset buttons
        **/
        public var verticalGap:int;
        /**
        * Array of buttons
        **/
        public var buttons:Array;
        /**
        * Button height
        **/
        public var buttHeight:int;
        /**
        * Array of datasets.
        **/
        public var datasets:Array;
        /**
        * Constructor.
        **/
        public function ButtonPanel()
        {
            super();
            horizontalGap = 1;
            verticalGap = 1;
            buttons = new Array();
            datasets = new Array();
            buttHeight = 15;
            //addEventListener(ChildExistenceChangedEvent.CHILD_ADD, update);
        }
        /**
        * Add a dataset/button
        **/
        public function add(dataset:DataSet):void
        {
            datasets.push(dataset);
            addChildAt(dataset, numChildren-1);
        }
        override protected function measure():void
        {
            super.measure();
            measuredMinHeight = buttHeight * datasets.length;
        }
        /**
        * In addition to super(), lays out the buttons according to horizontalGap and verticalGap, wrapping as necessary.
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            var leftSide:int = 0;
            var xoffset:int = leftSide;
            var yoffset:int = 0;
            for (var i:int; i< numChildren; i++)
            {
                var b:DisplayObject = getChildAt(i);
                b.height = buttHeight;
                if ((xoffset + b.width) > unscaledWidth)
                {
                    xoffset = leftSide;
                    yoffset += buttHeight + verticalGap;
                }
                b.x = xoffset;
                b.y = yoffset;
                xoffset += b.width + horizontalGap;
            }
        }
    }
}

