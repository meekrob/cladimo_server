/* EmptyTrack - placeholder with empty text for blocks missing an otherwise present species
*/
package mafComponents
{
    import mafComponents.*;

    /**
    * For rows in a Block that have no alignment text.
    **/
    public class EmptyTrack extends mafComponents.Track
    {
        /**
        * Constructor.
        **/
        public function EmptyTrack(forsrc:String="")
        {
            super("");
            name = "EmptyTrack_" + forsrc;
            setStyle("backgroundColor", 0xDDDDDD);
            setStyle("backgroundAlpha", 0.5);
        }
    }
}
