package mafComponents
{
    import mx.controls.TextArea;
    import mx.core.UITextField;
    import mx.core.IUITextField;
    //import flash.text.TextField;
    public class TrackTextArea extends TextArea 
    {
        public function TrackTextArea()
        {
            super();
        }
        public function get txtField():IUITextField
        {
            return textField;
        }
    }
}
