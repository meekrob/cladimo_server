/* ComponentTrack: Track
*/
package mafComponents
{
    import flash.display.*;
    import flash.text.*;
    import mx.controls.Alert;
    import mx.controls.TextArea;
    import flash.events.MouseEvent;
    import mafComponents.*;
    import mafComponents.datatypes.*;
    import mx.utils.*;
    /**
    * Component that shows alignment text and annotations in a Block.
    **/
    public class ComponentTrack extends Track
    {
        /**
        * The 'src' field of a MAF. Usual species.chrom.
        **/
        public var src:String;
        /**
        * Breakdown of src component: As in 'species'
        **/
        public var primary_src:String; //species
        /**
        * Breakdown of src component: As in 'chromosome' or 'contig'
        **/
        public var secondary_src:String; //chromosome or contig
        /**
        * Breakdown of src component: Reserved for third level.
        **/
        public var tertiary_src:String; //?
        /**
        * Breakdown of src component: Reserved for fourth level.
        **/
        public var quaternary_src:String;//?

        /**
        * Starting coordinate of sequence.
        **/
        public var seq_start:int;
        /**
        * Ending coordinate of sequence.
        **/
        public var seq_end:int;
        /**
        * Species chromosome or contig size.
        **/
        public var src_size:int;
        /**
        * Size of the sequence in the alignment text.
        **/
        public var seq_size:int;
        /**
        * '+' for forward, '-' for reverse complement.
        **/
        public var strand:String;
        //public var coord_map:Array;
        /** A coordinate map to track the sequence index via the alignment text position.
        *
        * @see /mafbrowser/coordinate_mapping.html Coordinate Mapping
        **/
        public var col_to_coord:Array;
        /** A coordinate map to track the alignment text position via the sequence index.
        *
        * @see /mafbrowser/coordinate_mapping.html Coordinate Mapping
        **/
        public var coord_to_col:Array;

        /**
        * Measured using the TextArea component.
        * @see #measureDim()
        **/
        public var track_width:Number;
        /**
        * Measured using the TextArea component.
        * @see #measureDim()
        **/
        public var single_base_height:Number;
        /**
        * Measured using the TextArea component.
        * @see #measureDim()
        **/
        public var single_base_width:Number;
        /**
        * List of Element annotations contained in this ComponentTrack.
        **/
        public var elements:Array;
        private var newElements:Array;

        /**
        * Constructor.
        **/
        public function ComponentTrack(_src:String, _seq_start:int, _src_size:int, _seq_size:int, _strand:String, txt:String)
        {
            super();
            src = _src;
            seq_start = _seq_start;
            seq_end = seq_start + _seq_size;
            src_size = _src_size;
            seq_size = _seq_size;
            strand = _strand;
            text = txt;

            parse_sources();
            name = "ComponentTrack_" + species;
            
            parse_coord_map();
            addEventListener(MouseEvent.MOUSE_OVER, emphasis_on);
            addEventListener(MouseEvent.MOUSE_OUT, emphasis_off);
            addEventListener(MouseEvent.DOUBLE_CLICK, show_coord_map);

            elements = new Array();
            newElements = new Array();
        }
        import mx.core.UIComponent;
        import flash.geom.Rectangle;
        /**
        * In addition to super.updateDisplayList(), place and size Element annotations.
        * <p>
        *  Annotations are placed according to Element.track_start and Element.track_end.
        * Some adjustments are made to the x and y locations, and to width and height, to get the 
        * "highlight" effect to center the text more cleanly.
        * </p>
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (textSet)
            {
                //trace(name + ".updateDisplayList():", textSet, text);
                for (var i:int=0; i < numChildren; i++)
                {
                    var obj:UIComponent = getChildAt(i) as UIComponent;
                    if (obj is Element)
                    {
                        var el:Element = obj as Element;
                        //trace("..............el:", el.x, el.y);
                        //trace("..............el:", el.track_start, el.track_end, length);
                        var start_rect:Rectangle = getCharBoundaries(el.track_start);
                        //trace("..............start_rect:", start_rect);
                        var end_rect:Rectangle = getCharBoundaries(el.track_end);
                        // track_end might be out-of-bounds
                        if (! end_rect)
                        {
                            end_rect = start_rect.clone();
                            if (el.track_end >= length)
                            {
                                end_rect.width = unscaledWidth - end_rect.x;
                            }
                        }
                        //trace("..............end_rect:", end_rect);
                        var el_width:Number = (end_rect.x+end_rect.width) - start_rect.x;
                        var el_height:Number = (end_rect.y+end_rect.height) - start_rect.y;
                        var fillColors:Array = el.getStyle("fillColors");
                        //trace("..............................");
                        el.move(start_rect.x - 3, start_rect.y - 1);
                        el.setActualSize(el_width + 2, el_height + 1);
                    }
                }
            }
        }
        /**
        * In addition to super.commitProperties(), add any pending Elements to the display list.
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (textSet)
            {
                if (newElements.length > 0)
                {
                    while (newElements.length > 0)
                    {
                        var el:Element = newElements.pop();
                        addChildAt(el, 0);
                        elements.push(el);
                    }
                }
            }
        }
        /**
        * Returns a new ComponentTrack as a subset of the current text.
        *
        * @see col_to_coord
        **/
        override public function slice(col_start:int, ncols:int):Track
        {
            var newSrc:String = src;
            var newSrcSize:int = src_size;
            var newStrand:String = strand;
            var newStart:int = col_to_coord[col_start] + seq_start;
            var newSize:int = col_to_coord[col_start+ncols] - col_to_coord[col_start];
            var newText:String = text.substr(col_start, ncols);
            return new ComponentTrack(newSrc, newStart, newSrcSize, newSize, newStrand, newText);
        }
        /**
        * Return the genomic position at a given index in the alignment text.
        * @param index The index of the alignment text.
        * @see #col_to_coord
        * @see #genomeCoordinatesForRange
        **/
        public function genomePositionAt(index:int):int
        {
            trace(name + '.genomePositionAt(' + index + '):' + col_to_coord[index]);
            var new_seq_start:int = seq_start + col_to_coord[index];
            if (strand == '-')
                return src_size - new_seq_start;

            return new_seq_start;
        }
        /**
        * Return an object specifying the genomic position for a given range within the alignment text.
        * @param startIndex The lower index of the alignment text.
        * @param endIndex The higher index of the alignment text.
        * @return <listing>
{
    chrom: chrom,
    seq_start: genomePositionAt(startIndex),
    seq_end: genomePositionAt(endIndex)
}
 
</listing>
        * @see #col_to_coord
        * @see #genomePositionAt
        **/
        public function genomeCoordinatesForRange(startIndex:int, endIndex:int):Object
        {
            trace(name + '.genomeCoordinatesForRange(' + startIndex + ',' + endIndex + ')');
            return {
                chrom: chrom,
                seq_start: genomePositionAt(startIndex),
                seq_end: genomePositionAt(endIndex)
            };
        }
        /**
        * In addition to super.measure(), calls measureDim().
        * @see #measureDim()
        **/
        override protected function measure():void
        {
            super.measure();
            measureDim();
        }
        /**
        * Sets the borderColor style upon emphasis.
        **/
        public function emphasis_on(evt:MouseEvent):void
        {
            setStyle("borderColor", 0xb7babc);
        }
        /**
        * Sets the borderColor style upon de-emphasis.
        **/
        public function emphasis_off(evt:MouseEvent):void
        {
            setStyle("borderColor", 0xFFFFFF);
        }
        /**
        * Sets some parameters by using TextArea.measureText()
        * <p>
        * Parameters determined here:
        * <ul>
        * <li>track_width</li>
        * <li>single_base_height</li>
        * <li>single_basae_width</li>
        * </ul>
        * </p>
        * @see #track_width
        * @see #single_base_height
        * @see #single_base_width
        * @see TextArea#measureText()
        **/
        public function measureDim():void
        {
            var nbDim:TextLineMetrics = ta.measureText(text);
            track_width = nbDim.width;
            nbDim = ta.measureText("A");
            single_base_height = measuredHeight;
            single_base_width = nbDim.width;
        }
        private function parse_sources():void
        {
            if(src)
            {
                var fields:Array = src.split(".");
                primary_src = fields[0];
                secondary_src = fields[1];
            }
        }
        /**
        * Species of the alignment component sequence.
        * @see #primary_src
        **/
        public function get species():String
        {
            return primary_src;
        } 
        /**
        * Chromosome of the alignment component sequence.
        * @see #secondary_src
        **/
        public function get chrom():String
        {
            return secondary_src;
        }
        /**
        * Contig name of the alignment component sequence.
        * @see #secondary_src
        **/
        public function get contig():String
        {
            return secondary_src;
        }
        /**
        * Return an object with species and chromosome location information for this <code>ComponentTrack</code>.
        * <p>This function returns the ComponentTrack's underlying data, except for the sequence, as an object.</p>
        * <p>For the "+" strand:</p>
        * <listing>
        *   { species:species, 
        *         chrom:chrom, 
        *         seq_start:seq_start, 
        *         seq_end:seq_end 
        *   }</listing>
        * <p>For the "-" strand:</p>
        * <listing>
        *  { species:species, 
        *       chrom:chrom, 
        *       seq_start:(src_size-seq_end), 
        *       seq_end:(src_size-seq_start) 
        *  }</listing>
        * @see #genomePositionAt
        * @see #genomeCoordinatesForRange
        **/
        public function get bounds():GenomePosition
        {
            if (strand == "+")
                return new GenomePosition({ species:species, chrom:chrom, seq_start:seq_start, seq_end:seq_end });
            else
                return new GenomePosition({ species:species, chrom:chrom, seq_start:(src_size-seq_end), seq_end:(src_size-seq_start) });
        }
        /**
        * Returns text.length
        **/
        public function get textSize():int
        {
            return text.length;
        }
        /**
        * The sequence size within the alignment text.
        * @see #seq_size
        **/
        public function get seqSize():int
        {
            return seq_size;
        }
        /**
        * Create a new Element and add to the list of Elements. Causes properties and displayList to invalidate.
        **/
        public function addTrackElement(track_start:int, track_end:int, track_index:int):Element
        {
            var el:Element;
            el = new Element(track_start * char_width, (track_end-track_start) * char_width, height, "", track_start, track_end);
            el.start_col = track_start
            el.end_col = track_end;
            el.track = this;
            newElements.push(el);
            invalidateProperties();
            invalidateDisplayList();
            return el;
        }
        private function parse_coord_map():void
        {
            col_to_coord = new Array(textSize);
            coord_to_col = new Array(seqSize);
            var cmptextposition:int = 0;
            var seqposition:int = 0;
            for (var i:int=0; i< text.length; i++)
            {
                var c:String = text.charAt(i);
                if (c != '-')
                {
                    col_to_coord[cmptextposition] = seqposition;
                    coord_to_col[seqposition] = cmptextposition;
                    seqposition++;
                }
                else
                {
                    if (cmptextposition==0)
                        col_to_coord[cmptextposition] = 0;
                    else
                        col_to_coord[cmptextposition] = col_to_coord[cmptextposition-1];
                }
                cmptextposition++;
            }
            //trace(name + '.text', text);
            //trace(name + ".col_to_coord", col_to_coord);
            //trace(name + ".coord_to_col", coord_to_col);

        }
        /**
        * Gives the alignment text offset, given a genomic coordinate.
        * @param seq_position Sequence position in genome coordinates.
        * @see coord_to_col
        **/
        public function seq_to_align_position(seq_position:int):int
        {
            var offset:int = 0;
            if (strand == "+") 
                offset = seq_position-seq_start;
            else
                offset = (src_size - seq_position) - seq_start -1; // minus one for GFF-based intervals

            if (offset < 0)
                return coord_to_col[0];
            else if (offset >= coord_to_col.length)
                return coord_to_col[coord_to_col.length-1];
            else
                return coord_to_col[offset];

        }

        /**
        * Calls Alert.show on the Array coord_to_col.
        * @see #coord_to_col
        **/
        public function show_coord_map(event:MouseEvent):void
        {
            Alert.show(coord_to_col.toString());
        }

        // MafGraph drawing functions
        override public function draw_segments(canvas:Sprite, yshift:Number, trackColor:int = 0x000033, gapColor:int = 0x888888):void
        {
            var str:String = text; // the alignment text
            var re:RegExp = /(-+)/;
            var segments:Array = str.split(re); // split by gap (-) runs
            var xloc:int = 0;
            for (var i:int=0; i<segments.length; i++)
            {
                var segment:String = segments[i];
                if (segment.length > 0)
                {
                    if (segment.charAt(0) == '-')
                        draw_gap_segment(xloc, yshift, segment.length, canvas, gapColor);
                    else
                        draw_nongap_segment(xloc, yshift, segment.length, canvas, trackColor);
                }
                xloc += segment.length;
            }
        }
        override public function draw_elements(canvas:Sprite, yshift:Number):void
        {
        }
        import flash.geom.Matrix;
        public function draw_gap_segment(x:int, y:int, len:int, slot:Sprite, fillColor:int=0x888888):void
        {
            var darkgrey:int = 0x242424;
            var white:int = 0xFFFFFF;
            var black:int = 0;
            //var mx:Matrix = new Matrix();
            //mx.createGradientBox(len ,6, Math.PI/2, 0, y+2)
            //slot.graphics.beginGradientFill(GradientType.LINEAR, [darkgrey, black], [1,1], [0, 0x7F], mx);
            slot.graphics.beginFill(fillColor);
            slot.graphics.drawRoundRect( x, y +2, len, 6, 3);
            //slot.graphics.lineStyle(0, grey);
            //slot.graphics.moveTo(x,y);
            //slot.graphics.lineTo(x+len,y);
            //slot.graphics.lineStyle(NaN);
            slot.graphics.endFill();
        }
        public function draw_nongap_segment(x:int, y:int, len:int, slot:Sprite, fillColor:int=0x000033):void
        {
            var darkgrey:int = 0x242424;
            var darkblue:int = 0x000033;
            var grey:int = 0x888888;
            var white:int = 0xFFFFFF;
            var black:int = 0;
            //var mx:Matrix = new Matrix();
            //mx.createGradientBox(len ,10, Math.PI/2, 0, y)
            //slot.graphics.beginGradientFill(GradientType.LINEAR, [darkgrey, black], [1,1], [0, 0x7F], mx);
            slot.graphics.beginFill(fillColor);
            slot.graphics.drawRoundRect( x, y, len, 10, 3);
            slot.graphics.endFill();
        }
    }
}

