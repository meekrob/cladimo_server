package mafComponents
{
    import mx.core.IUIComponent;
    import mafComponents.*;

    /**
    * Defines the properties for a component to reside in a <code>Block</code> and select areas within the <code>Block</code>.
    * 
    * <p>Try to use this interface in place of a class for the BlockSelection so that I can more easily build a replacement.</p>
    */
    public interface IBlockSelection extends IUIComponent
    {
        /**
        * Function to deliver event listeners to the subject <code>Block</code>s.
        **/
        function watch(block:Block):void;
        /**
        * Reset the selection on the argument <code>Block</code>, if provided.
        **/
        function reset(block:Block):void;

        /**
        * The <code>x</code> value computed by the implementing component, as determined by the selection.
        **/
        function get alt_x():Number;
        /**
        * The <code>y</code> value computed by the implementing component, as determined by the selection.
        **/
        function get alt_y():Number;
        /**
        * The <code>width</code> value computed by the implementing component as, determined by the selection.
        **/
        function get alt_width():Number;
        /**
        * The <code>height</code> value computed by the implementing component as, determined by the selection.
        **/
        function get alt_height():Number;
        /**
        * The genome positions of the active selection
        **/
        function get bounds():Array;
    }
}
