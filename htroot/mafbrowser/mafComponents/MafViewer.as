/* MafViewer:HBox
*   src_panel:Block
*   scroller:BlockScroller
*/
package mafComponents
{
    import mx.events.ScrollEvent;
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import mx.core.EdgeMetrics;
    import mx.controls.*;
    import mx.containers.*;
    import spark.components.Panel;
    import mx.rpc.events.ResultEvent;
    import mx.events.*;
    import flash.events.MouseEvent;
    import mx.utils.*;
    import mafComponents.Track;
    import mafComponents.Block;
    import mafComponents.events.BrowserScrollEvent;
    import mx.managers.CursorManager;

    /**
    * The main viewing window of the MafBrowser.
    **/
    public class MafViewer extends mx.containers.Panel
    {
        /**
        * A Block whose tracks are the species names.
        **/
        public var src_panel:Block;
        public var focus_info:TextArea;
        /**
        * A link to the graph panel.
        **/
        public var mafgraph:MafGraph;
        public var focus_collapse:Button;
        /**
        * The scrolling component which contains the Blocks.
        **/
        public var scroller:BlockScroller;
        /**
        * The horizontal scrollbar that positions the scroller.
        **/
        public var outer_scrollbar:HScrollBar;
        /**
        * A reference to the containing object: MafConsole.
        **/
        public var console:MafConsole;
        /**
        * The container for the two main components in this container: src_panel and scroller
        **/
        public var view_window:HBox;
        /**
        * A debugging TextArea.
        **/
        public var msg:TextArea;

        private var newBlocks:Array;
        private var newBlocksPending:Boolean = false;

        /**
        * Used during draggable tracks to reorder the species in the display.
        **/
        public var reorder_start_index:int;
        /**
        * Used during draggable tracks to reorder the species in the display.
        **/
        public var reorder_start_track:Track;
        /**
        * Returns scroller.block_height or measuredMinHeight.
        * @see BlockScroller#block_height 
        **/
        public function get block_height():int
        {
            if (scroller) return scroller.block_height;
            return measuredMinHeight;
        }
        /**
        * Constructor.
        **/
        public function MafViewer()
        {
            super();
            name = "MafViewer";
            layout = "vertical";
            status = "Alignment";
            horizontalScrollPolicy = "off";
            verticalScrollPolicy = "off";
            setStyle("borderThicknessBottom", 0);
            setStyle("borderColor", 0x8B0000);
            //setStyle("headerColors", [0xEEE8AA, 0xEEE8AA]);
            var red:uint = 0xFF0000;
            var blue:uint = 0x0000FF;
            setStyle("color", 0);
            setStyle("headerColors", [red, blue]);
            setStyle("accentColor", red);
            setStyle("borderAlpha", 1);
            setStyle("borderThicknessRight", 0);
            setStyle("borderThicknessLeft", 4);
            newBlocks = new Array();

        }
        /**
        * Adds a new Block to the pending array and invalidates properties.
        **/
        public function addBlock(newBlock:Block):void
        {
            newBlocksPending = true;
            newBlocks.unshift(newBlock);
            invalidateProperties();
            invalidateDisplayList();
        }
        /**
        * In addition to super(), calls scroller.addBlock on all pending Blocks.
        * @see BlockScroller#addBlock
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (newBlocksPending)
            {
                if (scroller)
                {
                    var newBlock:Block;
                    do
                    {
                        newBlock = newBlocks.pop();
                        newBlock.addEventListener(FlexEvent.CREATION_COMPLETE, function():void
                        {
                            invalidateDisplayList(); // mafViewer must refresh its display list with each rendered block
                                                     // in order to resize the outer_scrollbar and other measurements
                        });
                        scroller.addBlock(newBlock);
                        trace(newBlock.name + ".addEventListener: TextSizeChanged: console.mafgraph.draw_block(", newBlock, ")");
                        newBlock.addEventListener("TextSizeChanged", function(e:Event):void
                        {
                            trace("Event: TextSizeChanged: console.mafgraph.draw_block(", e.target.name, ")");
                            console.mafgraph.draw_block(e.target as Block);
                        });
                    } while (newBlocks.length > 0);
                    newBlocksPending = false;
                }
            }
        }
        override protected function measure():void
        {
            super.measure();
            measuredMinHeight = 100;
            measuredMinWidth = 100;
            measuredHeight = 100;
            //maxHeight = 300;
        }
        public function outer_scroll(event:ScrollEvent):void
        {
            scroller.horizontalScrollPosition = event.target.scrollPosition;
            var browserEVT:BrowserScrollEvent = new BrowserScrollEvent(event, scroller, view_window);
            dispatchEvent(browserEVT);
            trace(name + "dispatchEvent(browserEVT)");
        }
        public function vertical_scroll(event:ScrollEvent):void
        {
            var browserEVT:BrowserScrollEvent = new BrowserScrollEvent(event, scroller, view_window);
            console.dispatchEvent(browserEVT);
        }
        public function show_stuff():void
        {
                //title = StringUtil.substitute("{0} {1} {2} {3}", scroller.text_scroll_position, vertical_scroll_position, scroller.text_scroll_width, text_scroll_height);
        }
        public var button_panel:ButtonPanel;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (button_panel)
            {
                
                var leftSide:int = 0;
                var xoffset:int = leftSide;
                var yoffset:int = 0;
                for (var i:int; i< button_panel.numChildren; i++)
                {
                    var b:DisplayObject = button_panel.getChildAt(i);
                    b.height = button_panel.buttHeight;
                    if ((xoffset + b.width) > unscaledWidth)
                    {
                        xoffset = leftSide;
                        yoffset += button_panel.buttHeight + button_panel.verticalGap;
                    }
                    xoffset += b.width + button_panel.horizontalGap;
                }
                button_panel.height = yoffset + button_panel.buttHeight;
            }
            src_panel.width = Math.max(50, src_panel.track_width);

            //scroller.width = unscaledWidth - src_panel.track_width - focus_info.width - 40;
            scroller.width = unscaledWidth - src_panel.track_width - 40;
            view_window.width = unscaledWidth - 20;
            outer_scrollbar.height = 20;
            view_window.height = unscaledHeight - (outer_scrollbar.height * 3) - button_panel.height;
            outer_scrollbar.maxScrollPosition = scroller.maxHorizontalScrollPosition;
            outer_scrollbar.lineScrollSize = scroller.horizontalLineScrollSize;
            outer_scrollbar.pageScrollSize = scroller.horizontalPageScrollSize;
            outer_scrollbar.scrollPosition = scroller.horizontalScrollPosition;
            outer_scrollbar.width = scroller.width;
            outer_scrollbar.move(outer_scrollbar.x + src_panel.getExplicitOrMeasuredWidth() + (view_window.getStyle("horizontalGap") as Number),  outer_scrollbar.y);
        }
        /**
        * In addition to super(), handles creation of button_panel, view_window, outer_scrollbar, src_panel, and scroller.
        **/
        override protected function createChildren():void 
        {
            super.createChildren();
            if (! button_panel)
            {
                button_panel = new ButtonPanel();
                button_panel.name = "MafViewerButtonPanel";
                button_panel.setStyle("backgroundColor", 0xFFFFFF);
                addChild(button_panel);
            }
            if (! view_window)
            {
                view_window = new HBox();
                view_window.name = "MafViewerWindow";
                view_window.horizontalScrollPolicy = "off";
                view_window.verticalScrollPolicy = "on";
                addChild(view_window);
            }
            if (! outer_scrollbar)
            {
                outer_scrollbar = new HScrollBar();
                outer_scrollbar.name = "MafViewerScrollbar";
                outer_scrollbar.addEventListener(ScrollEvent.SCROLL, outer_scroll); 
                view_window.addEventListener(ScrollEvent.SCROLL, vertical_scroll); 
                outer_scrollbar.setStyle("bottom", 0);
                addChild(outer_scrollbar);
            }
            if (! src_panel)
            {
                src_panel = new Block();
                src_panel.name = "src_panel_Block";
                src_panel.setStyle("borderStyle", "none");
                var black:uint = 0x000000;
                //src_panel.setStyle("chromeColor", black);
                view_window.addChild(src_panel);
            }
            if (! scroller)
            {
                scroller = new BlockScroller(this);
                scroller.name = "BlockScroller";
                scroller.msg = msg;
                //scroller.addEventListener(mx.events.FlexEvent.CREATION_COMPLETE, set_info_listeners);
                scroller.horizontalScrollPolicy = "off";
                view_window.addChild(scroller);
            }
        }
        /*public function set_info_listeners(evt:Event):void
        {
            scroller.selection.addEventListener("selectionChange", update_focus_info);
        }
        public function update_focus_info(evt:Event):void
        {
            var bounds:Array = scroller.selection.bounds;
            focus_info.text = 'position:\n';
            var line_offset:int = console.species_list.indexOf(bounds[0].species);
            for each (var line:Object in bounds)
            {
                if (line != null)                
                    focus_info.text += line.chrom + ":" + line.seq_start + "-" + line.seq_end + "\n";
                else
                    focus_info.text += "\n";
            }
            var max_focus_width:Number = 0;
            if (focus_info && (focus_info.text.length > 0))
            {
                var lines:Array = focus_info.text.split("\n")
                if (lines.length > 1)
                {
                    for each (var info_line:String in lines)
                    {
                        var nbDim:TextLineMetrics = focus_info.measureText(info_line);
                        max_focus_width = Math.max(nbDim.width, max_focus_width);
                    }
                }
            }
            focus_info.width = max_focus_width + 20;;
            invalidateDisplayList();
        }*/
    }
}
