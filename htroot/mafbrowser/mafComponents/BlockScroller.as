/* BlockScroller:HBox
*   blocks:Array 
*   public function addBlock(newBlock:Block):void
*
*/
package mafComponents
{
    import mx.events.ScrollEvent;
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import mx.collections.ArrayCollection;
    import mx.core.EdgeMetrics;
    import mx.controls.*;
    import mx.containers.*;
    import mx.rpc.events.ResultEvent;
    import flash.events.MouseEvent;
    import mx.utils.ObjectUtil;
    import mafComponents.Track;
    import mafComponents.Block;
    import mafComponents.events.BlockChangedEvent;

    /**
    * The Horizontal container for Blocks in the main window.
    **/
    public class BlockScroller extends HBox
    {

        // Change this variable to use a different BlockSelection class
        //private var IBlockSelectionImplementor:Class = BlockSelection;
        private var IBlockSelectionImplementor:Class = SparkBlockSelection;

        /**
        * The reference species in the alignment.
        **/
        public var reference_species:String;
        private var totalWidth:int;
        /**
        * A debugging window.
        **/
        public var msg:TextArea;
        /**
        * The currently selected Block.
        **/
        public var selected_block:Block;
        private var selected_blockChanged:Boolean = false;
        /**
        * A special list of Blocks in the container and the display list of this container.
        **/
        public var blocks:Array;
        private var blocksChanged:Boolean = false;
        private var newBlocks:Array;
        private var newBlocksChanged:Boolean = false;
        

        [Deprecated]
        public var _text_scroll_position:int;
        [Deprecated]
        public var _text_scroll_width:int;
        /**
        * The rectangular selection component.
        **/
        public var selection:IBlockSelection;
        private var _mafviewer:MafViewer;

        /**
        * A reference to the MafViewer window of the application.
        **/
        public function get mafviewer():MafViewer { return _mafviewer; }

        private var _blockScrollMap:Array;
        private var _blockScrollTargets:Array;

        /**
        * Constructor.
        **/
        public function BlockScroller(_mv:MafViewer)
        {
            super();
            _mafviewer = _mv;
            verticalScrollPolicy = "off";
            horizontalScrollPolicy = "on";
            setStyle("horizontalGap", 0);
            //addEventListener(ScrollEvent.SCROLL,show_scroll_rect);
            blocks = new Array();
            newBlocks = new Array();

            _blockScrollMap = null;
            _blockScrollTargets = null;
        }

        import mx.core.UIComponent;
        public function getBlockIndexAtX(x:uint):int
        {
            if (_blockScrollMap && _blockScrollTargets)
            {
                var i:uint = 0;
                do
                { 
                    if (i >= _blockScrollMap.length) return -1;
                }
                while (_blockScrollMap[i++] < x);

                return i-1;
            }
            return -1;
        }
        public function getBlockAtX(x:uint):UIComponent
        {
            var block_i:int = getBlockIndexAtX(x);
            if (block_i >=0)
                return _blockScrollTargets[block_i];

            return null;
        }
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth,unscaledHeight);

            /*
            create a byPixel map of the valid block components along the width
            */
            var child_i:uint = 0;
            var compWidth:uint = 0;
            _blockScrollMap = new Array();
            _blockScrollTargets = new Array();

            while (child_i < numChildren)
            {
                var d_o:DisplayObject = getChildAt(child_i);
                if (d_o is UIComponent)
                {
                    var uic:UIComponent = d_o as UIComponent;
                    var val:Number = uic.getLayoutBoundsX() + uic.getLayoutBoundsWidth();
                    //trace("scroller.updateDisplayList:", uic.name, val);
                    _blockScrollMap.push(val);
                    _blockScrollTargets.push(uic);
                }
                child_i += 1;
            }
        }

        /**
        * In addition to super(), creates an instance of the IBlockSelectionImplementor.
        **/
        override protected function createChildren():void
        {
            super.createChildren();
            if (! selection)
            {
                selection = new IBlockSelectionImplementor();
                // is a floating child going from Block to Block
            }
        }

        import mx.events.FlexEvent;

        /**
        * In addition to super(), calls renderBlock on any pending Block in newBlocks.
        * @see renderBlock
        * @see newBlocks
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();

            if (newBlocksChanged)
            {
                trace("BlockScroller.commitProperties(): adding", newBlocks.length, "new Blocks");
                var newBlock:Block;
                //do
                {
                    newBlock = newBlocks.pop();
                    renderBlock(newBlock);
                    newBlock.addEventListener("ComponentTracksChanged", function(e:Event):void
                    {
                        trace(name + ".listenedTo('ComponentTracksChanged') from", e.target, e.currentTarget);
                        dispatchEvent(new BlockChangedEvent( Block(e.target)));
                        selection.reset(Block(e.target));
                    });
                    newBlock.addEventListener(FlexEvent.CREATION_COMPLETE, function(e:FlexEvent):void
                    {
                        if (newBlocks.length > 0) 
                        {
                            invalidateProperties(); // "schedule" another commitProperties after this one
                        }
                    });
                }// while (newBlocks.length > 0);
                if (newBlocks.length == 0) newBlocksChanged = false;
            }
        }

        /**
        * Returns blocks[0].single_base_width
        * @see Block#single_base_width
        **/
        public function get single_base_width():int
        {
            if (blocks) return blocks[0].single_base_width;
            return 0;
        }
        /**
        * Returns blocks[0].track_height
        * @see Block#track_height
        **/
        public function get single_base_height():int
        {
            if (blocks) return blocks[0].track_height;
            return 0;
        }
        /**
        * Returns blocks[0].char_width
        * @see Block#char_width
        **/
        public function get char_width():Number
        {
            return blocks[0].char_width;
        }
        /**
        * Returns blocks[0].char_height
        * @see Block#char_height
        **/
        public function get char_height():Number
        {
            return blocks[0].char_height;
        }
        /**
        * If blocks, returns blocks[0].block_height
        * @see Block#block_height
        **/
        public function get block_height():int
        {
            if (blocks && blocks.length > 0) return blocks[0].block_height;
            return height;
        }
        /**
        * Returns horizontalScrollPosition/char_width
        * @see #char_width
        **/
        public function get text_scroll_position():int
        {
            return horizontalScrollPosition/char_width
        }
        /**
        * Returns height/char_height
        * @see #char_height
        **/
        public function get text_scroll_height():int
        {
            return height/char_height;
        }
        /**
        * Returns width/char_width
        * @see #char_width
        **/
        public function get text_scroll_width():int
        {
            return width/char_width
        }

        /**
        * Debugging: Attaches a message window (a TextArea), to each block.
        **/
        public function setWindow(window:TextArea):void
        {
            for (var i:int = 0; i<blocks.length; i++)
            {
                blocks[i].msg = window;
            }
            msg = window;
        }
        /**
        * Calls addBlock on an array of Blocks
        * @see #addBlock
        **/
        public function loadBlocks(blocks:Array):void
        {
            var nblocks:int = blocks.length;
            var block_i:int = 1;
            for each(var block:Block in blocks)
            {
                addBlock(block);
                _mafviewer.status = "loaded block " + block_i + " of " + nblocks;
                block_i += 1;
            }
            selection.reset(blocks[0]);
        }
        /**
        * Adds a Block to the Array of pending Blocks newBlocks
        * @see #newBlocks
        **/
        public function addBlock(newBlock:Block):void
        {
            newBlocks.push(newBlock);
            newBlocksChanged = true;
            invalidateProperties();
            invalidateSize();
            invalidateDisplayList();
        }
        private var spacerBlocks:int = 0;
        /**
        * Invoked during the commitProperties() part of the update cycle, this method processes a new Blocks for the display list.
        *
        * <p>
        * The Tracks of the new Block are scanned to see if they are continuous with the new Block. If they are not, short notes are
        * added to the corresponding track position of a SpacerBlock, which is added in front of (to the left of), the pending Block.
        * </p>
        *
        * <p>
        * Additionally, a width measurement of the Block as added to the BlockScroller tally, and Block.selection and Block.msg are set to the
        * values held in the BlockScroller.
        * </p>
        * @see #msg
        * @see #selection
        **/
        protected function renderBlock(newBlock:Block):void
        {
            if (blocks.length > 0)
            {
                var spacer_block:Block = new SpacerBlock();
                spacer_block.name = "SpacerBlock_" + spacerBlocks;
                spacerBlocks++;
                var prev_block:Block = blocks[blocks.length-1];
                // scan tracks for continuity
                var i:int = 0;
                // currently EMPTY: never executes
                for each (var track:Track in newBlock.tracks)
                {
                    if ((track is ComponentTrack) && (prev_block.tracks[i] is ComponentTrack))
                    {
                        var prev_track:ComponentTrack = prev_block.tracks[i];
                        var cur_track:ComponentTrack = ComponentTrack(track);
                        if (prev_track.chrom == cur_track.chrom)
                        {
                            var gap:int = cur_track.seq_start - prev_track.seq_end;
                            if (gap > 0)
                                spacer_block.addTrack(".." + gap + "bp.");
                            else
                                spacer_block.addTrack(" ");
                        }
                        else 
                        {
                            //spacer_block.addTrack(prev_track.chrom + "/" + cur_track.chrom);
                            var infoTrack:Track = spacer_block.addTrack("con/tig");
                            infoTrack.toolTip = "contig change from " + prev_track.chrom + " to " + cur_track.chrom;
                        }
                    }
                    else
                    {
                        spacer_block.addTrack(" ");
                    }
                    spacer_block.last_track.setStyle("textAlign", "center");
                    i++;
                }
                addChildAt(spacer_block, 0);
                trace("renderBlock: added spacer_block:" + spacer_block.name);
                blocks.push(spacer_block);
            }
            addChildAt(newBlock, 0);
            trace("renderBlock: added newblock:" + newBlock.name);
            newBlock.scroller = this;
            newBlock.selection = selection;
            newBlock.msg = msg;
            blocks.push(newBlock);
        }
    }
}
