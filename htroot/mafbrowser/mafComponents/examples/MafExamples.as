package mafComponents.examples 
{
    public class MafExamples
    {
        public static const ghp010:XML = <maf name="ghp010.maf">
	<species_list>
		<species id="mouse" order="0"/>
		<species id="rat" order="5"/>
		<species id="chimp" order="4"/>
		<species id="human" order="3"/>
		<species id="rhesus" order="4"/>
		<species id="cow" order="5"/>
		<species id="dog" order="6"/>
		<species id="armadillo" order="7"/>
		<species id="elephant" order="8"/>
		<species id="fugu" order="9"/>
	</species_list>
	<block score="44123.0" text_size="21">
		<component size="4" species="mouse" src="mouse.chr7" src_size="145134094" start="66075464" strand="+" text="TTCC-----------------"/>
		<component size="4" species="rat" src="rat.chr1" src_size="267910886" start="120622624" strand="+" text="TTCC-----------------"/>
		<component size="4" species="chimp" src="chimp.chr15" src_size="100063422" start="868633" strand="-" text="tttc-----------------"/>
		<component size="4" species="human" src="human.chr15" src_size="100338915" start="831439" strand="-" text="tttc-----------------"/>
		<component size="4" species="rhesus" src="rhesus.chr7" src_size="169801366" start="88526819" strand="-" text="tttc-----------------"/>
		<component size="4" species="cow" src="cow.chr21" src_size="49724630" start="2856757" strand="+" text="tttc-----------------"/>
		<component size="4" species="dog" src="dog.chr3" src_size="94715083" start="42796225" strand="+" text="tctc-----------------"/>
		<component size="4" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3029" strand="-" text="tttc-----------------"/>
		<component size="4" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3631" strand="+" text="tttc-----------------"/>
		<component size="21" species="fugu" src="fugu.chrUn" src_size="349519338" start="216212737" strand="+" text="TTCCCTACAAAATCCTCATTC"/>
	</block>
	<block score="23042.0" text_size="45">
		<component size="43" species="mouse" src="mouse.chr7" src_size="145134094" start="66075468" strand="+" text="ACAAATCCTGAGAGTTGATTT--GAAGACCCCCAGAATAAATATT"/>
		<component size="42" species="rat" src="rat.chr1" src_size="267910886" start="120622628" strand="+" text="ACAGATCCCAAGACTTGATTT--GAAGACCCC-AAAATAAATATT"/>
		<component size="39" species="human" src="human.chr15" src_size="100338915" start="831443" strand="-" text="---aaggacaggtgctaattt--GGAAAATCC-CAAATACAtgta"/>
		<component size="39" species="chimp" src="chimp.chr15" src_size="100063422" start="868637" strand="-" text="---aAGGACAGGCGCTAATTT--GGAAAACCC-CAAATACATGTA"/>
		<component size="40" species="cow" src="cow.chr21" src_size="49724630" start="2856761" strand="+" text="--aaagatgctatgctaattt--gtaAAACCC-AAAATAAGTTTT"/>
		<component size="40" species="dog" src="dog.chr3" src_size="94715083" start="42796229" strand="+" text="--aaagatgctgtgctaattt--GCAAAACCC-CAAATAAGTTTC"/>
		<component size="40" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3033" strand="-" text="--aaaaatgttatgctaatGT--GCAGAACCC-AAAATAAATTTC"/>
		<component size="41" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3635" strand="+" text="--acaaacgctgtgccaatct--GCAAAACCCCAAAATAATTTTT"/>
		<component size="42" species="fugu" src="fugu.chrUn" src_size="349519338" start="216212758" strand="+" text="AAAATTTCTACAAGCTCACTTCAGTAGAACTG-GGTAAAAATG--"/>
	</block>
	<block score="176158.0" text_size="275">
		<component size="217" species="mouse" src="mouse.chr7" src_size="145134094" start="66075511" strand="+" text="TCGTGC--TGCTT-TAAGAT-AGAGCAGGGC-TTCAGAACACGC---CAGGAT--------GTCTCTTCAGC-TTTTGGA---AGGCTCCAGTGTCTG--TGTTTCTGGAAGGTTCTAATCAGCAGCTCTATTTTTGTGGGT---GGCTGGCGC-GGCAGAGAGGCTCTGTGGCTGGGTGG----CTGATAAGGTTATCTGGGGACTGTTTCTGG-----CAGTTCAAGTGCCTGTTGT-----------------------ACAACTTGGGGCT"/>
		<component size="217" species="rat" src="rat.chr1" src_size="267910886" start="120622670" strand="+" text="CCGCAC--TGCTT-TAAGAT-AGAGCAGGGC-TTCAGAGCGCGC---CAGGAT--------GTCTCTTCAGC-TTTTGGA---AGGCTCCAGGGTCAG--TGTTTCTGGAAGGTTCTAATCAGCAGCTCTATTTTTGTGGGT---GGCTGGCGC-GGCAGGAAGGCTCTGTGGCTGGGTGG----CTGATAAGGTTATCTGGGGACTGTTTCTGG-----CAGTTCAAGTGCCTGTTGT-----------------------ACAACTTGGGGCT"/>
		<component size="213" species="chimp" src="chimp.chr15" src_size="100063422" start="868801" strand="-" text="TCGTAA--TTTTT-AAAGAT-AGAGTGGGGCAGGAAAAGCTCAC---CAGAAT--------GTCTCTCCAGC-CCTTGAA---GGGCTCCAGCCTTGG--TG--TCTGGAAGGCTCTAATCAGCAGCTCTGTTTTTACGGGGCCAGGCTGGAGCAGACAGGAAGGCTTTAT-GCTATCTGA----CTGATAAGGTTGTCTGGGGACTGTTTCCATACCACCGGTTTCTG---------------------------------GGGACCTGGCAC-"/>
		<component size="246" species="human" src="human.chr15" src_size="100338915" start="831607" strand="-" text="TCGTAA--TTTTT-AAAGAT-AGAGTGGGGCAGGAAAAGCTCAC---CAGAAT--------GTCTCTCCAGC-CCGTGAA---GGGCTCCAGTCTTGG--TG--TCTGGAAGGCTCTAATCAGCAGCTCTGTTTTTACGGGGCCAGGCTGGAGCAGACAGGAAGGCTTTAT-GCTATCTGA----CTGATAAGGTTGTCTGGGGACTGTTTCCACACCACCGGTTTCTGTGCTTGTTGCTGTGGGCTTGGGTCTGGGGACCTGGGACCTGGCAC-"/>
		<component size="243" species="cow" src="cow.chr21" src_size="49724630" start="2856801" strand="+" text="TTGCAA--TTTTTCAAAGGTGATAGTGCAGCAAGAAAAGTTTCC---CAGAAT--------ATCCCTCCAATGCCTTGGA---GGGCTCTTTCCTTACAGAGTTTCAGGAAGGTTCTAATCAGCAGCTCTATTTTTGTGAGATCAGGTTGTGGTAGACAGG-AGGCTGTGTGGCTATCAGG---ACTGATAAGGGTATTTGGGGACTGTTTGTGG----CCAGTTTAAATGCCTGCTGTATAGGGCTGTAGTCTG-------GGGACCCGGTGA-"/>
		<component size="241" species="dog" src="dog.chr3" src_size="94715083" start="42796269" strand="+" text="TTATAC--TTTTTTAAAGAT-AGAGTAAGGCAGGAAAAGCTCAC---TTGGAT--------GTCTCTCTGGCACCCCGGA---GGGCTCTGGCCTTAC--TGCTTGTGGAAGGTCCTAATCAGTGGCACTATTTTTGTGTAGCCAGGCTGCAGCAGACAGGAAGGCTTTGTGGCTATCTGG---ACTGATAAGATTATTTAAGGACTGTTTCTGG----CCAGTGTAAATGCCTGCTGTGCAGGCCTCCAGTCTG-------GGGACCTGGTAA-"/>
		<component size="251" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3073" strand="-" text="TTGCAAATTTTTT-CAAGAT-CGAGTGTGGCAGGAAAATCTCACTCACAGGGTACTGCAAGGTCTCTCCAGT-CCTCAGA---GGGCGGTGGCTTTAG--TGGCTCTGGAAGTTTCTAATCACTGGCTCTATTTTTGTGGGGCCAGGCTGGAGCAGATCGGAAGGCTTTATGGCTGTCTCA---ACTGATAAGGTTATTTGGAGAATGTTTCTGG----ACAGTTTAAGTGCCTATTGTGCAGGGCT-TGGTCTG-------GGGGCCTGGGGC-"/>
		<component size="200" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3676" strand="+" text="------------T-AAAAAG-AAAGTAGTGT-ACCAAAGCTCAT---CAGAAT--------ACCTCTTTAGT---AAGGA---GGGTGCTGGCCTTAG--TGATTCTGGAAGGTTCTAATCAGTGGCTCTATTTTTGTGGGGCCAGGCTGGAATAGGTAGGAAGGCTTTAT---------------------------CTGGGGAGTGGCCGTGG----CCTGTTGAAGTGCCTATTGTACAGACT--CAGTCTG-------GGGACCTACGGC-"/>
		<component size="209" species="fugu" src="fugu.chrUn" src_size="349519338" start="216212800" strand="+" text="--------TCTAT-CTCACT-TGAACAGGGCTGTTGATTACAAC---CAAAGA--TCTGAGGTTTTTCCAAC-TGTGGGCAGTGGATTCTGACTCCAT--T---TCTGGAGGGTTCAAAATAGCT------TCTGTACGGACTCAGGT---------CAGGTGGTCCATAT-GTTACTGGGATCACTGACAAGTTA--TTGGTGAATGACTTATT----TCAATCTAATTTCAAGTTATTTTCATT----------------GCAGCT-------"/>
	</block>
	<block score="1672.0" text_size="81">
		<component size="70" species="mouse" src="mouse.chr7" src_size="145134094" start="66075728" strand="+" text="GGGGAAGTCGGA----GGGCTGGGCAGGGCGGGG-ACTTCGTC------GTCTTAGAGCTTGCCTTTCTGTGAGAGAGGAA"/>
		<component size="72" species="rat" src="rat.chr1" src_size="267910886" start="120622887" strand="+" text="GGGGAAGTCAAAGGCTGGGCGGGGCCGGGTGGAG-ACGTCAT--------TCTCAGAGCTTGCCTTTCTGTGAGGGAGGAA"/>
		<component size="73" species="chimp" src="chimp.chr15" src_size="100063422" start="869014" strand="-" text="--GGAGCTATGG----GGGCTCTGACTGTCTCAG-ACCCTGCCTCTGCTGTCTTAGAGCTCACACTTTAGGT-GGGGGGAA"/>
		<component size="74" species="human" src="human.chr15" src_size="100338915" start="831853" strand="-" text="--GGAGCTATGG----GGGCTCTGACTGTCTCAG-ACCCTGCCTCTGCCATCTTAGAGCTCACACTTTAGGTAGGGGGGAA"/>
		<component size="70" species="cow" src="cow.chr21" src_size="49724630" start="2857044" strand="+" text="--GGGTGAC-------AGGCTCTCTTTG-TAGAG-ACTCCTCTTCCACCCTCGCAAAGTTTGTGCTTCGGGTGCGGAGTGA"/>
		<component size="43" species="dog" src="dog.chr3" src_size="94715083" start="42796510" strand="+" text="--GG-------------GGCTCTGT-----------------------CTTCCCAGAGCCTGCATTTTAAATGAGAGGTGA"/>
		<component size="74" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3324" strand="-" text="--AGGACCCTG-----GGGCTCTGTGGGCATCATCATCCCACCCCCCCACCCTTTGCAGTTGTGCTCTATGTGGGGAGTGA"/>
		<component size="69" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3876" strand="+" text="--AGGACTGT------GGGCTCTGCCTGTGTCAG-ACCCAGTCTCTGCCCTCCTGGAGCTCCTGCTTTAGGTGAGGAG---"/>
	</block>
	<block score="-2396.0" text_size="40">
		<component size="34" species="mouse" src="mouse.chr7" src_size="145134094" start="66075798" strand="+" text="TAAAACCAAAGGTTC-CACTACCCAGAGGC-----CCCTC"/>
		<component size="34" species="rat" src="rat.chr1" src_size="267910886" start="120622959" strand="+" text="TAGAACTAAGGGCTC-CACTGTCCAGAGGC-----TCCTC"/>
		<component size="40" species="chimp" src="chimp.chr15" src_size="100063422" start="869087" strand="-" text="TGAAGCAACCGGTGCTCTCTAACCAGAGTCGGGAAGCCTC"/>
		<component size="40" species="human" src="human.chr15" src_size="100338915" start="831927" strand="-" text="TGAAGCAACCGGTGCTCTCTAAGCAGAGTCGGGAAGCCTC"/>
		<component size="38" species="cow" src="cow.chr21" src_size="49724630" start="2857114" strand="+" text="CGCATCTGAGGGCAC-TACCAACT-GGATCAGGCAGCCTT"/>
		<component size="39" species="dog" src="dog.chr3" src_size="94715083" start="42796553" strand="+" text="AGCCACCAGGAGGGT-TGCCAGCCAGGATCAGGGAGCCTT"/>
		<component size="23" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3954" strand="+" text="AGCACCCATCGGGAT-TTCTAGTC----------------"/>
	</block>
	<block score="2588.0" text_size="87">
		<component size="86" species="mouse" src="mouse.chr7" src_size="145134094" start="66075832" strand="+" text="ATCTCCACCATCCCAAGGCTCCCCGCTGTAGACTGTTGGGTATAGATAGGTAGGAATGGTTGTAGCAT-CCTATGACATAGCCCAAA"/>
		<component size="61" species="dog" src="dog.chr3" src_size="94715083" start="42796794" strand="+" text="GCTTAC------TCGGTGC-TCTCGTGTGGGCCTTTCAGG-------AGGCCTGGGTGGTGATGGCCA------------GGGCCAA"/>
		<component size="68" species="cow" src="cow.chr21" src_size="49724630" start="2857152" strand="+" text="GCTCAC------ACGGTGG-TCTTGCGTTGTTCTCTCAAGAAAGTAAAGGCCTGGGTGATTGTGGCCA------------GGCCCAA"/>
		<component size="66" species="human" src="human.chr15" src_size="100338915" start="831967" strand="-" text="ATGTAC------CCAGGG--CCTTGCTTTGGCCTCTCAGGGGTC-GCAGGCAAGACTGGCTTGGTCAG------------GCCCAGA"/>
		<component size="66" species="chimp" src="chimp.chr15" src_size="100063422" start="869127" strand="-" text="ATGTAC------CCAGGG--CCTTGCTTTGGCCTCTCAGGGGTT-GCAGGCAAGACTGGCTTGGTCAG------------GCCCAGA"/>
		<component size="75" species="rat" src="rat.chr1" src_size="267910886" start="120622993" strand="+" text="ATCT--------CCAAAGCTCCCTGCATTAGACTATTGGGT----ATAGGTAGGGATGACTGTAGCATGACTGTCACGTAGCCCAAG"/>
		<component size="77" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="3988" strand="+" text="GCTTAC------CCATTGC-CCTTCCCTTGGACTCTCAGGGGGATGCAGGCAGGAGTGACTGTGGTCAGGCACTGGCAGCGTCT---"/>
	</block>
	<block score="-1898.0" text_size="348">
		<component size="209" species="mouse" src="mouse.chr7" src_size="145134094" start="66075918" strand="+" text="GGGT---------------GTACCTCCGT----------GCAGCCCAGCTGGACCCACCAGGAGCCAGTGGGGTCA------------------------GGAGAGTTGGGTCCAGCCTAGACAAGAGACTGG-------------CAG--------------------------TGGAGTTAGATCACAACCTATAGACGGACAGGGCA--GGCACCCC-----ACCCACCATTCAA----------CCACTGTGCACTCC----------GAACAGACAGGGC--------TCAGGG--GAGGAAGC--------------CTGCTAGCCCTTGGGCCCACACATG"/>
		<component size="237" species="rat" src="rat.chr1" src_size="267910886" start="120623068" strand="+" text="GGGTGTCCTAGGATGGTGGGTACCTCCAT----------GCAGCCCAGTCAGACCCACCAGGAG-CAGGGGGGAGA------------------------AGAGAGCTGGGCCCAGCCTAGACAAGAGATTGG-------------CAG--------------------------TGGAGTTAGATCCCAGCCTATAGACAGGCAGGGCA--GGCATCCC-----ACCCACCATTCAA----------TCACTGTGCACTCG----------GAACAGCCAGGGC--------TCAGGG--AAGGGAGCAACTTCCCCCACAATAGATGGCCCTCAGGCCCACACATG"/>
		<component size="290" species="chimp" src="chimp.chr15" src_size="100063422" start="869193" strand="-" text="GGGTGCCCTGGGAAGGTGGGCCTCTCAGT--TCAGGCTGGAAGCCCAGCTAGGGTGGTCAGGCC-CTGAGGGCTGGAGGTGT--------------AGGTAGAAAGGTGCCCTCAG-GGAAACAACAGAAACGCCCAGTC------CAG--------------------------TGGCAATAAAGCCCCAAGGACAGGCAGACAGTGCA-GGGCACCCCCA---GGCCGCCAGCCCAGAACCTT-TCCCACTGGGGTCTGGAACTTTCCGTGGGCAGACAGAGCCCCACGGGCCAGGGCTGAGGGATCCATTGTCCCCACACTAGCAGGGCCCCTGACCCACTC---"/>
		<component size="290" species="human" src="human.chr15" src_size="100338915" start="832033" strand="-" text="GGGTGCCCTGGGAAGGTGGGCCTCTCAGT--TCAGGCTGGAAGCCCAGCTAGGGTGGTCAGGCC-CTGAGGGCTGGAGGTGT--------------AGGTAGAAAGGTGCCCTCAG-GGAAACAACAGAAACGCCCAGTC------CAG--------------------------TGGCAATAAAGCCCCAAGGACAGGCAGGCAGTGCA-GGGCACCCCCA---GGCCGCCAGCCCAGAACCTT-TCCCACTGGGGTCTGGGCCTTTCCATGGGCAGACAGAGCCCCACGGGCCAGGGCTGAGGGATCCATTGTCCCCACACTAGCAGGGCCCCTGACCCACTC---"/>
		<component size="289" species="cow" src="cow.chr21" src_size="49724630" start="2857220" strand="+" text="CTGAACCCCAAGAGGGT------GTGAGTGCTCAGGCCAATAGCCCAGGTAGGGAGGCTGAGCC-CTGAGGGTCAGAGCTGTCCTGGGCCCAGCTCAGGTTC-------CCTCCCTGGGAACCAGTTGGAGGA-----TC------CAGG-------------------------CAGCATCAAAGCCCAGGGAGCAGTGAGTGGGTGCAGGGATGTCCC-----TGCCCTCCTGCTAAAGCCTT-TCCCTCTGGCTGCTGGGCCTTCCCAGGGACAG--AGGGACCTGTGGCTCAAGGGCGAGGG-GCAAGTGTCCCCAGACTGGCTGGGCTGCAAACCCACACGCA"/>
		<component size="310" species="dog" src="dog.chr3" src_size="94715083" start="42796855" strand="+" text="GGGGACCCTGAGAGGGT------CTCAGCTCTC-TCCCCGTGGCCCAGCTGTG--------------GGGGGTTAGAGACGTGGTGGGTCCAGCTCAGGAGAAGCAGGGCCCTCAGGGAAAACAGC---AAGG-----TC------CAGGACTAAACACTCCAAACAAACACTCCAAGCACTAAAGCCCAGAAGGCAGACAAGTGGTGCAGGGGTGGTTCAGGGGTGGCAAAGGTCCAGAACCTT-TGCCTTTGGTGACTGAGCCTTCCCA-GGACAGACAGGGACCCATGGCTTGAGGATGAAGG-GCAGGTGTCCCCACACTGGCTGCGTCACAGACCCCCATGCA"/>
		<component size="227" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3547" strand="-" text="-------------AGATGGGA--CTTGGT----------CCAGCTCAGGCAGAGCAG--GGGCC-CTCAGGGAACAACA---------------------GGAGAGTCGTCCTCAC----AAAAGCCCAGGGG-------------CAG--------------------------CTGCCTC--TGCAAGGAGGGCAGATAAGTGGTCCAGGGGTGCCTGCTGTTCTCCCCTACCCTGGAACCTTCTCGCACTGGA-ACGGGGCCTTCCCG-GGGCAGACAGGGC-CCATGGCTCAGGGAGGAGGGGGCATCTGTCCACACACT------------------------"/>
		<component size="243" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="4065" strand="+" text="-------------------------CAGC--CCAGGGAAATGGCCCAGCCAGGGTGACCCAGCC-CTGGGGCTTAAATCTGAGTTGGGCCCTGCTCAGA-AAAGACCTGCCCTCCG-----TCAGCACCAGGG-----TCTGGGGGCAG--------------------------CTGCCTTTG---CAAAAGGGCAGGCAAGCAGTGCA-GGATGCCCCACCCGCACCCCCAGCCTGGAACCTTTTCACACTGGGAGCTGGGCCCTCCCAGGGACAGACAGGGT-CCATGGCTCAGCAGCGAGGG-GCACATG----------------------------------"/>
	</block>
	<block score="224793.0" text_size="43">
		<component size="36" species="mouse" src="mouse.chr7" src_size="145134094" start="66076127" strand="+" text="GCTTCAGGCAG---AGGCAGCCAAC---GCTGTGGAGG-CTGG"/>
		<component size="31" species="rat" src="rat.chr1" src_size="267910886" start="120623305" strand="+" text="GCTTCAGGCAG---AGGCAGC--------CTGTGGAGG-CTGG"/>
		<component size="39" species="human" src="human.chr15" src_size="100338915" start="832323" strand="-" text="GCATCCAGCAA---GCGCGGCTGACCTTTCTCTGGGTG-CTTG"/>
		<component size="39" species="chimp" src="chimp.chr15" src_size="100063422" start="869483" strand="-" text="GCATCCAGCAA---GCGCGGCTAACCTTTCTCTGGGTG-CTTG"/>
		<component size="39" species="rhesus" src="rhesus.chr7" src_size="169801366" start="88527789" strand="-" text="GCATCCAGCAA---GTGCAGCTGACCTTTCTCTGGGTG-CTTG"/>
		<component size="32" species="cow" src="cow.chr21" src_size="49724630" start="2857509" strand="+" text="TGCTCCAGCAG---AGGCA--------TTTTGCACAGGACCTG"/>
		<component size="39" species="dog" src="dog.chr3" src_size="94715083" start="42797165" strand="+" text="TCCTCCATCAG---AAGCCGCTGATCTTTTTGCAGCTG-CCTG"/>
		<component size="32" species="armadillo" src="armadillo.scaffold_180734" src_size="4632" start="3774" strand="-" text="------GGCTG---GGACA-CAGACCCCTTTGAGGGTG-CCTG"/>
		<component size="42" species="elephant" src="elephant.scaffold_68633" src_size="8238" start="4308" strand="+" text="GCCTCATGCTGGCTGGACCACAGACCCCTCTGCAGGTG-TCTG"/>
	</block>
</maf>;

    }
}
