package mafComponents
{
    import mafComponents.*;
    import mx.controls.*;
    import mx.containers.*;
    import mx.events.*;
    import flash.events.Event;


    /**
    * Container for the diagram representation of a Block.
    **/
    public class MiniBlock extends VBox
    {
        /**
        * Has drawing happened?
        **/
        public var drawn:Boolean = false;
        private var _minitracks:Array;
        /**
        * List of minitracks (Canvas).
        **/
        public function get minitracks():Array
        {
            return _minitracks;
        }
        private function refresh_tracks(evt:ChildExistenceChangedEvent):void
        {
            _minitracks = new Array();
            for (var i:int=0; i<numChildren; i++)
            {
                var obj:Object = getChildAt(i);
                if (obj is Canvas) _minitracks.push(obj);
            }
        }
        /**
        * Add a minitrack (Canvas).
        **/
        public function addMiniTrack(newtrack:Canvas):void
        {
            newtrack.mouseFocusEnabled = false;
            addChild(newtrack);
        }
        /**
        * The sort index of the Block which this MiniBlock represents.
        **/
        public function get order():int
        {
            if (block)
            {
                return block.getIndex();
            }
            return -1;
        }
        /**
        * Same as getChildren().length
        **/
        public function get nchildren():int
        {
            var children:Array = getChildren();
            return children.length;
        }
        private var _block:Block;
        /**
        * The Block which this MiniBlock represents.
        **/
        public function get block():Block
        {
            return _block;
        }
        private var _mafgraph:MafGraph = null;
        private var _mafgraphChanged:Boolean = false;
        /**
        * A link to the parent MafGraph.
        * @see MafGraph
        **/
        public function set mafgraph(value:MafGraph):void
        {
            if (value != _mafgraph)
            {
                _mafgraph = value;
                invalidateProperties();
            }
        }
        public function get mafgraph():MafGraph
        {
            return _mafgraph;
        }
        private var newElements:Array;
        /**
        * Add a new element to the pending list.
        * @see Element
        **/
        public function addDataElement(el:Element):void
        {
            newElements.push(el);
            invalidateProperties();
        }
        ///////////// Fields gotten from the mafgraph ////////////

        /**
        * Determined by MafGraph.box_height
        * @see MafGraph#box_height
        **/
        public function get box_height():Number
        {
            if (_mafgraph) return _mafgraph.box_height;
            return -1;
        }
        /**
        * Determined by MafGraph.char_width
        * @see MafGraph#char_width
        **/
        public function get char_width():Number
        {
            if (_mafgraph) return _mafgraph.char_width;
            return -1;
        }
        /**
        * Determined by MafGraph.char_height
        * @see MafGraph#char_height
        **/
        public function get char_height():Number
        {
            if (_mafgraph) return _mafgraph.char_height;
            return -1;
        }
        /**
        * Determined by MafGraph.colorArray
        * @see MafGraph#colorArray
        **/
        public function get colorArray():Array
        {
            if (_mafgraph) return _mafgraph.colorArray;
            return null;
        }
        /**
        * Constructor.
        **/
        public function MiniBlock(srcBlock:Block)
        {
            super();
            _block = srcBlock;
            _block.miniblock = this;
            addEventListener(ChildExistenceChangedEvent.CHILD_ADD, refresh_tracks);
            horizontalScrollPolicy = "off";
            verticalScrollPolicy = "off";
            setStyle("verticalGap", 0);
            setStyle("HorizontalGap", 0);
            setStyle("backgroundAlpha", 1);
            setStyle("backgroundAlpha", 0xFF0000);
            setStyle("paddingLeft", 0);
            setStyle("paddingRight", 0);
            setStyle("borderStyle", "solid");

            newElements = new Array();
        }
        import mx.core.UIComponent;
        /**
        * @private
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            for (var obj_i:int = 0; obj_i < numChildren; obj_i++)
            {
                var obj:UIComponent = UIComponent(getChildAt(obj_i));
                //trace(name + ".updateDisplayList:" + obj_i, ":", obj.name, x, y);
            }
            
            /*graphics.clear();
            graphics.beginFill(0);
            graphics.drawRect(0,0, unscaledWidth, unscaledHeight);
            graphics.endFill();*/
        }
        /**
        * In addition to super(), calls drawElement() on any pending Elements.
        * @see #drawElement()
        * @see Element
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (drawn && (newElements.length > 0))
            {
                while (newElements.length > 0)
                {
                    var el:Element = newElements.pop();
                    drawElement(el);
                }
            }
        }
        /**
        * Creates the pixeltrack onto which the Element draws its graphical icon, and calls Element.draw().
        * @see Element#draw()
        **/
        protected function drawElement(el:Element):void
        {
            var currentBlock:Block = block;
            var currentMiniBlock:MiniBlock = currentBlock.miniblock;
            trace("Drawing", currentMiniBlock);
            //trace("******listened to" + e.type, currentMiniBlock, "for", currentBlock);
            var pixeltrack:TextArea = new TextArea();
            pixeltrack.setStyle("backgroundAlpha", 0);
            //pixeltrack.width = mafgraph.miniblocks[block_index].width;
            pixeltrack.width = currentMiniBlock.width;
            pixeltrack.height = mafgraph.char_height;
            var track_index:int = block.tracks.indexOf(el.track);
            currentMiniBlock.minitracks[track_index].addChild(pixeltrack);
            el.draw(pixeltrack, 0);
            //el.msg = text_panel;
        }
        /**
        * Draw the all Tracks in the Block.
        * <p>For each Track in the Block, create a new "minitrack" (a Canvas) and call addMiniTrack() on it, then
        * create a new "pixeltrack" (a TextArea), and call draw_segments() on it. Dispatches MiniBlockDrawn on the
        * source Block after completion.
        * </p>
        * @see #addMiniTrack()
        * @see ComponentTrack#draw_segments()
        **/
        public function draw():void
        {
            var miniblock:MiniBlock = this;
            // maybe this should be in MiniBlock
            //miniblock_settings(miniblock);
            var block:Block = miniblock.block;
            miniblock.height = box_height;
            miniblock.width = block.text_size;

            var ntracks:int = block.tracks.length;
            var yshift:Number = 0;
            var track_i:int = 0;
            for each (var track:Track in block.tracks)
            {
                var trackColor:int = colorArray[track_i % colorArray.length];
                try
                {
                    var minitrack:Canvas = new Canvas();
                    minitrack.height = char_height;
                    minitrack.width = miniblock.width;
                    //minitrack.setStyle("contentBackgroundColor", trackColor);
                    minitrack.setStyle("contentBackgroundAlpha", 0.0);
                    miniblock.addMiniTrack(minitrack);
                }
                catch (error:Error)
                {
                    Alert.show("Couldn't add minitrack " + error.toString());
                }
                var pixeltrack:TextArea = new TextArea();
                //pixeltrack.setStyle("backgroundAlpha", 1);
                //pixeltrack.setStyle("backgroundColor", 0xEEE8AA);
                pixeltrack.width = miniblock.width;
                pixeltrack.height = minitrack.height;
                pixeltrack.selectable = false;
                pixeltrack.editable = false;
                pixeltrack.text = '';
                //try
                {
                    /*if (block_i == 0)
                    {
                        Alert.show("track " + track_i + ": yshift: " + yshift);
                    }*/
                    minitrack.addChild(pixeltrack);
                    var darkBlue:int = 0x000033;
                    var lightBlue:int = 0x8888a5;
                    track.draw_segments(pixeltrack, 0, darkBlue, lightBlue);
                }
                /*catch(error:Error)
                {
                    Alert.show("couldn't add pixeltrack:TextArea, " + error.toString());
                }*/

                /*if (track is ComponentTrack)
                {
                    var ct:ComponentTrack = ComponentTrack(track);
                    for each(var element:Element in ct.elements)
                    {
                        element.draw(pixeltrack, 0);
                    }
                }*/
                yshift += char_height;
                track_i += 1;
            }
            miniblock.drawn = true;
            block.dispatchEvent(new Event("MiniBlockDrawn"));
            trace("~" + name + "-->" + block.name + ".MiniBlockDrawn ->", miniblock);
        }
    }
}

