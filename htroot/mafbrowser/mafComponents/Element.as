package mafComponents
{
    import mx.controls.TextArea; // for error message
    import flash.display.*;
    import flash.net.SharedObject;
    import spark.components.Button;


    /**
    * The display class for annotated alignment text.
    *
    * <p>
    * An Element is created by calling <code>ComponentTrack.addTrackElement(...)</code>, which is done by the event handlers that process the XML
    * input of a dataset, in <code>MafConsole.datasethandle_obj()</code> and its calling chain that comes from parsing the browser line.
    * </p>
    * @see ComponentTrack#addTrackElement()
    * @see MafConsole#datasethandle_obj()
    **/
    public class Element extends Button
    {
        /**
        * The Element's owning ComponentTrack
        **/
        public var track:ComponentTrack;
        public var _msg:TextArea;
        /**
        * For debugging messages
        **/
        public var msg:TextArea;

        private var _element_class:String;
        /**
        * The left-most alignment column of the annotation
        **/
        public var start_col:int;
        /**
        * The right-most alignment column of the annotation
        **/
        public var end_col:int;
        /**
        * A link to the corresponding drawing surface in the mini graph.
        * @see #draw()
        * @see MafGraph
        **/
        public var pixeltrack:Sprite;
        /**
        * The annotation fill color.
        **/
        public var highlightColor:Number;


        private var _borderColor:uint;
        [Bindable]
        public function set borderColor(newBC:uint):void
        {
            if (_borderColor != newBC)
            {
                _borderColor = newBC;
                invalidateDisplayList();
            }
        }
        public function get borderColor():uint { return _borderColor; }

        private var _yshift:int;
        /**
        * The group to which this annotation belongs.
        **/
        public function get element_class():String
        {
            return _element_class;
        }
        public function set element_class(v:String):void
        {
            _element_class = v;
        }

        /**
        * Apparently unused.
        **/
        public var track_start:int = 0;
        /**
        * Apparently unused.
        **/
        public var track_end:int = 0;

        /**
        * Constructor. Called with alignment coordinates, not sequence coordinates.
        **/
        public function Element(_alt_x:Number=0,  _alt_width:Number=5, _alt_height:Number=5, _element_class:String="", ts:int=0, te:int=0)
        {
            super();
            track_start = ts;
            track_end = te;
            x = _alt_x;
            y = 0;
            width = _alt_width;
            height = _alt_height;
            element_class = _element_class;
            highlightColor = 0xFFFF00;
            set_color(highlightColor);
            set_alpha(1);
            borderColor = 0x000000;
        }
        /**
        * Set the highlight color.
        **/
        public function set_color(newcolor:int):void
        {
            highlightColor = newcolor;
            //setStyle("chromeColor", highlightColor);
            setStyle("contentBackgroundColor", highlightColor);
            invalidateDisplayList();
        }
        public function set_alpha(alpha:uint):void
        {

            setStyle("contentBackgroundAlpha", alpha);
            invalidateDisplayList();
        }
        /**
        * Write the specified style to the msg debugging TextArea.
        * @see #msg
        **/
        public function showStyle(s:String):void
        {
            msg.text += s + ": " + getStyle(s) + "\n";
        }
        /**
        * Drawing commands for this Element's diagram on the mini graph.
        * <p>
        * The command takes a <code>canvas:Sprite</code> and sets it to <code>pixeltrack</code>, 
        * and <code>yshift</code> specified externally as the row in the alignment. 
        * This function is called by <code>redraw()</code>.
        * </p>
        * <p>
        * The alignment text underneath the annotation is split using a regular expression <code>/(-+)/</code>, 
        * calling <code>draw_nongap_segment()</code> on the pieces of text with sequence character, 
        * and <code>draw_gap_segment()</code> on the stretches of gaps (-).
        * </p>
        * @see #pixeltrack
        * @see #redraw()
        * @see #draw_gap_segment()
        * @see #draw_nongap_segment()
        * @see MafGraph
        **/
        public function draw(canvas:Sprite, yshift:Number):void
        {
            var str:String = track.text.substr(start_col, end_col-start_col);
            var re:RegExp = /(-+)/;
            var segments:Array = str.split(re);
            var xloc:int = start_col;
            for (var i:int=0; i<segments.length; i++)
            {
                var segment:String = segments[i];
                if (segment.length > 0)
                {
                    if (segment.charAt(0) == '-')
                        draw_gap_segment(xloc, yshift, segment.length, canvas);
                    else
                        draw_nongap_segment(xloc, yshift, segment.length, canvas);
                }
                xloc += segment.length;
            }
            pixeltrack = canvas;
            _yshift = yshift;
        }
        /**
        * Clears the pixeltrack and calls draw().
        * @see #draw()
        **/
        public function redraw():void
        {
            pixeltrack.graphics.clear();
            draw(pixeltrack, _yshift);
        }
        import flash.geom.Matrix;
        /**
        * Called by draw on gap segments. Uses highlightColor.
        **/
        public function draw_gap_segment(x:int, y:int, len:int, slot:Sprite):void
        {
            var grey:int = 0x888888;
            var white:int = 0xFFFFFF;
            var red:int = 0xFF0000;
            var black:int = 0;
            var doFill:Boolean = (getStyle("contentBackgroundAlpha") as uint) == 1;
            if (doFill)
            {
                var mx:Matrix = new Matrix();
                mx.createGradientBox(len, 6, Math.PI/2, 0, y+2)
                slot.graphics.beginGradientFill(GradientType.LINEAR, [highlightColor, black], [1,1], [0, 0xFF], mx);
            }
            slot.graphics.drawRoundRect( x, y+2, len, 6, 0, 0 );
            slot.graphics.endFill();
        }
        /**
        * Called by draw on non-gap segments. Uses highlightColor.
        **/
        public function draw_nongap_segment(x:int, y:int, len:int, slot:Sprite):void
        {
            var grey:int = 0x888888;
            var white:int = 0xFFFFFF;
            var red:int = 0xFF0000;
            var black:int = 0;
            var doFill:Boolean = (getStyle("contentBackgroundAlpha") as uint) == 1;
            trace(name, "contentBackgroundAlpha", getStyle("contentBackgroundAlpha"), "doFill", doFill);
            if (doFill)
            {
                var mx:Matrix = new Matrix();
                mx.createGradientBox(len, 10, Math.PI/2, 0, y)
                slot.graphics.beginGradientFill(GradientType.LINEAR, [highlightColor, black], [1,1], [0, 0xFF], mx);
            }
            slot.graphics.lineStyle(1.0, borderColor);
            slot.graphics.drawRoundRect( x, y, len, 10, 0, 0 );
            slot.graphics.endFill();
        }
        override public function styleChanged(styleProp:String):void 
        {

            trace(name, "styleChanged", styleProp);
            super.styleChanged(styleProp);

            // Check to see if style changed. 
            if (styleProp=="contentBackgroundAlpha" || styleProp=="contentBackgroundColor") 
            {
                //bStypePropChanged=true; 
                redraw();
                return;
            }
        }
    }
}

