package mafComponents.utils
{
    import flash.geom.*;
    public class geomUtils
    {
        public static function minXBetweenPoints(point1:Point, point2:Point):Number { return Math.min(point1.x,point2.x); }
        public static function minYBetweenPoints(point1:Point, point2:Point):Number { return Math.min(point1.y,point2.y); }
        public static function maxXBetweenPoints(point1:Point, point2:Point):Number { return Math.max(point1.x,point2.x); }
        public static function maxYBetweenPoints(point1:Point, point2:Point):Number { return Math.max(point1.y,point2.y); }
        public static function spanningRectBetweenPoints(p1:Point, p2:Point):Rectangle
        {
            var diff:Point = p1.subtract(p2);
            return new Rectangle(minXBetweenPoints(p1,p2), minYBetweenPoints(p1,p2), Math.abs(diff.x), Math.abs(diff.y));
        }
    }
}
