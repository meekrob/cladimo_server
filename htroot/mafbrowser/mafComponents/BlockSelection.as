/* BlockSelection - a floating TextArea control that resides in the focused-on Block.
    BlockSelection responds to mouse input on the MAF blocks, and emulates a selection grid
    when the user sweeps along a block.
*/
package mafComponents
{
    import flash.geom.*;
    import mx.events.ScrollEvent;
    import flash.display.*;
    import flash.text.*;
    import mx.core.EdgeMetrics;
    import mx.controls.Alert;
    import mx.controls.Text;
    import mx.controls.TextArea;
    import mx.containers.*;
    import mx.rpc.events.ResultEvent;
    import flash.events.*;
    import flash.ui.Keyboard;
    import mx.utils.*;
    import mafComponents.*;

    [Event(name="selectionChange", type="flash.events.Event")]

    public class BlockSelection extends TextArea implements IBlockSelection
    {
        private var key_to_registered:Boolean;
        private var _viewer:MafViewer;

        private var textChanged:Boolean = false;
        private var _nrows:int = 0;
        private var _ncols:int = 0;
        public function get nrows():int { return _nrows; }
        public function get ncols():int { return _ncols; }

        public var selectedBlock:Block;
        public var selected_track_index:int;
        public var selected_char_index:int;
        public var shift_key_pressed:Boolean;
        public var mouse_dragging:Boolean;
        public var slicedBlock:Block;
        public var msg:TextArea;

        public var block_row:int;
        public var block_col:int;

        // point_selected point
        private var _alt_x:Number = -1;
        private var _alt_y:Number = -1;
        private var _alt_width:Number;
        private var _alt_height:Number;

        // just pass the private values through so it meets the IBlockSelection interface
        public function set alt_x(value:Number):void { _alt_x = value; }
        public function set alt_y(value:Number):void { _alt_y = value; }

        public function get alt_x():Number { return _alt_x; }
        public function get alt_y():Number { return _alt_y; }
    
        public function set alt_width(value:Number):void { _alt_width = value; }
        public function set alt_height(value:Number):void { _alt_height = value; }

        public function get alt_width():Number { return _alt_width; }
        public function get alt_height():Number { return _alt_height; }



        public var pivot_pnt_x:int;
        public var pivot_pnt_y:int;

        public function BlockSelection()
        {
            super();
            setStyle("leading",1);
            setStyle("borderStyle", "none");
            setStyle("fontFamily", "Courier");
            setStyle("fontSize", "12");
            setStyle("focusThickness", 0);
            setStyle("paddingLeft",0);
            setStyle("paddingRight",0);
            setStyle("paddingTop",0);
            setStyle("paddingBottom",0);
            var darkBlue:uint = 0x000033;
            setStyle("contentBackgroundColor", darkBlue);
            setStyle("backgroundColor", darkBlue);
            setStyle("contentBackgroundAlpha", 0.5);
            editable = false;
            wordWrap = false;
            horizontalScrollPolicy = "off";
            verticalScrollPolicy = "off";
            key_to_registered = false;
            slicedBlock = null;
            includeInLayout = false;
        }
        public function get viewer():MafViewer
        {
            if (! _viewer) _viewer = get_viewer();
            return _viewer;
        }
        public function get_viewer():MafViewer
        {
            var obj:Object = parent;
            while (!(obj is MafViewer))
            {
                obj = obj.parent;
            }
            return obj as MafViewer;
        }
        public function get bounds():Array
        {
            if (slicedBlock)
                return slicedBlock.bounds;
            else
                return selectedBlock.bounds;
        }
        public function watch(block:Block):void
        {
            block.addEventListener(MouseEvent.CLICK, click);
            block.addEventListener(MouseEvent.MOUSE_MOVE, mouse_move);
            block.addEventListener(MouseEvent.MOUSE_DOWN, mouse_down);
            block.addEventListener(MouseEvent.MOUSE_UP, mouse_up);
        }
        public function keyTo(evt:KeyboardEvent):void
        {
            if (focusManager.getFocus() != this) return;

            var x_adjust:int = 0;
            var y_adjust:int = 0;

            if(evt.keyCode == Keyboard.LEFT) x_adjust = -1;
            else if(evt.keyCode == Keyboard.BACKSPACE) x_adjust = -1;
            else if(evt.keyCode == Keyboard.RIGHT) x_adjust = +1;
            else if(evt.keyCode == Keyboard.SPACE) x_adjust = +1;
            else if(evt.keyCode == Keyboard.UP) y_adjust = -1;
            else if(evt.keyCode == Keyboard.DOWN) y_adjust = +1;
            else if(evt.keyCode == Keyboard.ENTER) y_adjust = +1;
            else return;

            var current_block:Block = selectedBlock;
            if ((x_adjust != 0) || (y_adjust != 0))
            {
                setStyle("backgroundAlpha", .5);
                text = "";
                // equivalent to set_point_selection()
                selected_char_index = Math.min( Math.max(0, selected_char_index + x_adjust), current_block.text_size - 1);
                selected_track_index = Math.min( Math.max(0, selected_track_index + y_adjust), current_block.ntracks - 1);
                slicedBlock = selectedBlock.slice(selected_char_index, selected_track_index, 1, 1);
                alt_x = selected_char_index*charWidth;
                alt_y = selected_track_index*trackHeight;
                alt_width = charWidth;
                alt_height = trackHeight;
                move(selected_char_index*charWidth, selected_track_index*trackHeight);
                setActualSize(charWidth, trackHeight);
                dispatchEvent(new Event("selectionChange"));
            }
        }
        public function set_pivot_point(evt:MouseEvent):void
        {
            var currentCoordRect:Rectangle = Block(evt.currentTarget).coordRect_under_mouse();
            pivot_pnt_x = currentCoordRect.x;
            pivot_pnt_y = currentCoordRect.y;
        }
        import mx.core.UIComponent;
        import mx.core.IUIComponent;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            trace(name + '.updateDisplayList(' + unscaledWidth + ', ' + unscaledHeight + ')');
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            var vm:EdgeMetrics = borderMetrics;
            trace('... borderMetrics: vm.left', vm.left, 'right', vm.right, 'top', vm.top, 'bottom', vm.bottom);
            for (var obj_i:int = 0; obj_i < numChildren; obj_i++)
            {
                var obj:DisplayObject = getChildAt(obj_i) as DisplayObject;
                if (obj is IUIComponent)
                {
                    var UIobj:IUIComponent = obj as IUIComponent;
                    var obj_width:Number = UIobj.getExplicitOrMeasuredWidth();
                    var obj_height:Number = UIobj.getExplicitOrMeasuredHeight();
                    trace('...UIComponent:' + obj.name + ':' + 'x', obj.x, 'y', obj.y, 'width', obj_width, 'height', obj_height);
                }
                else
                {
                    trace('...< DisplayObject:' + obj.name + ':' + 'x', obj.x, 'y', obj.y, 'width', obj.width, 'height', obj.height);
                    obj.x = 0;
                    obj.y = 0;
                    //if (obj.width != unscaledWidth) obj.width = unscaledWidth;
                    //if (obj.height != unscaledHeight) obj.height = unscaledHeight;
                    trace('...> DisplayObject:' + obj.name + ':' + 'x', obj.x, 'y', obj.y, 'width', obj.width, 'height', obj.height);
                }

                
            }
        }
        public function reset(block:Block):void
        {
            //if (!block) return; 
            visible = false;

            if (! selectedBlock)
                selectedBlock = block;

            if (block!=selectedBlock) 
            {
                if (selectedBlock && selectedBlock.contains(this))
                {
                    selectedBlock.removeChild(this);
                }
                selectedBlock = block;
                move(0,0);
                setActualSize(charWidth, trackHeight);
                selectedBlock.addChild(this);
                dispatchEvent(new Event("selectionChange"));
                if (! key_to_registered) 
                {
                    stage.addEventListener(KeyboardEvent.KEY_DOWN, keyTo);
                    key_to_registered = true;
                }
                
            }
            selected_char_index = 0;
            selected_track_index = 0;
            trace("BlockSelection: selectedBlock", selectedBlock);
            alt_x = selected_char_index*charWidth;
            alt_y = selected_track_index*trackHeight;
            alt_width = charWidth;
            alt_height = trackHeight;
            move(selected_char_index*charWidth, selected_track_index*trackHeight);
            setActualSize(charWidth, trackHeight);
            if (slicedBlock) slicedBlock = null;
            setStyle("backgroundAlpha", .5);
            text = "";
        }
        public function click(evt:MouseEvent):void
        {
            trace(name + '.click(evt:MouseEvent).' + evt.type);
            var current_block:Block = evt.currentTarget as Block;
            reset(current_block);
            set_point_selection(evt);
        }
        private function set_point_selection(evt:MouseEvent, relocate:Boolean=true):void
        {
            trace(name + ".set_point_selection:");
            var current_block:Block = evt.currentTarget as Block;
            var current_block_position:Point = new Point(current_block.mouseX, current_block.mouseY);
            //var current_coord:Object = coord_under_point(current_block_position, current_block);
            var current_coord:Object = current_block.coord_under_mouse();

            selected_char_index = current_coord.x;
            trace("             selected_char_index", selected_char_index);
            selected_track_index = current_coord.y;
            trace("             selected_track_index", selected_track_index);
            alt_x = selected_char_index*current_block.char_width;
            trace("             alt_x", alt_x);
            alt_y = selected_track_index*current_block.track_height;
            trace("             alt_y", alt_y);
            alt_width = current_block.char_width;
            trace("             alt_width", alt_width);
            alt_height = current_block.track_height;
            trace("             alt_height", alt_height);
            if (relocate)
            {
                var new_x:Number = selected_char_index*current_block.char_width;
                var new_y:Number = selected_track_index*current_block.track_height;
                move(new_x, new_y);
                trace("             move(new_x, new_y)", new_x, new_y);
                setActualSize(current_block.char_width, current_block.track_height);
                trace("             setActualSize(current_block.char_width, current_block.track_height)", current_block.char_width, current_block.track_height);
            }
        }
        /* event listeners for the active Block */
        public function mouse_down(evt:MouseEvent):void
        {
            trace(name + '.mouse_down(evt:MouseEvent).' + evt.type);
            click(evt);
            mouse_dragging = true;
            text = "";
            pivot_pnt_x = alt_x;
            pivot_pnt_y = alt_y;
            invalidateProperties();
        }
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (textChanged)
            {
                trace(name + 'commitPropertes(): text:\n' + text);
                textChanged = false;
                dispatchEvent(new Event("selectionChange"));
                if (text)
                {
                    setStyle("contentBackgroundColor", 0x880000);
                    setStyle("contentBackgroundAlpha", .5);
                }
                else
                {
                    setStyle("contentBackgroundColor", 0x000033);
                    setStyle("contentBackgroundAlpha", 0.5);
                }
                invalidateSize();
                invalidateDisplayList();
            }
            if (mouse_dragging)
            {
                setStyle("contentBackgroundColor", 0x000033);
                setStyle("contentBackgroundAlpha", 0.5);
            }
            if (text || mouse_dragging)
                visible = true;

        }
        override protected function measure():void
        {
            super.measure();
            if (selectedBlock && charWidth && trackHeight && _ncols && _nrows)
            {
                trace(name + '.measure()', selectedBlock.name, charWidth , trackHeight , _ncols , _nrows)
                measuredWidth = charWidth * ncols + 2;
                measuredMinWidth = measuredWidth;
                measuredHeight = trackHeight * nrows + 2;
                measuredMinHeight = measuredHeight;
                trace('...' + name + '.measure()', measuredWidth, measuredHeight);
            }
        }

        public function get charWidth():int
        {
            return selectedBlock.char_width;
        }
        public function get trackHeight():int
        {
            return selectedBlock.track_height;
        }

        public function mouse_up(evt:MouseEvent):void
        {
            trace(name + '.mouse_down(evt:MouseEvent).' + evt.type);
            mouse_dragging = false;
            var selected:Rectangle = selection_rect();
            var selection_row:int = selected.y / trackHeight;
            var selection_col:int = selected.x / charWidth;
            _nrows = selected.height / trackHeight;
            _ncols = selected.width / charWidth;


            verticalScrollPosition = 0;

            var blockSelectedTracks:Array = selectedBlock.tracks;
            var newText:String = '';
            for (var row_i:int = selection_row ; row_i < (selection_row+nrows); row_i++)
            {
                var slicedText:String = blockSelectedTracks[row_i].slice_text(selection_col, ncols);
                trace(blockSelectedTracks[row_i].name + '[' + row_i + ']' + '[' + selection_col + '+' + ncols + ']:' + slicedText);
                newText += slicedText;
                if (row_i < ((selection_row+nrows)-1))
                {
                    newText += '\r';
                }
            }

            text = newText;
            textChanged = true;
            invalidateProperties();
            selectionBeginIndex=0;
            selectionEndIndex = text.length;
            setStyle("backgroundAlpha", 1);
            setFocus();
        }
        public function mouse_move(evt:MouseEvent):void
        {
            if ( mouse_dragging)
            {
                set_point_selection(evt, false);
                var selected:Rectangle = selection_rect();
                move(selected.x, selected.y)
                setActualSize(selected.width, selected.height);
            }
        }
        public function selection_rect():Rectangle
        {
            return spanning_rect(pivot_pnt_x, pivot_pnt_y, alt_x, alt_y);
        }
        public function spanning_rect(a_x:int, a_y:int, b_x:int, b_y:int):Rectangle
        {
            var upper_left_x:int = Math.min(a_x, b_x);
            var upper_left_y:int = Math.min(a_y,b_y);
            var spanning_width:int = Math.max(Math.abs(a_x - b_x) + charWidth, charWidth);
            var spanning_height:int = Math.max(Math.abs(a_y - b_y) + trackHeight, trackHeight);
            return new Rectangle(upper_left_x, upper_left_y, spanning_width, spanning_height);
        }
    }
}
