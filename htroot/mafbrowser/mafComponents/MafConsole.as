/* MafConsole:VBox
* Implements the class MafXML with public function MafXMLHandler()
*/
package mafComponents
{
    import mx.events.ScrollEvent;
    import flash.display.*;
    import flash.text.*;
    import mx.core.EdgeMetrics;
    import mx.controls.*;
    import mx.containers.*;
    import mx.events.*;
    import mx.rpc.events.ResultEvent;
    import flash.events.*;
    import mx.rpc.http.HTTPService;
    import mx.utils.*;
    import mafComponents.Track;
    import mafComponents.Block;
    import mx.managers.*;
    import mafComponents.MafViewer;
    import mafComponents.MafGraph;
    import mafComponents.events.*;

    /**
    * Dispatched after the MAF is determined to be finished loading.
    **/
    [Event(name="MAFLoaded", type="flash.events.Event")]
    /**
    * Dispatched after config.xml has loaded.
    **/
    [Event(name="ConfigLoaded", type="flash.events.Event")]

    /**
    * The main container for the MafBrowser
    **/
    public class MafConsole extends DividedBox
    {
        /**
        * The diagram view of the MAF, in the lower window.
        **/
        public var mafgraph:MafGraph;
        /**
        * The scrollable text view of the MAF, in the upper window.
        **/
        public var mafviewer:MafViewer;
        /**
        * Some text panel?
        **/
        public var text_panel:TextArea;
        /**
        * A reference to the button_panel;
        * @see ButtonPanel
        **/
        public var button_panel:ButtonPanel;
        /**
        * The file upload button.
        **/
        public var upload_button:Button;
        /**
        * Shows the genomic location of the cursor or selection.
        * @see PositionWindow
        **/
        public var positionwindow:PositionWindow;
        /**
        * Launches the positionwindow.
        * @see #positionwindow.
        **/
        public var position_window_button:Button;
        /**
        * Instance of a dataset upload class.
        * @see MafUploader
        **/
        public var data_uploader:MafUploader;
        /**
        * Instance of a MAF upload class.
        * @see MafUploader
        **/
        public var maf_uploader:MafUploader;
        /**
        * Datasets pending, processed by <code>commitProperties()</code>.
        * @see #commitProperties()
        **/
        public var datasetsToLoad:Array;
        /**
        * Loaded datasets.
        **/
        public var datasets:Array;
        /**
        * The reference species in the alignment.
        **/
        public var reference_species:String;
        /**
        * List of species.
        **/
        public var species_list:Array;
        /**
        * Primary container in the upper window.
        **/
        public var panel:Panel;
        /**
        * Annotations not loaded yet...
        **/
        public var pending_annots:Array;
        /**
        * Loaded from an external XML file.
        **/
        public var color_settings:Object;

        private var _locale:Object; // loaded from config.xml by console.mxml
        /**
        * Is config.xml loaded?
        **/
        public function get config_is_loaded():Boolean { return (_locale != null); }

        /**
        * Constructor.
        **/
        public function MafConsole()
        {
            super();
            horizontalScrollPolicy = "off";
            direction = "vertical";
            datasets = new Array();
            datasetsToLoad = new Array();
            data_uploader = new MafUploader(null, null, "data", this);
            addEventListener("MAFLoaded", afterMAFLoaded);
        }

        /**
        * In addition to <code>super()</code>, runs <code>load_datasets()</code> on <code>datasetsToLoad</code> 
        * if the alignment is finished rendering.
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (_maf_is_loaded && (datasetsToLoad.length > 0))
                load_datasets();
        }

        /**
        * Settings in config.xml.
        **/
        public function get locale():Object {return _locale;}
        /**
        * Set locale settings, usually read from config.xml.
        **/
        public function set locale(obj:Object):void
        {
            _locale = obj;
            dispatchEvent(new Event("ConfigLoaded"));
        }

        private var _maf_is_loaded:Boolean = false;
        /**
        * Flag indicating the render state of the alignment.
        **/
        public function get maf_is_loaded():Boolean { return _maf_is_loaded; }
        /**
        * Set indicator that the alignment is loaded and launch the event "MAFLoaded".
        **/
        protected function signal_MAFLoaded():void
        {
            trace("____MAFLOADED____");
            _maf_is_loaded = true;
            dispatchEvent(new Event("MAFLoaded"));
        }
        private function afterMAFLoaded(result:Event):void
        {
            trace(name + ".afterMAFLoaded(): " + result.type);
            if (datasetsToLoad.length > 0)
            {
                invalidateProperties();
            }
        }
        /**
        * Color specifications gotton from the browser URL
        * @see color_settings
        * @see get_color_settings
        **/
        public function load_xml_colors_from_url(event:ResultEvent):void
        {
            color_settings = new Object();
            var settings:Array = event.result.color_settings.type.toArray();
            for each (var item:Object in settings)
            {
                color_settings[item.name] = parseInt(item.color, 16);
            }
        }
        /**
        * Loads '/cladimo/color-settings.xml'. CURRENTLY COMMENTED OUT.
        * @see color_settings
        **/
        public function get_color_settings():void
        {
            /*var service:HTTPService = new HTTPService();
            service.url = "/cladimo/color-settings.xml";
            service.addEventListener("result", load_xml_colors_from_url);
            service.send();*/
        }
        private var blocksLoaded:int = 0;
        /**
        * In addition to super(), creates <code>positionwindow</code>, <code>position_window_button</code>, <code>upload_button</code>,
        * <code>mafgraph</code>, <code>mafviewer</code>
        * @see #positionwindow
        * @see #position_window_button
        * @see #upload_button
        * @see #mafgraph
        * @see #mafviewer
        **/
        override protected function createChildren():void
        {
            super.createChildren();
            if (! positionwindow)
            {
                positionwindow = new PositionWindow();
                positionwindow.name = "PositionWindow";
            }
            if (! position_window_button)
            {
                position_window_button = new LinkButton();
                position_window_button.name = "PositionWindowButton";
                position_window_button.label = "Position";
                position_window_button.setStyle("borderColor", 0xFFFFFF);
                position_window_button.toolTip = "click for details";
                position_window_button.addEventListener(MouseEvent.CLICK, positionwindow.popup);
                positionwindow.buttonref = position_window_button;
            }
            if (! upload_button)
            {
                upload_button = new Button();
                upload_button.name = "DataSetUploadButton";
                upload_button.label = "+";
                upload_button.setStyle("borderColor", 0xFFFFFF);
                upload_button.addEventListener(MouseEvent.CLICK, browse);
            }
            if (! mafgraph)
            {
                mafgraph = new MafGraph();
                mafgraph.name = "MafGraph";
                addEventListener(BrowserScrollEvent.DEFAULT_NAME, mafgraph.processBrowserScroll);
                addChild(mafgraph);
            }
            if (! mafviewer)
            {
                mafviewer = new MafViewer();
                mafviewer.console = this;
                mafviewer.addEventListener("creationComplete", function():void {
                    button_panel = mafviewer.button_panel;
                    button_panel.addChild(upload_button);
                    button_panel.addChildAt(position_window_button,0);
                    mafviewer.scroller.selection.addEventListener("selectionChange", positionwindow.update);
                    mafviewer.scroller.addEventListener(BlockChangedEvent.DEFAULT_NAME, 
                        function(event:BlockChangedEvent):void
                        {
                            blocksLoaded++;
                            trace(name + ".listenedTo(" + event.type + ")", event.block);
                            trace("      blocksLoaded", blocksLoaded, "out of", blocksToLoad);
                            if (blocksToLoad && (blocksLoaded == blocksToLoad))
                            {
                                // signal that the MAFViewer can receive annotations now 
                                // because the objects are loaded (maybe)
                                signal_MAFLoaded();
                            }
                        }
                    );
                    mafviewer.mafgraph = mafgraph;
                });
                addChildAt(mafviewer,0);
            }

        }
        /**
        * Launches a file browsing window, using data_uploader.
        * @see #data_uploader
        **/
        public function browse(event:MouseEvent):void
        {
            data_uploader.browse(datasethandler);
        }
        /**
        * Calls <code>datasethandle_obj()</code> on <code>event.result</code>
        * @see #datasethandle_obj()
        **/
        public function datasethandler(event:ResultEvent):void
        {
            var dataobj:Object = event.result;
            addEventListener("MAFLoaded", function():void
            {
                datasethandle_obj(dataobj);
            });
        }
        /**
        * Add a dataset to <code>datasetsToLoad</code>.
        * @see #datasetsToLoad
        **/
        [Deprecated]
        public function load_dataset(dataxml:Object):void
        {
            datasetsToLoad.push(dataxml);
            invalidateProperties();
        }
        private function load_datasets():void
        {
            while (datasetsToLoad.length > 0)
            {
                var dataxml:Object = datasetsToLoad.pop();
                datasethandle_obj(dataxml);
            }
        }
        /**
        * Process an XML dataset.
        **/
        public function datasethandle_obj(dataxml:Object):void
        {
            // load an XML dataset
            var dataset:DataSet = new DataSet(this);
            dataset.name = 'DataSet_' + dataxml.@name;
            try
            {
                dataset.handle_obj(dataxml);
            }
            catch(error:Error)
            {
                Alert.show("could not process file " + error);
                return;
            }
            var added:int = 0;
            // distribute elements of the dataset
            for each (var block:Block in mafviewer.scroller.blocks)
            {
                if (block is SpacerBlock) continue;
                var block_index:int = mafviewer.scroller.blocks.indexOf(block);
                //trace("MafConsole.datasethandle_obj.block[", block_index + ":" + block.name, "]");
                for (var i:int = 0; i < dataset.intervals.length; i++)
                {
                    var interval:Object = dataset.intervals[i];
                    var track:ComponentTrack = block.get_track_for_interval(interval);
                    //trace("    MafConsole.datasethandle_obj.block[", block_index, "].get_track_for_interval[", interval, "]:", track);

                    if (track)
                    {
                        var track_start:int;
                        var track_end:int;
                        var interval_start:int = Number(interval.@start) - 1;
                        var interval_end:int = Number(interval.@end) - 1;
                        if (track.strand == "+")
                        {
                            track_start = track.seq_to_align_position(interval_start);
                            track_end = track.seq_to_align_position(interval_end-1) + 1; // must go inclusive here
                        }
                        else
                        {
                            track_start = track.seq_to_align_position(interval_end);
                            track_end = track.seq_to_align_position(interval_start+1) + 1; // must go inclusive here, add one for GFF-based intervals
                        }
                            
                        var track_index:int = block.tracks.indexOf(track);

                        if (track_start == -1) track_start = 0;
                        if (track_end <= 0) track_end = track.textSize;
                        var track_span:int = track_end - track_start;

                        var el:Element = track.addTrackElement(track_start, track_end, block.tracks.indexOf(track));
                        dataset.addDataElement(el, interval.@name);
                        block.miniblock.addDataElement(el); // will draw when miniblock is rendered
                        added++;
                    }
                }
            }
            datasets.push(dataset);
            button_panel.add(dataset);
            //Alert.show("added " + added + " regions");
            //button_panel.height = button_panel.buttHeight * datasets.length;
            //Alert.show(StringUtil.substitute("bp.height {0} bp.buttHeight {1} datasets.length {2}", button_panel.height, button_panel.buttHeight,datasets.length));
        }
        /**
        * Some display colors. UNUSED.
        **/
        public function colors():Array
        {
            return [0xffff33, 0xFF0000, 0x00FF00, 0x0000FF];
        }
        /**
        * Calls <code>mafxmlhandle_object()</code> on <code>event.result.</code>
        * @see #mafxmlhandle_object()
        **/
        public function mafxmlhandler(event:ResultEvent):void
        {
            var maf:Object = event.result;
            mafxmlhandle_object(maf);
        }
        private var blocksToLoad:int = 0;
        /**
        * Process an Object representing the alignment.
        * @see MafXML
        **/
        public function mafxmlhandle_object(maf:Object):void
        {
            // load an XML maf
            if (maf_uploader.title)
            {
                mafgraph.title = maf_uploader.title;
                mafviewer.title = maf_uploader.title;
            }
            else
                mafgraph.title = maf_uploader.filename;
            var xmlmaf:MafXML = new MafXML(this, mafviewer);
            xmlmaf.msg = text_panel;
            //xmlmaf.handle(event);
            xmlmaf.handleObject(maf);
            blocksToLoad = xmlmaf.nblocks;
            trace("MafConsole.mafxmlhandle_object(), blocksToLoad", blocksToLoad);

            // transfer data
            reference_species = xmlmaf.species_list_str[0];
            species_list = xmlmaf.species_list_str;
            //mafgraph.set_panel( ["species:"].concat( xmlmaf.species_list_str ) );
            trace(name + ".mafgraph.set_panel(", xmlmaf.species_list_str, ")");
            mafgraph.set_panel( xmlmaf.species_list_str );
            //mafviewer.src_panel.addTrack("species:");
            mafviewer.src_panel.addTracks(xmlmaf.species_list_str);
            //trace("MafConsole.mafxmlhandle_object():"); 
            //trace("    mafviewer.src_panel.addTracks(xmlmaf.species_list_str) ... src_panel.ntracks", mafviewer.src_panel.ntracks);
            mafviewer.src_panel.draggable_tracks = false;
            // actual maf text
            //mafviewer.scroller.loadBlocks(xmlmaf.blocks);
            mafviewer.scroller.setWindow(text_panel);
            mafviewer.scroller.reference_species = reference_species;

            //mafgraph.draw(mafviewer.scroller.blocks); // changed this to trigger per block, via the MafViewer.commitProperties that adds pending blocks
            mafgraph.msg = text_panel;

            //mafviewer.scroller.addEventListener(ScrollEvent.SCROLL, mafgraph.scroller);
            visible = true;
            if (direction=="vertical")
            {
                mafviewer.percentHeight = 45;
                mafgraph.percentHeight = 45;
            }
            else
            {
                mafviewer.percentWidth = 45;
                mafgraph.percentWidth = 45;
            }
            //load_cmd(null);
            CursorManager.removeBusyCursor();
        }
        /**
        * Loads/processes the browser line.
        * @see parentApplication#parameters
        * @see BrowserManager
        * @see MafUploader#process_filename()
        **/
        public function load_cmd(evt:FlexEvent):void
        {
            var annot:String = parentApplication.parameters.annotations;
            var annot_files:Array;
            var tag:String;
            if (annot)
            {
                annot_files = annot.split(",");
                for each(tag in annot_files)
                {
                    data_uploader.process_filename(tag, datasethandler);
                }
            }
            else
            {
                var bm:IBrowserManager = BrowserManager.getInstance();
                bm.init("", "Welcome!");
                var params:Object = URLUtil.stringToObject(bm.fragment, "&");

                annot_files = params.annotations.split(",");

                for each(tag in annot_files)
                {
                    data_uploader.process_filename(tag, datasethandler);
                }
            }
        }
        /**
        * @private
        **/
        override protected function measure():void
        {
            super.measure();
            /*
            measuredMinHeight = 200;
            var total_height:int = 0;
            if (mafgraph) total_height += mafgraph.box_height+20;
            if (mafviewer) total_height += mafviewer.block_height + 40;
            if (text_panel) total_height += text_panel.height;
            
            measuredWidth = total_height;
            */
        }
        /**
        * @private
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);

            if (direction =="vertical")
            {
                if (mafgraph) mafgraph.width = unscaledWidth - 1;
                if (mafviewer) mafviewer.width = unscaledWidth - 1;
                if (text_panel) text_panel.width = unscaledWidth - 1;
                if (button_panel) button_panel.width = unscaledWidth - 1;
            }
            if (mafgraph.selectbox) mafgraph.selectWidth = mafviewer.scroller.text_scroll_width;
        }
    }
}

