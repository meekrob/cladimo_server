package mafComponents
{
    import mx.rpc.events.ResultEvent;
    import flash.net.SharedObject;
    import flash.events.*;
    import flash.geom.*;
    import mx.events.*;
    import mx.managers.*;
    import mx.containers.*;
    import mx.controls.*;
    import mx.utils.*;
    import mx.collections.ArrayCollection;

    /**
    * Displays genome locations as specified by the IBlockSelection component.
    **/
    public class PositionWindow extends TitleWindow
    {
        /**
         * The text component that displays the information.
        **/
        protected var info:TextArea;
        /**
         * Local changes saved by the user.
        **/
        protected var local_vars:SharedObject;
        /**
         * The close button.
        **/
        public var buttonref:Button;
        public function PositionWindow()
        {
            title = "Position";
            local_vars  = SharedObject.getLocal("cladimo-highlights");
            showCloseButton = true;
            addEventListener(CloseEvent.CLOSE, close);
            setStyle("borderAlpha", 1);
        }
        public function popup(evt:MouseEvent):void
        {
            if (! this.isPopUp)
            {
                PopUpManager.addPopUp(this, buttonref, false);
                var p:Point = new Point()
                p.x = 0;
                p.y = 0;
                p = buttonref.localToGlobal(p);
                this.x = p.x;
                this.y = p.y + buttonref.height;
                info.validateNow();
                info.height = info.textHeight + 20; // + info.viewMetrics.top + info.viewMetrics.bottom;
                info.width = info.textWidth + 10; // + info.viewMetrics.left + info.viewMetrics.right;
                height = info.height + viewMetricsAndPadding.top + viewMetricsAndPadding.bottom;
                width = info.width + viewMetricsAndPadding.left + viewMetricsAndPadding.right; 
            }
            else
            {
                PopUpManager.removePopUp(this);
            }
        }
        public function close(evt:CloseEvent):void
        {
            PopUpManager.removePopUp(this);
        }
        override protected function createChildren():void
        {
            super.createChildren();
            if (! info) init_info();
        }
        private function init_info():void
        {
            info = new TextArea();
            info.wordWrap = false;
            info.editable = false;
            info.horizontalScrollPolicy = "off";
            info.verticalScrollPolicy = "off";
            info.setStyle("leading", 3);
            addChild(info);
            info.text = buttonref.label;
        }
        public function get text():String
        {
            if (info) return info.text;
            return "";
        }
        public function set text(v:String):void
        {
            if (info) info.text = v;
        }

        public function update(evt:Event):void
        {
            var bounds:Array = evt.target.bounds;
            if (bounds.length == 1) 
                buttonref.label = bounds[0].species + "." + bounds[0].chrom + ":" + bounds[0].seq_start + "-" + bounds[0].seq_end;
            if (!info) init_info();
            if (info)
            {
                info.text = '';
                for each (var line:Object in bounds)
                {
                    if (line != null)                
                    {
                        info.text += line.species + "." + line.chrom + ":" + line.seq_start + "-" + line.seq_end + "\n";
                        if (bounds.length > 1) 
                            buttonref.label = "selection";
                    }
                    else
                        info.text += "\n";
                }
                info.validateNow();
                info.height = info.textHeight + 20; // + info.viewMetrics.top + info.viewMetrics.bottom;
                info.width = info.textWidth + 10; // + info.viewMetrics.left + info.viewMetrics.right;
                height = info.height + viewMetricsAndPadding.top + viewMetricsAndPadding.bottom;
                width = info.width + viewMetricsAndPadding.left + viewMetricsAndPadding.right; 
            }
        }
    }
}

