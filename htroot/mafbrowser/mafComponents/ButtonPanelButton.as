package mafComponents
{
    import spark.components.*;
    import mafComponents.*;
    /**
    * Button class for DataSets and DataClass instances
    **/
    public class ButtonPanelButton extends Button
    {
        private var _elementClass:String;
        public function get elementClass():String { return _elementClass; }

        [Bindable]
        public var borderColor:uint;

        public function ButtonPanelButton(element_class:String="")
        {
            super();
            _elementClass = element_class;
            borderColor = 0x000000;
        }
    }
}
