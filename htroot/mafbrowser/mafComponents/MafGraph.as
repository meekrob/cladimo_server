/* MafGraph: HBox
*/
package mafComponents
{
    import mx.events.ScrollEvent;
    import flash.display.*;
    import flash.text.*;
    import flash.events.Event;
    import flash.geom.Point;
    import mx.core.EdgeMetrics;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.Text;
    import mx.controls.TextArea;
    import mx.containers.*;
    import mx.rpc.events.ResultEvent;
    import flash.events.MouseEvent;
    import mx.utils.ObjectUtil;
    import mafComponents.*;
    import mafComponents.events.BrowserScrollEvent;

    /**
    * The primary container for the mini-graph in the lower half of the console.
    **/
    public class MafGraph extends Panel
    {
        /**
        * Species labels for rows in the MafGraph.
        **/
        public var sourceblock:TextArea;
        /**
        * Used by MiniBlocks to establish the height measurement.
        **/
        public var box_height:Number;
        /** 
        * Height of a character of text in the sourceblock. Used to establish the height of rows.
        **/
        public var char_height:Number;
        /** 
        * Width of a character of text in the sourceblock. Used to size the sourceblock.
        **/
        public var char_width:Number;
        /**
        * Rectangular area that approximates the scroll location of the MafViewer.
        * @see MafViewer
        **/
        public var selectbox:TextArea;
        /**
        * The left shift of the selectbox, determined by the horizontal scroll of the MafViewer.
        **/
        public var selectX:Number;
        /**
        * The top shift of the selectbox, determined by the horizontal scroll of the MafViewer.
        **/
        public var selectY:Number;
        /**
        * The width of the selectbox, determined by the horizontal span of the MafViewer.
        **/
        public var selectWidth:Number;
        /**
        * The height of the selectbox, determined by the vertical span of the MafViewer.
        **/
        public var selectHeight:Number;
        /**
        * Debugging.
        **/
        public var msg:TextArea;
        /**
        * UNUSED.
        **/
        public var colorArray:Array;

        /**
        * The list of miniblocks in the component.
        **/
        public var miniblocks:Array;
        private var new_miniblocks:Array;
        private var new_miniblocksChanged:Boolean = false;

        /**
        * Constructor.
        **/
        public function MafGraph()
        {
            super();
            layout = "horizontal";
            status = "Overview";
            setStyle("horizontalGap", 0);
            setStyle("borderColor", 0x00008B);
            setStyle("headerColors", [0xE0FFFF, 0xE0FFFF]);
            setStyle("borderAlpha", 1);
            setStyle("highlightAlphas", [0,0]);
            setStyle("borderThicknessRight", 0);
            setStyle("borderThicknessBottom", 0);
            setStyle("borderThicknessLeft", 4);
            setStyle("paddingRight", 10);
            miniblocks = new Array();
            new_miniblocks = new Array();
            selectX = 0;
            selectY = 0;

            colorArray = new Array(
                0x000000, // black 0
                0x888888, // gray 1
                0xE0E0E0, // lightgray 2
                0xFF0000, // red 3
                0x00FF00, // green 4
                0x0000FF, // blue 5
                0xFF00FF, // magenta 6
                0xFFFF00, // yellow 7
                0x00FFFF // cyan 8
            );
        }

        public function processBrowserScroll(evt:BrowserScrollEvent):void
        {
            trace(name + ".processBrowserScroll");
            var centrPnt:Number = evt.scroller.text_scroll_position+(evt.scroller.text_scroll_width/2);
            var leftPnt:Number = centrPnt - width/2;

            trace(name, horizontalScrollPosition + "/" + maxHorizontalScrollPosition,
                        evt.scroller.text_scroll_position + ":" + centrPnt, 
                        width, 
                        getExplicitOrMeasuredWidth());
                
            horizontalScrollPosition = Math.max(0, leftPnt);
            draw_selectbox( evt.scroller.text_scroll_position, 
                            evt.vertical_scroll_position, 
                            evt.scroller.text_scroll_width, 
                            evt.text_scroll_height);
        }

        /**
        * Set styles and properties on a miniblock.
        **/
        public function miniblock_settings(miniblock:TextArea):void
        {
            miniblock.setStyle("fontFamily", "Courier");
            miniblock.setStyle("fontSize", "9");
            miniblock.verticalScrollPolicy = "off";
            miniblock.horizontalScrollPolicy = "off";
            miniblock.wordWrap = false;
            miniblock.setStyle("paddingLeft",0);
            miniblock.setStyle("paddingRight",0);
            //miniblock.setStyle("borderSides", "left");
        }
        /**
        * Number of rows in the MafGraph/alignment.
        **/
        public var nrows:uint = 0;
        /**
        * Write the species names into the sourceblock textarea. Also sets nrows.
        * @see #nrows
        * @see #sourceblock
        **/
        public function set_panel(str:Array):void
        {
            nrows = str.length;
            miniblock_settings(sourceblock);
            sourceblock.text = str.join("\n");
            trace(name + ".set_panel(): sourceblock.text: ", "`" + sourceblock.text + "'");
            sourceblock.validateNow();

            var nbDim:TextLineMetrics = sourceblock.measureText("y");
            char_width = nbDim.width;
            char_height = nbDim.height;

            sourceblock.width = sourceblock.textWidth + char_width;
            sourceblock.height = sourceblock.textHeight + char_height;
            box_height = sourceblock.height;
        }
        /**
        * Calls draw_block() on each block.
        * @see #draw_block()
        **/
        public function draw(blocks:Array):void
        {
            //var block_i:int = 0;
            //var nblocks:int = blocks.length;
            for each (var block:Block in blocks)
            {
                draw_block(block);
                //block_i += 1;
            }
        }
        /**
        * Creates a MiniBlock instance based on the block.
        **/
        public function draw_block(block:Block):void
        {
            trace("MafGraph.draw_block(", block.name, "):", block.text_size);
            //var miniblock:TextArea = new TextArea();
            var miniblock:MiniBlock = new MiniBlock(block);
            miniblock.name = "Mini" + miniblock.block.name;
            miniblock.mafgraph = this;
            new_miniblocks.push(miniblock);
            new_miniblocksChanged = true;
            invalidateProperties();
            invalidateSize();
            invalidateDisplayList();
        }
        /**
        * Calls draw() on the MiniBlock.
        * @see MiniBlock#draw()
        **/
        public function draw_miniblock(miniblock:MiniBlock):void
        {
            miniblock.draw();
        }

        /**
        * If selectbox is created, calls draw_selectbox().
        * <p>Calls <code>draw_selectbox(selectX - sourceblock.width, selectY, selectWidth, selectHeight)</code>
        * </p>
        * @see #selectX
        * @see #selectY
        * @see #selectWidth
        * @see #selectHeight
        **/
        public function redraw_selectbox():void
        {
            if (selectbox) draw_selectbox(selectX - sourceblock.width, selectY, selectWidth, selectHeight);
        }
        /**
        * Creates (if necessary), sizes and places the selectbox.
        **/
        public function draw_selectbox(_x:Number, _y:Number, _width:Number, _height:Number):void
        {
            if (! selectbox)
            {
                selectbox = new TextArea();
                selectbox.setStyle("contentBackgroundAlpha", "0");
                selectbox.setStyle("borderColor", 0xFF0000);
                var borderThickness:Number = 10;
                selectbox.setStyle("contentBorderThickness", borderThickness);
                addChild(selectbox);
            }
            selectX = _x + sourceblock.width;
            selectY = _y * char_height;
            selectbox.move(selectX,selectY);
            selectWidth = _width;
            selectHeight = _height;
            //selectbox.height = _height * (char_height-1);
            selectbox.height = _height * nrows;
            selectbox.width = _width;
        }
        /**
        * In addition to super(), calls addMiniBlockSorted and draw_miniblock on any pending miniblock.
        * @see #addMiniBlockSorted()
        * @see #draw_miniblock()
        **/
        override protected function commitProperties():void
        {
            super.commitProperties();
            if (new_miniblocksChanged)
            {
                //trace("MafGraph.commitProperties(): new_miniblocksChanged:", new_miniblocks.length);
                var i:int = 0;
                var miniblock:MiniBlock;
                do
                {
                    miniblock = new_miniblocks.pop();
                    miniblocks.push(miniblock); 
                    var blockIndex:int = miniblock.block.getIndex();
                    //trace("MafGraph.commitProperties(): adding miniblock", i, "for block", blockIndex);
                    //addChild(miniblock); 
                    addMiniBlockSorted(miniblock);

                    draw_miniblock(miniblock);
                    i++;
                } while (new_miniblocks.length > 0);
                //trace("MafGraph.commitProperties(): new_miniblocksChanged. Now has", miniblocks.length, "miniblocks in display list.");
                //trace("MafGraph.commitProperties(): new_miniblocksChanged. Now has", numChildren, "children in display list.");
                new_miniblocksChanged = false;
            }
        }
        /**
        * Adds the MiniBlock according to the sort order of the actual Blocks.
        **/
        protected function addMiniBlockSorted(newminiblock:MiniBlock):void
        {
            
            for (var child_i:int = 0; child_i < numChildren; child_i++)
            {
                var displayObj:DisplayObject = getChildAt(child_i);
                if (displayObj is UIComponent)
                {
                    var obj:UIComponent = displayObj as UIComponent;
                    if (obj is MiniBlock)
                    {
                        if (newminiblock.order > (obj as MiniBlock).order)
                        {
                            addChildAt(newminiblock, child_i);
                            return;
                        }
                    }
                }
            }
            addChild(newminiblock);
        }
        /**
        * @private
        **/
        override protected function measure():void
        {
            super.measure();
            /*var blockHeights:Number = 0;
            var blockWidths:Number = 0;
            for (var block_i:int = 0; block_i < miniblocks.length; block_i++)
            {
                blockHeights += miniblocks[block_i].getExplicitOrMeasuredHeight();
                blockWidths += miniblocks[block_i].getExplicitOrMeasuredWidth();
            }
            measuredMinWidth = blockWidths;
            measuredMinHeight = blockHeights;
            measuredWidth = measuredMinWidth;
            measuredHeight = measuredMinHeight;*/
        }
        import mx.core.UIComponent;
        /**
        * In addition to super(), calls redraw_selectbox().
        * @see #redraw_selectbox()
        **/
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (selectbox) redraw_selectbox();
            /*
            trace("MafGraph.updateDisplayList(", unscaledWidth, ",", unscaledHeight, ")");
            trace("  child   x   y gEMW gEMH");
            var obj_i:int = 0;
            var runningX:Number = 0;
            var runningY:Number = 0;
            for each (var obj:UIComponent in getChildren())
            {
                if (obj is MiniBlock)
                {
                    trace(" miniblock", (obj as MiniBlock).order);
                }
                 
                trace(" ", obj_i, obj.x, obj.y, obj.getExplicitOrMeasuredWidth(), obj.getExplicitOrMeasuredHeight());
                obj_i++;
            }
            */

        }
        /**
        * In addition to super(), creates the sourceblock.
        * @see #sourceblock
        **/
        override protected function createChildren():void
        {
            super.createChildren();
            if (! sourceblock)
            {
                sourceblock = new TextArea();
                addChild(sourceblock);
            }
        }
    }
}

