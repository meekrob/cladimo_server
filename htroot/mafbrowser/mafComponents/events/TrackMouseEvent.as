/*
* TrackMouseEvent - All the properties of a mouseEvent, plus the resolved character index.
*/
package mafComponents.events
{
    import flash.geom.Rectangle;
    import mafComponents.*;
    import flash.events.*;
    public class TrackMouseEvent extends MouseEvent
    {
        public static const CLICK:String = "clickTrack";
        public static const MOUSE_MOVE:String = "moveMouseTrack";
        public static const MOUSE_UP:String = "mouseUpTrack";
        public static const MOUSE_DOWN:String = "mouseDownTrack";
        public static const MOUSE_OUT:String = "mouseOutTrack";

        /** info about the track, text coordinates, etc. **/
        private var _textIndex:int = -1;
        public function get textIndex():int { return _textIndex; }
        private var _charBounds:Rectangle = null;
        public function get charBounds():Rectangle { return _charBounds; }

        public override function clone():Event
        {
            return new TrackMouseEvent(_evt);
        }

        public override function toString():String
        {
            return this.type + " Event";
        }

        private var _evt:MouseEvent;

        public function TrackMouseEvent(evt:MouseEvent)
        {
            super(
                evt.type + 'Track',
                //bubbles = evt.bubbles;
                false,
                evt.cancelable,
                evt.localX,
                evt.localY,
                evt.relatedObject,
                evt.ctrlKey,
                evt.altKey,
                evt.shiftKey,
                evt.buttonDown,
                evt.delta
            );

            _evt = evt;

            var track:Track = Track(evt.currentTarget);
            if ((! isNaN(localX)) && (! isNaN(localY)))
            {
                _textIndex = track.getCharIndexAtPoint(localX, localY);
                _charBounds = track.getCharBoundaries(_textIndex);
            }
        }
    }
}
