package mafComponents.events
{
    import flash.events.Event;
    import flash.geom.Rectangle;
    /**
        The BlockSelectionEvent class interprets mouse events from the component tracks within and boils them down into
        coordinates representing a rectangular subset of the block, defined from left to right by the text index range selected,
        and top to bottom by the tracks selected.

        <p>These coordinates are represented by the Rectangle blockCoordinates.

        Concurrently, the pixel coordinates of the stage are also relayed, originally by the component tracks which responded to the
        events. These coordinates will be used by BlockSelection and other classes to draw or place elements on top of the Block.

        These coordinates are represented by the Rectangle blockArea.</p>
        

    **/
    public class BlockSelectionEvent extends Event
    {
        /**
            The BlockSelectionEvent.BLOCK_SELECTION_DRAW event type indicates that the user has clicked somewhere on
            the block and held the mouse button down while sweeping across the block.
        **/
        public static const BLOCK_SELECTION_DRAW:String = "blockSelectionDraw";

        /**
            The BlockSelectionEvent.BLOCK_SELECTION_FINISH event type indicates that either the user has finished sweeping the block
            with the mouse button down, or has simply clicked on a single character/position in the Block.
        **/
        public static const BLOCK_SELECTION_FINISH:String = "blockSelectionFinish";

        public var blockCoordinates:Rectangle = null;
        public var blockArea:Rectangle = null;
        public function BlockSelectionEvent(type:String,  blockCoordinates:Rectangle=null, blockArea:Rectangle=null)
        {
            super(type);
            this.blockCoordinates = blockCoordinates;
            this.blockArea = blockArea;
        }
        public override function clone():Event
        {
            return new BlockSelectionEvent(this.type, this.blockCoordinates, this.blockArea);
        }
        public override function toString():String
        {
            return this.type + " Event";
        }
    }
}
