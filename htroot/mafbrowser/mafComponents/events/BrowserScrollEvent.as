/*
* BrowserScrollEvent - Broadcast set changes to the MAFBrowser's position, as defined by any of the controls that may change it.
*/
package mafComponents.events
{
    import mafComponents.BlockScroller;
    import flash.events.Event;
    import mx.events.ScrollEvent;
    import mx.core.UIComponent;
    import mx.containers.*;
    public class BrowserScrollEvent extends ScrollEvent
    {
        private var _view_window:HBox;
        public function get view_window():HBox { return _view_window; }

        private var _scroller:BlockScroller;
        public function get scroller():BlockScroller { return _scroller; }

        private var _blockIndex:int;
        public function get blockIndex():int { return _blockIndex; }

        private var _blockRef:UIComponent;
        public function get blockRef():UIComponent { return _blockRef; }

        private var _blockOffset:int;
        public function get blockOffset():int { return _blockOffset; }

        private var _columnInBlock:int;
        public function get columnInBlock():int { return _columnInBlock; }

        /**
        * Gives scroller.tet_scroll_widt, or -1 if the scroller is not created yet.
        * @see BlockScroller#text_scroll_width
        **/
        public function get text_scroll_width():int
        {
            if (_scroller) return _scroller.text_scroll_width;
            return -1;
        }


        /**
        * Returns scroller.text_scroll_position, or -1 if the scroller is not created yet.
        * @see BlockScroller#text_scroll_position
        **/
        public function get text_scroll_position():int
        {
            if (_scroller) return _scroller.text_scroll_position;
            return -1;
        }

        public function get vertical_scroll_position():int
        {
            return _view_window.verticalScrollPosition/_scroller.single_base_height;
        }
        public function get text_scroll_height():int
        {
            return ((_view_window.height)/_scroller.single_base_height);
        }

        public override function clone():Event
        {
            return new BrowserScrollEvent(_evt, _scroller, _view_window);
        }
        public override function toString():String
        {
            return "BrowserScroll:" + _scroller.horizontalScrollPosition;
        }

        private var _evt:ScrollEvent;
        public static const DEFAULT_NAME:String = "BROWSER_SCROLL";

        public function BrowserScrollEvent(evt:ScrollEvent, scroller:BlockScroller, view_window:HBox)
        {
            super(DEFAULT_NAME,
                true, //evt.bubbles,
                evt.cancelable,
                evt.detail,
                evt.position,
                evt.direction,
                evt.delta
            );
            _evt = evt;
            _scroller = scroller;
            _view_window = view_window;

            _blockRef = _scroller.getBlockAtX(_scroller.horizontalScrollPosition);
            if (_blockRef) 
            { 
                _blockOffset = _blockRef.getLayoutBoundsX();
                _columnInBlock = (_scroller.horizontalScrollPosition - _blockOffset) / _scroller.char_width;
            }
            else
            {   
                _blockOffset = -1;
                _columnInBlock = -1;
            }
            
        }
    }
}
