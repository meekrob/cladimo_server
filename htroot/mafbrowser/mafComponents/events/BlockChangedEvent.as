package mafComponents.events
{
    import flash.events.Event;
    import mafComponents.Block;


    /**
    * Dispatched when the components of a <code>Block</code> have changed.
    **/
    public class BlockChangedEvent extends Event
    {
        private var _block:Block;
        public function get block():Block
        {
            return _block;
        }
        public static const DEFAULT_NAME:String = "BLOCK_CHANGED";
        public function BlockChangedEvent(blockThatChanged:Block)
        {
            super(DEFAULT_NAME);
            _block = blockThatChanged;
        }
        public override function clone():Event
        {
            return new BlockChangedEvent(_block);
        }
        public override function toString():String
        {
            return DEFAULT_NAME + ":" + _block.name;
        }

    }
}
