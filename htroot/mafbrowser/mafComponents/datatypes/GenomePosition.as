package mafComponents.datatypes
{
    import mafComponents.*;
    /**
    * Abstraction for a genome position.
    *
    * <p>
    * A genome position must specify the chromosome (or sequence name), plus the limits of the range within that sequence.
    * Optionally, it can also be informative to record the species, for use in multispecies alignments.
    * Also, the genome range may be specified for the '-' strand of the sequencing/assembly direction, rather than the '+'
    * strand. If so, the conversion from '-' to '+' requires the size of the sequence in the form of: 
    * <listing>
    * plus_strand_index = seqSize - minus_strand_index
    * </listing>
    * </p>
    * <p>
    * There are two commonly-used coordinate systems:
    * <ul>
    * <li>BED-like: 0-based intervals, end point is exclusive.</li>
    * <li>GFF-like: 1-based intervals, end point is inclusive.</li>
    * </ul>
    * This object stores coordinates in a base/offset system, equivalent to the BED-like system, and converts from it to return
    * GFF-like coordinates. Consequently, an object initialized with GFF-like coordinates is converted and stored using the BED-like system.
    * </p>
     <p>
    * Example 0 and 1-based coordinates for the subsequence 'TAGA' in the sequence below.
     </p>

    <p>
    <strong>BED-like:</strong>
     <table>
    <tr>
        <td>chromosomeN</td>
        <td>A</td> <td>G</td> <td>G</td> <td>T</td> <td>A</td> <td>G</td> <td>A</td> <td>C</td> <td>C</td>
    </tr>
    <tr>
        <td>0-based</td>
        <td>0</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
    </tr>
    <tr>
     <td >Exclusive end</td>
     <td > </td>
     <td > </td>
     <td > </td>
     <td>■</td>
     <td>■</td>
     <td>■</td>
     <td>■</td>
     <td>□</td>
    </tr>
    </table>

    <strong>GFF-like:</strong>
    <table>
    <tr>
        <td>chromosomeN</td>
        <td>A</td> <td>G</td> <td>G</td> <td>T</td> <td>A</td> <td>G</td> <td>A</td> <td>C</td> <td>C</td>
    </tr>
    <tr>
        <td>1-based</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
    </tr>
    <tr>
     <td >Inclusive end</td>
     <td > </td>
     <td > </td>
     <td > </td>
     <td>■</td>
     <td>■</td>
     <td>■</td>
     <td>■</td>
    </tr>
    </table>
    </p>
    <p>
    The two coordinate systems produce similar, but slightly different values:
    <ul>
        <li>BED-like: chrN:3-7</li>
        <li>GFF-like: chrN:4-7</li>
    </ul>
    </p>
    **/
    public class GenomePosition 
    {
        /**
         * Chromosome (or sequence name) of the genome position.
        **/
        public var chrom:String = "";
        /**
         * Lower index of the sequence range.
        **/
        public function get seq_start():uint
        {
            if (strand == '-')
                return (seqSize + mode) - (_base+_size);

            return _base + mode;
        }
        /**
         * Higher index of the sequence range.
        **/
        public function get seq_end():uint
        {
            if (strand == '-')
                return (seqSize + mode) - _base;

            return _base + _size; // true for both modes
        }
        /**
         * Can be '+' or '-'. When '-', the index is decreasing.
        **/
        public var strand:String = '+';
        /**
         * Total chromosome (or sequence) size, used to convert between +/- numbering systems.
        **/
        public var seqSize:uint = 0;
        /**
         * The species from which the sequence is derived.
        **/
        public var species:String = "";
        public var _type:String;
        public static const BED_LIKE:uint = 0;
        public static const SANGER:uint = 1;
        /**
         * Determines whether the numbering is 0-based with exclusive ranges, as in BED-like, or 1-based with inclusive ranges, as in Sanger.
         * @see #BED_LIKE
         * @see #SANGER
        **/
        public var mode:uint = SANGER;
        private var _base:uint = 0;
        private var _size:uint = 0;
        /**
        * The internal, +-strand 0-based representation of the interval.
        **/
        public function get base():uint { return _base; }
        /**
        * The size (or span) of the interval.
        **/
        public function get size():uint { return _size; }
        /**
         * Create a GenomePosition interval. Internally, all intervals are represented using a typical array-oriented base/offset method 
         * and computed for the + strand; the exact values of <code>seq_start</code> and <code>seq_end</code> are determined by <code>mode</code> and <code>strand</code>, and can be switched as needed.
         * @param properties 
        <ul>
                <li><code>mode:uint</code>: <strong>0</strong> for BED-like and <strong>1</strong> for Sanger</li>
                <li><code>strand:String</code>: <strong>+</strong> or <strong>-</strong>. If <strong>-</strong> is specified, <code>seqSize</code> must also be specified.</li>
                <li><code>seqSize:uint</code>: Total chromosome size of chrom</li>
                <li><code>chrom:String</code>: Chromosome (or sequence name) of the genome position.</li>
                <li><code>seq_start:uint</code>: Lower sequence index</li>
                <li><code>seq_end:uint</code>: Greater sequence index</li>
                <li><code>species:String</code>: Species to which the sequence pertains</li>
        </ul>
         * @throws ArgumentError If a '-' strand is specified without a seqSize, ArgumentError is thrown.
        **/
        public function GenomePosition(properties:Object)
        {
            if (properties.hasOwnProperty('mode'))
                mode = properties.mode;
            else
                mode = SANGER;

            if (properties.hasOwnProperty('strand'))
            {
                strand = properties.strand;
                if (properties.hasOwnProperty('seqSize'))
                    seqSize = properties.seqSize;
                else if (strand == '-')
                    throw new ArgumentError("GenomePosition: seqSize must be provided with a minus strand argument");
            }

            if (properties.hasOwnProperty('chrom'))
                chrom = properties.chrom;
            if (properties.hasOwnProperty('species'))
                species = properties.species;

            var s:uint;
            var e:uint;
            if (properties.hasOwnProperty('seq_start')) s = properties.seq_start;
            if (properties.hasOwnProperty('seq_end')) e = properties.seq_end;

            if (strand == '+')
                _base = s - mode;
            if (strand == '-')
                _base = (seqSize + mode) - e;

            _size = (e - s) + mode;
        }
        public function toString():String
        {
            var outstring:String = '';
            outstring += chrom + ':' + seq_start + '-' + seq_end;
            return outstring;
        }
        public function toLocaleString():String
        {
            return toString();
        }
        public function valueOf():Object
        {
            return toString();
        }
    }
}
