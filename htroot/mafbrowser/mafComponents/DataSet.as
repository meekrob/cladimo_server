package mafComponents
{
    import flash.geom.Point;
    import mx.rpc.events.ResultEvent;
    import flash.net.SharedObject;
    import flash.events.*;
    import mx.events.*;
    import mx.managers.*;
    import mx.containers.*;
    import mx.controls.*;
    import mx.utils.*;
    import mx.collections.ArrayCollection;

    /**
    * The container for Element instances associated with a dataset
    **/
    public class DataSet extends HBox
    {
        /**
        * Array of interval data.
        **/
        public var intervals:Array;
        /**
        * Associative Array of Element instances, keyed by element class name, values are DataClass instances
        * @see DataClass
        **/
        public var elements:Object;
        public var labels:Object;
        /**
        * Object of element_classes - UNUSED!!!
        **/
        public var element_classes:Object;
        /**
        * Accessed from SharedObject.getLocal('cladimo-highlights')
        * @see SharedObject#getLocal
        **/
        public var local_vars:SharedObject;
        /**
        * Gotten from console.color_settings
        * @see MafConsole#color_settings
        **/
        public var global_vars:Object;
        /**
        * Reference to the parent console.
        **/
        public var console:MafConsole;
        /**
        * Constructor- called during MafConsole.datasethandle_obj.
        *
        * <p>
        * A DataSet instance is created by MafConsole during an event chain that loads XML input.
        * </p>
        **/
        public function DataSet(parentConsole:MafConsole=null) 
        {
            super();
            name = "new dataset";
            elements = new Object();
            element_classes = new Object();
            labels = new Object();

            console = parentConsole;
            if (console) global_vars = console.color_settings;
            local_vars  = SharedObject.getLocal("cladimo-highlights");
        }
        /**
        * Adds an individual Element instance.
        * <p>
        * If called with a non-empty string for element_class, the Element instance is associated with that DataClass. A new DataClass instance
        * is created if necessary. The Element instance is added by calling DataClass.push() in the <code>elements</code> objects.
        * </p>
        * <p>If no element_class is specified, it is added under "default".
        * </p>
        * @see #elements
        * @see DataClass#push()
        **/
        public function addDataElement(el:Element, element_class:String=""):void
        {
            trace(name + '.addDataElement(', el, ",", element_class, ')');
            if (element_class != "") 
            { 
                if (! elements.hasOwnProperty(element_class))
                    elements[element_class] = new DataClass(element_class);

                elements[element_class].push(el);
                elements[element_class].dataset = this;
                el.element_class = element_class;
                /*if (global_vars.hasOwnProperty(element_class))
                    el.set_color(global_vars[element_class]);
                else if (local_vars.data.hasOwnProperty("color-" + element_class))
                    el.set_color(local_vars.data["color-" + element_class]);*/
            }
            else 
            {
                if (! elements.hasOwnProperty("default"))
                    elements["default"] = new DataClass();
                elements["default"].push(el);
            }
        }
        /**
        * In addition to super(), creates ButtonPanelButton instances for each element_class and sets their style properties.
        **/
        override protected function createChildren():void
        {
            super.createChildren();
            var element_list:Array = new Array();
            for (var element_class1:String in elements) element_list.push(element_class1);
            element_list.sort();
            for each (var element_class:String in element_list)
            {
                    if (! labels.hasOwnProperty(element_class))
                    {
                        labels[element_class] = new ButtonPanelButton(element_class);
                        labels[element_class].name = 'ButtonPanelButton_' + element_class;
                        labels[element_class].label = element_class.split("_")[0];
                        labels[element_class].setStyle("contentBackgroundColor", elements[element_class].elements[0].getStyle("contentBackgroundColor"));
                        labels[element_class].height = elements[element_class].elements[0].height;
                        labels[element_class].addEventListener(MouseEvent.CLICK, popup);
                        elements[element_class].buttonref = labels[element_class];
                        addChild(labels[element_class]);
                    }
            }
        }

        import mx.core.UIComponent;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            for (var i:int; i< numChildren; i++)
            {
                var b:UIComponent = UIComponent(getChildAt(i));
                var bWidth:Number = b.getExplicitOrMeasuredWidth();
                var bHeight:Number = b.getExplicitOrMeasuredHeight();
                trace(name + '.updateDisplayList(' +unscaledWidth + ',' + unscaledHeight + '): ' + b.name, bHeight);
            }
        }
        /**
        * Passes data object from the event to handle_obj
        * @see #handle_obj()
        **/
        public function handle(event:ResultEvent):void
        {
            var result:Object = event.result.file.bed;
            trace("handle_obj MAFLoaded event listener");
            console.addEventListener("MAFLoaded", function():void
            {
                handle_obj(result);
            });
        }
        /**
        * Process a dataset object into an array of intervals.
        **/
        public function handle_obj(dataset:Object):void
        {
            intervals = new Array();
            for each (var interval:Object in dataset.interval)
            {
                intervals.push(interval);
            }
            trace('Dataset.handle_obj, intervals.length:', intervals.length);
        }
        /**
        * Creates a Pop-Up of a DataClass
        * @see DataClass
        **/
        public function popup(event:MouseEvent):void
        {
            //var element_class:String = event.currentTarget.label;
            var buttn:ButtonPanelButton = event.currentTarget as ButtonPanelButton; // labels[element_class];
            var element_class:String = buttn.elementClass;
            var dc:DataClass = elements[element_class];

            if (!dc.isPopUp)
            {
                PopUpManager.addPopUp(dc, buttn, false);
                var p:Point = new Point();
                p.x = 0;
                p.y = 0;
                p = buttn.localToGlobal(p);
                dc.x = p.x;
                dc.y = p.y + buttn.height;
                //buttn.removeEventListener(popup);
            }
            else
            {
                PopUpManager.removePopUp(dc);
            }
        }
        /**
        * Set the color for an element class
        * @see color_changer()
        **/
        public function set_color(newcolor:Number, element_class:String="default"):void
        {
            local_vars.data["color-" +  element_class] = newcolor;
            for each (var el:Element in elements[element_class].elements)
            {
                el.set_color(newcolor);
                el.redraw();
            }
            labels[element_class].setStyle("fillColors", elements[element_class][0].getStyle("fillColors"));
            //labels[element_class].setStyle("chromeColor", elements[element_class][0].getStyle("chromeColor"));
            labels[element_class].setStyle("contentBackgroundColor", elements[element_class][0].getStyle("contentBackgroundColor"));
        }
        /**
        * Invoke set_color() through a ColorPickerEvent
        * @see set_color()
        **/
        public function color_changer(cp:ColorPickerEvent):void
        {
            set_color(cp.currentTarget.selectedColor, cp.currentTarget.name);
            var cp_obj:ColorPicker = cp.currentTarget as ColorPicker;
            PopUpManager.removePopUp(cp_obj);
        }
    }
}
