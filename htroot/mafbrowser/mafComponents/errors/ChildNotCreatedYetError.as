package mafComponents.errors
{
    public class ChildNotCreatedYetError extends ReferenceError
    {
        public function ChildNotCreatedYetError(message:String = "")
        {
            super(message);
        }
    }
}
