function contentResize() {
        var w = $(window).width() - 50;
        var h = (w * .618034);
        $('#mainBox').width(w);
        $('#mainBox').height(h);
        $('#honcho').text('window: h' + $(window).height() + ',w' + $(window).width() + '. mainBox: h' + $('#mainBox').height() + ', w' + $('#mainBox').width());
        var mainBoxWidth = $('#mainBox').width();
        if (mainBoxWidth <= 980)
        {
            $('#iupacFieldset').css('font-size', '70%');
        }
        else if (mainBoxWidth <= 994)
        {
            $('#iupacFieldset').css('font-size', '80%');
        }
        else if (mainBoxWidth <= 1018)
        {
            $('#iupacFieldset').css('font-size', '90%');
        }
        else 
        {
            $('#iupacFieldset').css('font-size', '100%');
        }
}

$(document).ready(function() {

    contentResize();
    $('#cladimo_form input[name="target"]:radio').change(function() {
        //var form_target = $('#cladimo_form').attr('target');
        //alert('radio change triggered:' + form_target + "=>" + $(this).val());
        var selected_target = $(this).val();
        $('#cladimo_form').attr('target', selected_target);
    });

    $(window).resize(function() {
        contentResize();
    });

});
