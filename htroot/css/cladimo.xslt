<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<xsl:template match="/">
<html>
    <head>
        <title>Cladimo output XML for <xsl:value-of select="cladimo_output/dataname" /></title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="/cladimo-web/cladimo_output.css" />
    </head>
    <body>
        <h2>Cladimo output XML for 
            <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('/cgi-bin/results.py?format=gff&amp;tag=', normalize-space(cladimo_output/accession))" />
            </xsl:attribute>
            <xsl:value-of select="cladimo_output/dataname" />
            </a>
        </h2>
    <div style="float: left;">
    <!-- Output summary info here-->
        <div class="output_summary">
        </div>
    <!-- Table of contents -->
    <table class="contents">
    <tr>
        <th>Block</th><th>Cladistic Motifs</th><th>Individual sequence matches</th>
    </tr>
    <xsl:for-each select="cladimo_output/scanned_block">
    <tr>
        <td align="center">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('#block_',position())" />
            </xsl:attribute>
            <xsl:value-of select="concat('block_',position())" />
        </a>
        </td>
        <td align="center"><xsl:value-of select="count(cladistic_motif)"/></td>
        <td align="center"><xsl:value-of select="count(cladistic_motif/*/match)" /></td>
    </tr>
    </xsl:for-each>
    <tr>
        <th>Total</th>
        <th><xsl:value-of select="count(cladimo_output/*/cladistic_motif)" /></th>
        <th><xsl:value-of select="count(cladimo_output/*/*/*/match)" /> </th>
    </tr>
    </table>
    </div>
    <!-- detailed output -->
    <div style="float: left; width: 100%;">
        <xsl:for-each select="cladimo_output/scanned_block">
        <div class="scanned_block">
            <a>
                <xsl:attribute name="name">
                    <xsl:value-of select="concat('block_',position())"/>
                </xsl:attribute>
            </a>
            <h3 class="tight">Sequence/Alignment Block <xsl:value-of select="position()" /></h3>
            <table class="alignment_info">
            <tr>
                <th colspan="3">Reference Interval</th>
                <th>Type</th>
                <th>Columns</th>
                <th>Aligned Sources</th>
            </tr>
            <tr>
                <td align="center"><xsl:value-of select="alignment/reference_interval/chrom"/></td>
                <td align="center"><xsl:value-of select="alignment/reference_interval/start"/></td>
                <td align="center"><xsl:value-of select="alignment/reference_interval/end"/></td>
                <td align="center"><xsl:value-of select="alignment/@type" /></td>
                <td align="center"><xsl:value-of select="alignment/@columns" /></td>
                <td align="center"><xsl:value-of select="count(alignment/aligned_sources/aligned_source)"/></td>
            </tr>
            </table>
            
                    <!--<div class="block_aligned_sources">
                        <xsl:for-each select="alignment/aligned_sources/aligned_source" >
                            <div><xsl:value-of select="position()"/>) <xsl:value-of select="."/></div>
                        </xsl:for-each>
                    </div>-->
                <h3>Cladistic Motifs (<xsl:value-of select="count(cladistic_motif)" />)</h3>
<xsl:for-each select="cladistic_motif">
<table class="reference_interval">
<tr>
    <th>Cladistic Motif 
    </th>
    <th colspan="3">Reference Position</th>
    <th>Number of matches </th>
    <xsl:if test="boolean(./@pwm_name)"><th>Name</th></xsl:if>
    <xsl:if test="boolean(./@pwm_accession)"><th>Accession</th></xsl:if>
    <xsl:if test="boolean(./@pwm_motif)"><th>Motif</th></xsl:if> <!-- at least this one is always present -->
</tr>
<tr>
    <td align="center"><xsl:value-of select="position()" /></td>
    <td><xsl:value-of select="reference_interval/chrom"/></td>
    <td><xsl:value-of select="reference_interval/start"/></td>
    <td><xsl:value-of select="reference_interval/end"/></td>
    <td align="center">
        <xsl:value-of select="./@number_of_matches"/>
    </td>
    <xsl:if test="boolean(./@pwm_name)">
        <td><xsl:value-of select="./@pwm_name" /></td>
    </xsl:if>
    <xsl:if test="boolean(./@pwm_accession)">
        <td><xsl:value-of select="./@pwm_accession" /></td>
    </xsl:if>
    <xsl:if test="boolean(./@pwm_motif)">
        <td><xsl:value-of select="./@pwm_motif" /></td>
    </xsl:if>
</tr>
</table>
<table class="match">
<tr>
    <th style="background-color: lightgrey;">#</th>
    <th colspan="3">Reference position</th>
    <th style="background-color: lightgrey;">Sequence</th>
    <th colspan="2">Position in sequence</th>
    <th style="background-color: lightgrey;">Matched Sequence</th>
    <th>Orientation</th>
    <th style="background-color: lightgrey;">Score</th>
</tr>
<xsl:for-each select="matches/match">
<tr>
    <td style="background-color: lightgrey;"><xsl:value-of select="position()"/></td>
    <td><xsl:value-of select="reference_interval/chrom"/></td>
    <td><xsl:value-of select="reference_interval/start"/></td>
    <td><xsl:value-of select="reference_interval/end"/></td>
    <td style="background-color: lightgrey;"><xsl:value-of select="matched_position/source"/></td>
    <td><xsl:value-of select="matched_position/start"/></td>
    <td><xsl:value-of select="matched_position/end"/></td>
    <td align="center" style="background-color: lightgrey;"><xsl:value-of select="matched_sequence"/></td>
    <td><xsl:value-of select="reference_interval/strand"/></td>
    <td style="background-color: lightgrey;"><xsl:value-of select="reference_interval/score"/></td>
</tr>

</xsl:for-each>
</table>
</xsl:for-each>
        </div>
        </xsl:for-each>
</div><!-- detailed output -->
    </body>
</html>
</xsl:template>


</xsl:stylesheet>
