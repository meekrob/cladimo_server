#!/usr/bin/env python2.6
import cgi
import cgitb; cgitb.enable()
import sys,os,subprocess
import glob, string
import urllib

# get the project python modules
sys.path.append( os.path.abspath('../python') )
from accession import *
from htmilf import *
from formats import *

contents = cgi.FieldStorage()

#jobdir = '../jobdir'
jobdir = locale.jobdir

def toXML(path, dataname, tag):
    infile = open( path )
    doc = gff_to_styled_xml( infile, dataname, tag )
    print "Content-Type: application/xhtml+xml\n"
    print doc.toprettyxml(encoding="UTF-8")

def toText(path, tag):
    print 'Content-type: text/plain\n'
    print open(path).read()

def toList(*listargs):
    print 'Content-type: text/plain\n'
    for volume in listargs:
        print str(volume)

def filesByExtension( dircontents ):
    byextension = {}

    for entry in dircontents:
        dotplace = entry.rfind('.')
        if dotplace >= 0:
            ext = entry[dotplace:]
            byextension[ext] = entry

    return byextension

def main():

    if 'tag' in contents:

        tag = urllib.unquote( contents['tag'].value )
        dircontents = glob.glob( os.path.join( jobdir, '*' + tag + '*' ) )
        byextension = filesByExtension( dircontents )
        dataname = os.path.basename( byextension['.gff'] ).replace(tag + '_', '')

        if 'format' in contents:
            outformat = contents['format'].value
            if outformat == 'gff' and '.gff' in byextension:
                toText(os.path.join(jobdir, byextension['.gff']), dataname)
            elif outformat == 'xml' and '.gff' in byextension:
                toXML( os.path.join(jobdir, byextension['.gff']), dataname, tag )
            elif outformat == 'list':
                toList(dircontents, byextension)
        elif '.gff' in byextension:
            toXML( os.path.join(jobdir, byextension['.gff']), dataname, tag)
        else:
            toList(dircontents, byextension)
            

main()
