#!/usr/bin/env python2.6
import cgi
import cgitb; cgitb.enable()
import sys,os

contents = cgi.FieldStorage()
print "Content-Type: text/html\n\n"
print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Testing server vars</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
"""
sys.path.append( os.path.abspath('../python') )
import cladimo_locale; locale = cladimo_locale.cladimoLocale()
print locale.bxpython, "<br/>"
print cladimo_locale.basedir, "<br/>"
import bx.align.maf


print "hello world<br/>"
print contents
print contents.getvalue('tag')
print "<br/>".join( sys.path )
print """
</body>
</html>
"""


