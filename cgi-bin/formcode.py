#!/usr/bin/env python
import cgi, urllib2
import cgitb; cgitb.enable()
from htmilf import *
from formstuff import *
from accession import *

print htmlhead % {'style': '', 'title': '<title>Basic form code</title>'}

form = cgi.FieldStorage()
print_all_fields( form )

print htmlfoot
