#!/usr/bin/env python2.6
import sys,os
import cgi, urllib
import cgitb; cgitb.enable()

# get the project python modules
sys.path.append( os.path.abspath('../../python') )
import cladimo_locale; locale = cladimo_locale.cladimoLocale()
from htmilf import *
from formstuff import *
from accession import *

import formats

from xml.dom.minidom import Document
def main():
    form = cgi.FieldStorage()
    jobdir = locale.jobdir

    if 'tag' in form:
        tag = urllib.unquote( form['tag'].value )
        dircontents = glob.glob( os.path.join( jobdir, '*' + tag + '*.gff' ) )
        # should be 1 and only 1 match
        try:
            assert len(dircontents) == 1
        except AssertionError, e:
            raise AssertionError(str(e) + str(len(dircontents)))
        uploadedfilepath = dircontents[0]
        uploadedgffname = os.path.basename( uploadedfilepath ).replace(tag + "_", "")
        uploadedfile_h = open( uploadedfilepath )

        doc = formats.gff_to_xml( uploadedfile_h, uploadedgffname )
        uploadedfile_h.close()

        print "Content-Type: application/xhtml+xml\n"
        print doc.toprettyxml(encoding="UTF-8")
    else:
        print "Content-Type: text/html\n\n"
        print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
        <title>Maf File Upload</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>
    <body>
    """
        print form
        print "<hr>"
        print """
        <h1>Bed File Upload</h1>
        <form action="http://localhost/cgi-bin/bedxmlhandler.py" method="post" enctype='multipart/form-data'>
            <input type='file' name='filename'>
            <input type='submit' name='submit'>
        </form>
    </body>
    </html>
        """

def data_to_xml( infile ):

    dataset = None

    # create a mini document
    doc = Document()

    # create a new <bed> base element
    newbed = doc.createElement('bed')
    doc.appendChild(newbed)
    element_types = doc.createElement('types')
    types = {}
    newbed.appendChild(element_types)
    

    # create elements
    for line in infile:
        if line.startswith("#") or line.isspace():
            continue

        if line.startswith(">"): 
            if dataset:
                newbed.appendChild(dataset)
            dataset = doc.createElement('interval_set')
            line = line.lstrip(">")
            try:
                fields = line.split()
                dataset.setAttribute("chrom", fields[0])
                dataset.setAttribute("start", fields[1])
                dataset.setAttribute("end", fields[2])
                dataset.setAttribute("name", fields[3])
                #dataset.setAttribute("species", fields[4])
                splist = doc.createElement('species_list')
                for sp in fields[4].split(','):
                    spitem = doc.createElement('species_match')
                    spitem.setAttribute('species', sp)
                    splist.appendChild(spitem)
                dataset.appendChild(splist)

            except:
                pass

            continue

        try:
            fields = line.split()
            chrom = fields[0]
            start = fields[1]
            end = fields[2]
            name = None
            if len(fields) > 3:
                name = fields[3]
        except:
            continue

        newinterval = doc.createElement( "interval" )
        if chrom.find(".") > -1:
            fields = chrom.split(".")
            species, src = fields[0], fields[1]
            newinterval.setAttribute("species", species)
            newinterval.setAttribute("chrom", src)
        else:
            newinterval.setAttribute( "chrom", chrom )
        newinterval.setAttribute( "start", start )
        newinterval.setAttribute( "end", end )
        if name: 
            newinterval.setAttribute( "name", name )
            if name not in types:
                types[name] = 1
                newtype = doc.createElement("element_type")
                newtype.appendChild(doc.createTextNode(name))
                element_types.appendChild(newtype)
                #element_types.appendChild(doc.createTextNode(name))

        if dataset:
            dataset.appendChild( newinterval )
        else:
            newbed.appendChild( newinterval )

    return doc


def bed_to_xml( infile ):

    # create a mini document
    doc = Document()

    # create a new <bed> base element
    newbed = doc.createElement('bed')
    doc.appendChild(newbed)
    element_types = doc.createElement('types')
    types = {}
    newbed.appendChild(element_types)
    

    # create elements
    for line in infile:
        if line.startswith("#") or line.isspace():
            continue

        if line.startswith(">"): continue

        try:
            fields = line.split()
            chrom = fields[0]
            start = fields[1]
            end = fields[2]
            name = None
            if len(fields) > 3:
                name = fields[3]
        except:
            continue

        newinterval = doc.createElement( "interval" )
        if chrom.find(".") > -1:
            fields = chrom.split(".")
            species, src = fields[0], fields[1]
            newinterval.setAttribute("species", species)
            newinterval.setAttribute("chrom", src)
        else:
            newinterval.setAttribute( "chrom", chrom )
        newinterval.setAttribute( "start", start )
        newinterval.setAttribute( "end", end )
        if name: 
            newinterval.setAttribute( "name", name )
            if name not in types:
                types[name] = 1
                newtype = doc.createElement("element_type")
                newtype.appendChild(doc.createTextNode(name))
                element_types.appendChild(newtype)
                #element_types.appendChild(doc.createTextNode(name))

        newbed.appendChild( newinterval )

    return doc

if __name__ == "__main__": 
    main()
