#!/usr/bin/env python2.6
import sys, os
import cgi, urllib2
import cgitb; cgitb.enable()

# get the project python modules
sys.path.append( os.path.abspath('../../python') )
import cladimo_locale; locale = cladimo_locale.cladimoLocale()
from htmilf import *
from formstuff import *
from accession import *

import formats

MAF_BROWSER_FORM_FIELD = 'MAFBrowserFileUpload'

form = cgi.FieldStorage()

if form:
    if MAF_BROWSER_FORM_FIELD in form:
        filedat = form[ MAF_BROWSER_FORM_FIELD ]
        if len( filedat.value ) > INPUT_DATA_LIMIT:
            msg('Request was successful but the file is too large <b>( %d > %d )</b>' % (len( filedat.value ), INPUT_DATA_LIMIT), 'error')
        else:
            (outfile, filename, filepath, file_accession) = open_next_accession( locale.jobdir, '', '_' + filedat.filename)
            outfile.write( filedat.value )
            outfile.close()
            infile = bx.align.maf.Reader( open( filepath ) )
            doc = formats.maf_to_xml( infile, filedat.filename )
            print "Content-Type: application/xhtml+xml\n"
            print doc.toprettyxml(encoding="UTF-8")
        
    else:
        print "Content-Type: text/html\n\n"
        print form

else:
        print "Content-Type: text/html\n\n"
        print """
<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE html
   PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head> <meta charset="utf-8" /></head>
    <body>
    <form action="/cgi-bin/mafbrowser/upload.py" method="post" enctype='multipart/form-data'>
        <input type='file' name='%s'>
        <input type='submit' name='submit'>
    </form>
    </body>
</html>
""" % MAF_BROWSER_FORM_FIELD
