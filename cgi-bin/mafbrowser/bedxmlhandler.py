#!/usr/bin/env python
import cgi
import cgitb; cgitb.enable()
import sys,os

from xml.dom.minidom import Document

def bed_to_xml( infile ):

    # create a mini document
    doc = Document()

    # create a new <bed> base element
    newbed = doc.createElement('bed')
    doc.appendChild(newbed)

    # create elements
    for line in infile:
        if line.startswith("#") or line.isspace():
            continue

        fields = line.split()
        chrom = fields[0]
        start = fields[1]
        end = fields[2]
        name = None
        if len(fields) > 3:
            name = fields[3]

        newinterval = doc.createElement( "interval" )
        newinterval.setAttribute( "chrom", chrom )
        newinterval.setAttribute( "start", start )
        newinterval.setAttribute( "end", end )
        if name: newinterval.setAttribute( "name", name )

        newbed.appendChild( newinterval )

    return doc

form = cgi.FieldStorage()
if 'tag' in form:
    uploadedfilename = form.getvalue( 'tag' )
    doc = bed_to_xml( file( os.path.join('/home/dcking/cladimo-web', uploadedfilename ) ) )
    print "Content-Type: application/xhtml+xml\n"
    print doc.toprettyxml(indent="  ")
else:
    print "Content-Type: text/html\n\n"
    print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Maf File Upload</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
"""
    print form
    print "<hr>"
    print """
    <h1>Bed File Upload</h1>
    <form action="http://localhost/cgi-bin/bedxmlhandler.py" method="post" enctype='multipart/form-data'>
        <input type='file' name='filename'>
        <input type='submit' name='submit'>
    </form>
</body>
</html>
    """

