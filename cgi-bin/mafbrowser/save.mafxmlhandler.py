#!/usr/bin/env python
import sys,os
sys.path.insert( 0, '/afs/bx.psu.edu/home/dcking/linux-x86_64/lib/python2.4/site-packages/bx_python-0.5.0-py2.4-linux-x86_64.egg/' )
import bx.align.maf

from xml.dom.minidom import Document

def maf_to_xml( reader ):

    # create a mini document
    doc = Document()

    # create a new <maf> base element
    newmaf = doc.createElement('maf')
    doc.appendChild(newmaf)

    # create species elements
    newsplist = doc.createElement('species_list')
    newmaf.appendChild(newsplist)
    splist = {}

    # create block elements
    for block in reader:
        newblock = doc.createElement("block")
        newblock.setAttribute("score", str(block.score))
        newblock.setAttribute("text_size", str(block.text_size))
        # implicitly defined attributes
        for attr, setting in block.attributes.items():
            newblock.setAttribute( attr, str(setting) )

        newmaf.appendChild(newblock)

        for comp in block.components:
            newcomp = doc.createElement( "component" )
            newcomp.setAttribute( "src", comp.src )
            newcomp.setAttribute( "start", str( comp.start) )
            newcomp.setAttribute( "size", str( comp.size) )
            newcomp.setAttribute( "strand", str( comp.strand) )
            newcomp.setAttribute( "text", str( comp.text) )
            newcomp.setAttribute( "src_size", str( comp.src_size) )

            srcsp = comp.src.split('.')[0]
            newcomp.setAttribute( "species", srcsp )

            newblock.appendChild(newcomp)

            if srcsp not in splist:
                print >>sys.stderr, newsplist.attributes
                newsp = doc.createElement( "species" )
                newsp.setAttribute( "id", srcsp )
                newsplist.appendChild( newsp )
                splist[srcsp] =  1
        

    return doc


def pseudomaf_to_xml( fd, filename ):

    header = fd.next().lstrip()
    seq = ''.join(list(fd)).replace('\n','')
    
    doc = Document()

    newmaf = doc.createElement("maf")
    doc.appendChild(newmaf)

    # create single species
    newsplist = doc.createElement('species_list')
    newsp = doc.createElement( "species" )
    newsp.setAttribute( "id", filename )
    newsplist.appendChild( newsp )
    newmaf.appendChild(newsplist)
    # create single block
    newblock = doc.createElement("block")
    newblock.setAttribute("score", "0")
    newblock.setAttribute("text_size", str( len(seq) ) )
    # create single component
    newcomp = doc.createElement( "component" )
    newcomp.setAttribute( "src", filename )
    newcomp.setAttribute( "start", "0" )
    newcomp.setAttribute( "size", str( len( seq ) ) )
    newcomp.setAttribute( "strand", "+" )
    newcomp.setAttribute( "text", seq )
    newcomp.setAttribute( "src_size", str( len( seq ) ) )
    newcomp.setAttribute( "species", filename )
    newblock.appendChild(newcomp)
    newmaf.appendChild(newblock)

    return doc


def main():
    form = cgi.FieldStorage()

    if 'tag' in form:
        uploadedmafname = form.getvalue( 'tag' )
        try:
            doc = maf_to_xml( bx.align.maf.Reader( file( os.path.join('/home/dcking/cladimo-web', uploadedmafname) ) ) )
        except:
            doc = pseudomaf_to_xml( file( os.path.join('/home/dcking/cladimo-web', uploadedmafname) ), uploadedmafname )
        print "Content-Type: application/xhtml+xml\n"
        print doc.toprettyxml(indent="  ")
    else:
        print "Content-Type: text/html\n\n"
        print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
        <title>Maf File Upload</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>
    <body>
    """
        print form
        print "<hr>"
        print """
        <h1>Maf File Upload</h1>
        <form action="http://www.bx.psu.edu/~dcking/cgi-bin/maf_to_xml.py" method="post" enctype='multipart/form-data'>
            <input type='file' name='maf'>
            <input type='submit' name='submit'>
        </form>
    </body>
    </html>
        """

if __name__ == '__main__': 
    import cgi
    import cgitb; cgitb.enable()
    main()
