#!/usr/bin/env python2.6

import sys,os
import cgi, urllib
import cgitb; cgitb.enable()

# get the project python modules
sys.path.append( os.path.abspath('../../python') )
import cladimo_locale; locale = cladimo_locale.cladimoLocale()
import bx.align.maf
from htmilf import *
from formstuff import *
from accession import *

import formats

from xml.dom.minidom import Document

def main():
    form = cgi.FieldStorage()

    if 'tag' in form:
        uploadedmafname = form.getvalue( 'tag' )
        jobdir = locale.jobdir
        tag = urllib.unquote( form['tag'].value )
        dircontents = glob.glob( os.path.join( jobdir, '*' + tag + '*.maf' ) )
        # should be 1 and only 1 match
        uploadedfilepath = dircontents[0]
        uploadedmafname = os.path.basename( uploadedfilepath ).replace(tag + "_", "")
        uploadedfile_h = open( uploadedfilepath )
        try:
            doc = formats.maf_to_xml( bx.align.maf.Reader( uploadedfile_h ), uploadedmafname )
        except:
            doc = pseudomaf_to_xml( uploadedfile_h, uploadedmafname )

        uploadedfile_h.close()

        print "Content-Type: application/xhtml+xml\n"
        print doc.toprettyxml(encoding="UTF-8")
    else:
        print "Content-Type: text/html\n\n"
        print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
        <title>Maf File Upload</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>
    <body>
    """
        print form
        print "<hr>"
        print """
        <h1>Maf File Upload</h1>
        <form action="http://www.bx.psu.edu/~dcking/cgi-bin/maf_to_xml.py" method="post" enctype='multipart/form-data'>
            <input type='file' name='maf'>
            <input type='submit' name='submit'>
        </form>
    </body>
    </html>
        """

if __name__ == '__main__': 
    main()
