#!/usr/bin/env python
import cgi,random
import cgitb; cgitb.enable()
import sys,os

upload_dir = "/afs/bx.psu.edu/home/dcking/cladimo-web"

def get_output_root():
    tempdirfiles = os.listdir(upload_dir)
    outfileroot = "%.5d" % random.randrange(100000)
    while outfileroot in tempdirfiles:
        outfileroot = "%.5d" % random.randrange(100000)
    return outfileroot

def save_uploaded_file (fileitem):
    if not fileitem.file: return
    mafname = get_output_root() + "_" + fileitem.filename
    fout = file (os.path.join(upload_dir, mafname), 'wb')
    while 1:
        chunk = fileitem.file.read(100000)
        if not chunk: break
        fout.write (chunk)
    fout.close()
    return mafname

#get contents
contents = cgi.FieldStorage()
submitted_vals = contents.keys()

mafname = ""
mafpath = ""

# "post" options
if contents.getvalue("maf"):
    mafname = save_uploaded_file( contents[ "maf" ] )
    mafpath = os.path.join( upload_dir, mafname )

pwm_file_list = []
for k in submitted_vals:
    if k.startswith("pwmfile"):
        pwm_file_list.append( os.path.join(upload_dir, save_uploaded_file( contents[ k ] ) ) )

postmotiflist = ""
if contents.getvalue( "motif" ):
    postmotiflist = contents.getvalue( "motif" )

postlibnamelist = ""
if "libname" in submitted_vals:
    libnamearg = contents.getvalue("libname")
    if isinstance(libnamearg,list):
        postlibnamelist = " ".join( libnamearg )
    else:
        postlibnamelist = libnamearg

postliblist = ""
if "static" in submitted_vals:
    libarg = contents.getvalue("static")
    if isinstance(libarg,list):
        postliblist = " ".join( libarg )
    else:
        postliblist = libarg

# "get" options
if contents.getvalue("mafid"):
    mafname = contents.getvalue("mafid")
    mafpath = os.path.join( upload_dir, mafname )


getmotiflist = ""
if "motiflist" in submitted_vals:
    getmotiflist = " ".join(contents.getvalue( "motiflist" ).split(","))

getliblist = ""
if contents.getvalue( "liblist" ):
    getliblist = " ".join(contents.getvalue( "liblist" ).split(","))


outputformat = None
if contents.getvalue("outputformat"):
    outputformat = contents.getvalue("outputformat").split(",")
    
    

# perform the run
cladimopath = "~dcking/bin/x86_64/cladimo"
#cladimopath = "/afs/bx.psu.edu/home/dcking/bin/i686/cladimo"
cladimo = cladimopath + " " + mafpath + " " + " ".join(pwm_file_list) + " " + postlibnamelist + " " + postliblist + " " + postmotiflist + " " + getmotiflist + " " + getliblist

outfileroot = get_output_root()
outpath = os.path.join(upload_dir, outfileroot)
errpath = os.path.join(upload_dir, outfileroot + ".err")
os.system(cladimo + ">" + outpath + " 2>" + errpath)
spmatches = []
for m in os.popen("~dcking/work/cladimo/species_matches.py < " + outpath):
    spmatches.append(m.strip())

# Return results
print "Content-Type: text/html\n\n"
print """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Cladimo results</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
"""

maftitle = "_".join( mafname.split( "_" )[1:])

if outputformat and "browserlink" in outputformat:
    print "<a target=\"mafbrowser\" href=\"http://www.bx.psu.edu/~dcking/mafgraph/mafbrowser.html#mafTitle=" + maftitle + "&mafName=" + mafname + "&annotations=" + outfileroot + "\">View results in maf browser</a>"

if outputformat and "tracklink" in outputformat:
    print "<a href=\"\">"

if outputformat and "outputid" in outputformat:
    print outfileroot
else:
    if os.path.getsize(errpath) == 0:
        print "<a target=\"mafbrowser\" href=\"http://www.bx.psu.edu/~dcking/mafgraph/mafbrowser.html#mafTitle=" + maftitle + "&mafName=" + mafname + "&annotations=" + outfileroot + "\">View results in maf browser</a><br>"
        for m in spmatches:
            print "<a target=\"new\" href=\"%s?tag=%s&restrict=%s\">custom track for %s</a><br>" % ("http://www.bx.psu.edu/~dcking/cgi-bin/custom_tracker_wrapper.py", outfileroot, m, m)
        print "<br>"
        print "<hr>"
        print "<br>".join( list( file( outpath ) ) )

    if os.path.getsize(errpath) > 0:
        print "<pre>"
        print "Error <br>"
        print "<br>".join( list( file( errpath ) ) )
        print "</pre>"



print """
</body>
</html>
"""

