#!/usr/bin/env python2.6
import cgi, urllib2
import cgitb; cgitb.enable()
import sys,os,subprocess
import string

# get the project python modules
sys.path.append( os.path.abspath('../python') )
from accession import *
from htmilf import *

IUPAC_ALLOWABLE = 'ACKGMNRSTWY'

def loseExtension(filename):
    dotplace = filename.rfind('.')
    if dotplace > 0:
        return filename[:dotplace]

    return filename


def main():
    htmlhead = """Content-Type: text/html\n
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />
<style>
.error {
  background-color: LightSalmon;
  padding: 2px;
  width: 80%;
}
div.box {
  border: 1px solid black;
  padding: 1px;
  margin-top: 2px;
  position: absolute;
  min-width: 32%;
}
</style>
</head>
<body>
    """

    htmlfoot = """
</body>
</html>"""

    contents = cgi.FieldStorage()

    # extract fields
    try:
        # step 1, input seq/align
        input_type = contents[ 'input_type' ]
        input_file = contents[ 'input_file' ]
        input_text = contents[ 'input_text' ]
        # step 2, pwm/motif
        pwm_upload = contents[ 'pwm_upload' ]
        pwm_input_text = contents[ 'pwm_input_text' ]
        pwm_acc = contents[ 'pwm_acc' ]
        motif = contents[ 'motif' ]
        # step 3, parameters
        assembly_translate = contents[ 'assembly_translate' ]
        drop_non_ref = False
        if 'drop_non_ref' in contents: contents[ 'drop_non_ref' ]
        target = contents[ 'target' ]

    except KeyError, err:
        bail( htmlhead, span("error: field not found:"  + str( err ) ), htmlfoot )

    # conform input file data
    input_data = None
    maf_seq_input_filename = 'pastedtext'
    pwm_input_filename = 'pastedtext'

    if input_file.value and input_text.value:
        bail( htmlhead, span("both input file and value. which do you want?"), htmlfoot )

    elif input_file.value:
        # use uploaded file
        input_data = input_file.value.replace('\r\n', '\n') # force UNIX-style newlines
        maf_seq_input_filename = loseExtension(input_file.filename)

    elif input_text.value:
        # retrieve file via http
        if input_text.value.startswith('http://'):
            try:
                input_data = urllib2.urlopen( input_text.value ).fp.read()
            except urllib2.URLError, err:
                bail(htmlhead, div( span(err) +  br() + span("check url: %s" % input_text.value), raw=False, _class='error'), htmlfoot)
        # save pasted text as file
        else:
            input_data = input_text.value
            input_data = input_data.replace('\r\n', '\n') # force UNIX-style newlines

    else:
        # none-specified
        bail( htmlhead, span("error: select a MAF or FASTA sequence for input"), br(), htmlfoot )

    if len( input_data ) > INPUT_DATA_LIMIT:
        bail( htmlhead, span('Input MAF/seq is too large (%d > %d)', _class='error') % (len(input_data), INPUT_DATA_LIMIT), htmlfoot )

    # conform PWM file data, if any
    pwm_file_data = None

    if pwm_upload.value:
        pwm_file_data = pwm_upload.value
        pwm_file_data = pwm_file_data.replace('\r\n', '\n') # force UNIX-style newlines
        pwm_input_filename = pwm_upload.filename
    elif pwm_input_text.value:
        # retrieve file via http
        if pwm_input_text.value.startswith('http://'):
            try:
                pwm_file_data = urllib2.urlopen( pwm_input_text.value ).fp.read()
            except urllib2.URLError, err:
                bail( htmlhead, div( span(err) +  br() + span("check url: %s" % pwm_input_text.value), raw=False, _class='error'), htmlfoot )
        # save pasted text as file
        else:
            pwm_file_data = pwm_input_text.value

    # prepare job
    job_fp, job_filename, job_filepath, job_accession = open_next_accession( jobdir, jobprefix, jobsuffix )
    # MAF or seq
    maf_seq_filename = '%s_%s.%s' % ( job_accession, maf_seq_input_filename, input_type.value )
    maf_seq_filepath = os.path.join( jobdir, maf_seq_filename )
    maf_seq_fp = open( maf_seq_filepath, 'w' )
    maf_seq_fp.write( input_data )
    maf_seq_fp.close()
    # PWM file ?
    if pwm_file_data:
        pwm_filename = '%s_%s.pwm' % ( job_accession, pwm_input_filename )
        pwm_filepath = os.path.join( jobdir, pwm_filename )
        pwm_fp = open( pwm_filepath, 'w' )
        pwm_fp.write( pwm_file_data )
        pwm_fp.close()
    else:
        pwm_filename = ''
        pwm_filepath = ''

    # conform pwm/motif arguments
    query = []
    for acc in pwm_acc:
        if acc.value: 
            arg = acc.value.strip()
            argok = True
            for char in arg:
                if char not in string.printable:
                    #invalid string
                    bail(htmlhead, span('bad pie: non-printable characters in ' + arg), br(), htmlfoot )
                    argok = False
                    break

            if argok: query.append( arg )

    if motif.value:
        for arg in motif.value.split():
            arg = arg.strip().upper()
            argok = True
            for char in arg:
                if char not in IUPAC_ALLOWABLE:
                    #invalid motif
                    bail( htmlhead,  span('bad pie: invalid motif ' + arg), br(),
                    span("motifs may only contain %s characters" % IUPAC_ALLOWABLE), br(),
                    span('not a %s' % char), br() )
                    argok = False
                    break

            if argok: query.append( arg )

    # prepare for run
    outfilename = '%s_%s.gff' % (job_accession, maf_seq_input_filename)
    outfilepath = os.path.join(jobdir, outfilename)
    errfilename = 'job.%s.err' % (job_accession)
    errfilepath = os.path.join(jobdir, errfilename)
    command = '%s/cladimo %s %s %s > %s\n' % (bindir, maf_seq_filename, pwm_filename, ' '.join(query), outfilename) 

    job_fp.write( command )
    job_fp.close()
                
    # run the job
    rval = 0
    try:
        stderr_h = open(errfilepath, 'w')
        rval = subprocess.call(["sh", job_filepath], cwd=os.path.abspath(jobdir), stderr=stderr_h)
        stderr_h.close()
    except OSError as e:
        print htmlhead
        print "<div class='error'>"
        print e
        print '</div>'
        print htmlfoot

    # cgi-result page
    if rval < 0:
        print htmlhead
        if os.path.exists( errfilepath ):
            print "<div class='error'>"
            print '<br/>'.join( list( open( errfilepath ) ) )
            print '</div>'
        else:
            print span('Error, but no error file', _class='error'), br()
        print htmlfoot

        if os.path.exists( outfilepath ):
            print '\n'.join( list( open( outfilepath ) ) )
        else:
            print span('Error, no output', _class='error'), br()
        print htmlfoot
    else:
        if os.path.exists( outfilepath ):
            #print 'Content-Type: text/plain\n'
            #print ''.join( list( open( outfilepath ) ) )
            #print 'Location:/cgi-bin/results.py?tag=%s' % job_accession
            #print
            #print 'Content-Type: text/plain\n'
            #print target, target.value, bool('target' in contents), bool(('target' in contents) and target.value == 'mafbrowser')
            if 1:
                if ('target' in contents) and target.value == 'mafbrowser':
                    print 'Refresh: 0; url=/mafbrowser/mafbrowser.html#tag=%s' % job_accession
                    print
                else:
                    print 'Refresh: 0; url=/cgi-bin/results.py?tag=%s' % job_accession
                    print
        else:
            bail(htmlhead, span('Error, no output', _class='error'), br(), htmlfoot)
    
    ### end of results

if __name__ == '__main__': main()
