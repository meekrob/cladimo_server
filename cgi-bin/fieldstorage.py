#!/usr/bin/env python2.5
import cgi
import cgitb; cgitb.enable()
import sys,os

def styleargs(kwargs): 
    
    return '; '.join(['%s: %s' % (k.replace('_','-'),v) for k,v in kwargs.items()])

def span( s, **kwargs ):
    return "<span style='%s'>%s</span>" % (styleargs(kwargs), s)

def printwww(s):
    print cgi.escape( str(s) ), '<br/>'

print "Content-Type: text/html\n\n"
htmlhead = """
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
<head>
<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />
</head>
<body>
"""

htmlfoot = """
</body>
</html>"""

contents = cgi.FieldStorage()

print htmlhead
print "--------------------<br/>"
print span("list:", width='80%', background_color='LightCyan', position='absolute', font_weight='bold'), '<br/>'
for item in contents:
    field = contents[ item ]
    if isinstance(field, cgi.FieldStorage):
        print 'name:'; printwww(field.name)
        print 'filename:'; printwww(field.filename)
        print 'value:'; printwww(field.value[:40])
        print 'file:'; printwww(field.file)
        if isinstance(field.file, file):
            print 'file has %d lines<br/>' % len(list(field.file))
        print 'isinstance(file)', isinstance(field.file, file), '<br/>'
        print 'type:'; printwww(field.type)
        print 'type_options:'; printwww(field.type_options)
        print 'disposition:'; printwww(field.disposition)
        print 'disposition_options:'; printwww(field.disposition_options)
    print '<br/>'

print "--------------------<br/>"
print span("headers:", width='80%', background_color='LightCyan', position='absolute', font_weight='bold'), '<br/>'
if contents.headers:
    for k,v in contents.headers.items():
        print span(cgi.escape(k), font_weight='bold'), cgi.escape(v), '<br/>'

print htmlfoot


