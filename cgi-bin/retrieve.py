#!/usr/bin/env python2.6
import sys, os
import cgi, urllib2
import cgitb; cgitb.enable()
# get the project python modules
sys.path.append( os.path.abspath('../python') )
from htmilf import *
from formstuff import *
from accession import *

def getoutformat( form ):
    # txt unless xml=y or yes
    if 'xml' in form and form[ 'xml' ].value in ['y', 'yes']: 
        return 'xml'

    return 'txt'

header = htmlhead % {'style': '', 'title': '<title>Retrieve acc</title>'}

form = cgi.FieldStorage()
if 'tag' in form:
    filename = form[ 'tag' ].value
    ext = '.' + filename.split('.')[-1]
    print getfile( filename, ext, getoutformat( form ) )
        

elif 'acc' in form:
    acc = form[ 'acc' ].value
    files = get_accession_files( acc )
    if 'ext' in form:
        ext = form[ 'ext' ].value
        if ext in files:
            print getfile( files[ ext ], ext, getoutformat( form ) )
        else:
            # no accession.ext in jobdir
            print header
            print '%s not among list of files matching accession %s, ...<br/>' % (ext, acc)
            print '<pre>'
            print cgi.escape( str(files) ).replace(',', ',<br/>')
            print '</pre>'
            print htmlfoot
            
    else:
        # print list
        print header
        print '<pre>'
        print cgi.escape( str(files) ).replace(',', ',<br/>')
        print '</pre>'
        print htmlfoot
else:
    # no, anticipated tags in form submission,
    print header
    # print form input
    print_all_fields( form )
    print htmlfoot

